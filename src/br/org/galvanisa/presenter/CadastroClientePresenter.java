/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.sysmetal.SysMetalPresenter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class CadastroClientePresenter implements Initializable {

    private ClientesPresenter clientesPresenter = new ClientesPresenter();
    private ControleClientePresenter controleClientePresenter = new ControleClientePresenter();
    private ControleCliente controleCliente = new ControleCliente();
    private ObservableList<ControleCliente> controleClienteList = FXCollections.observableArrayList();
    ObservableList<Boolean> statusList = FXCollections.observableArrayList();
    private ControleClienteService controleClienteService = new ControleClienteService();
    private ControleCliente cliente = new ControleCliente();
    private String tipoOperacao = "NOVO";
    private SysMetalPresenter sysMetalPresenter = new SysMetalPresenter();

    @FXML
    private TextField codigoTxt;
    @FXML
    private TextField descricaoTxt;
    @FXML
    private TextField contratoTxt;
    @FXML
    private TextField opTxt;
    @FXML
    private ComboBox statusComboBox;
    @FXML
    private Button salvarBtn;
    @FXML
    private Button cancelarBtn;
    @FXML
    private ComboBox terceiroComboBox;
    @FXML
    private AnchorPane fundo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fundo.getStyleClass().setAll("bordePane-cliente");
        configureFields();
        setKeyEventInLabel(codigoTxt, descricaoTxt);
        setKeyEventInLabel(descricaoTxt, contratoTxt);
        setKeyEventInLabel(contratoTxt, opTxt);

        opTxt.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    statusComboBox.requestFocus();
                }
            }
        });

        statusComboBox.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    terceiroComboBox.requestFocus();
                }
            }
        });

        terceiroComboBox.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    salvarBtn.requestFocus();
                }
            }
        });

        salvarBtn.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (codigoTxt.getText().equals("") || descricaoTxt.getText().equals("") || opTxt.getText().equals("")) {
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Campo obrigatório (*) não preenchido!").showWarning();
                } else {
                    Node source = (Node) mouseEvent.getSource();
                    Stage stage = (Stage) source.getScene().getWindow();
                    stage.close();
                    try {
                        botaoSalvar();
                    } catch (IOException ex) {
                        Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        });

        salvarBtn.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    if (codigoTxt.getText().equals("") || descricaoTxt.getText().equals("") || opTxt.getText().equals("")) {
                        Dialogs.create()
                                .title("Aviso!").masthead("")
                                .message("Campo obrigatório (*) não preenchido!").showWarning();
                    } else {
                        Node source = (Node) keyEvent.getSource();
                        Stage stage = (Stage) source.getScene().getWindow();
                        stage.close();
                        try {
                            botaoSalvar();
                        } catch (IOException ex) {
                            Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        );

        cancelarBtn.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent
                    ) {

                        Node source = (Node) mouseEvent.getSource();
                        Stage stage = (Stage) source.getScene().getWindow();
                        stage.close();
                    }
                }
        );
    }

    public void botaoSalvar() throws IOException {

        controleCliente.setCodCliente(codigoTxt.getText());
        controleCliente.setNumContrato(contratoTxt.getText());
        controleCliente.setDescCliente(descricaoTxt.getText());
        controleCliente.setNumOp(opTxt.getText());
        controleCliente.setStatus((Boolean) statusComboBox.getValue());
        controleCliente.setTerceiro((Boolean) terceiroComboBox.getValue());
        if (tipoOperacao.equals("NOVO")) {
            controleClienteService.addControleCliente(controleCliente);
        } else if (tipoOperacao.equals("ALTERAR")) {
            cliente.setStatus((Boolean) statusComboBox.getValue());
            cliente.setTerceiro((Boolean) terceiroComboBox.getValue());
            controleClienteService.updateCliente(cliente);
        }
        //controleClienteList = controleClienteService.getAllControleCliente();

        clientesPresenter.populateTableViewInterna();
        clientesPresenter.populateTableViewTerceiros();
        sysMetalPresenter.actionBtnTerceiros();
    }

    public void setController(ControleClientePresenter controleClientePresenter) {
        this.controleClientePresenter = controleClientePresenter;
    }

    public void setController(ClientesPresenter clientesPresenter) {
        this.clientesPresenter = clientesPresenter;
        tipoOperacao = "NOVO";
    }

    public void setController(ClientesPresenter clientesPresenter, ControleCliente cliente) {
        this.cliente = cliente;
        this.clientesPresenter = clientesPresenter;
        alterarCliente();
        tipoOperacao = "ALTERAR";

    }

    private void configureFields() {
        codigoTxt.textProperty().addListener(new utilities.TextFieldListener(this.codigoTxt));
        opTxt.textProperty().addListener(new utilities.TextFieldListener(this.opTxt));
        statusList.setAll(Boolean.FALSE, Boolean.TRUE);
        statusComboBox.setItems(statusList);
        terceiroComboBox.setItems(statusList);
        statusComboBox.getSelectionModel().select(1);
        terceiroComboBox.getSelectionModel().select(1);
    }

    private void setKeyEventInLabel(TextField primeiro, TextField Segundo) {
        primeiro.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    Segundo.requestFocus();
                }
            }
        });
    }

    private void alterarCliente() {

        descricaoTxt.setText(cliente.getDescCliente());
        contratoTxt.setText(cliente.getNumContrato());
        statusComboBox.getSelectionModel().select(cliente.getStatus());
        terceiroComboBox.getSelectionModel().select(cliente.getTerceiro());
        codigoTxt.setText(cliente.getCodCliente().trim());
        opTxt.setText(cliente.getNumOp().trim());
        descricaoTxt.setEditable(false);
        codigoTxt.setEditable(false);
        opTxt.setEditable(false);
        terceiroComboBox.setEditable(false);
        contratoTxt.setEditable(false);

    }
}
