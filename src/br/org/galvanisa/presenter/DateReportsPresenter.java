/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.reports.entradas.ChamarRelatorioClienteEntradas;
import br.org.galvanisa.business.reports.saidas.ChamarRelatorioClienteSaidas;
import br.org.galvanisa.business.reports.transfer.ChamarRelatoriosGerais;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

/**
 * Simple Preloader Using the ProgressBar Control
 *
 * @author thiagorodrigues
 */
public class DateReportsPresenter implements Initializable {

    ChamarRelatorioClienteSaidas chamarRelatorioClienteSaida = new ChamarRelatorioClienteSaidas();
    ChamarRelatorioClienteEntradas chamarRelatorioClienteEntradas = new ChamarRelatorioClienteEntradas();
    ChamarRelatoriosGerais chamarRelatoriosGerais = new ChamarRelatoriosGerais();
    static String identificadorRelatorio;
    static int codControleCliente;
    static String codCliente;
    @FXML
    private DatePicker dataInicialTxt;
    @FXML
    private DatePicker dataFinalTxt;
    @FXML
    private Button btnGerarReport;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label titulo1;
    @FXML
    private Label titulo2;

    public String getCodCliente() {
        return codCliente;
    }

    void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public void initialize(URL location, ResourceBundle resources) {
        fundo.getStyleClass().add("bordePane");
        titulo1.getStyleClass().add("titulo");
        titulo2.getStyleClass().add("titulo");

        btnGerarReport.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                try {
                    String dataInicial = dataInicialTxt.getValue().toString().replace("-", "");
                    String dataFinal = dataFinalTxt.getValue().toString().replace("-", "");
                    //Chama um relatodio diferente de acordo com o identificador
                    //passado ao abrir a view DateReports
                    if ("EntradasData".equals(identificadorRelatorio)) {
                        chamarRelatorioClienteEntradas.chamarRelatorioEntradasClienteData(codCliente, dataInicial, dataFinal);
                    } else if ("TotalTransferenciaData".equals(identificadorRelatorio)) {
                        chamarRelatoriosGerais.chamarRelatorioTransferenciaData(dataInicial, dataFinal);
                    } else if ("TotalSaidasData".equals(identificadorRelatorio)) {
                        chamarRelatorioClienteSaida.chamarRelatorioTodasSaidasData(dataInicial, dataFinal);
                    } else if ("TotalEntradasData".equals(identificadorRelatorio)) {
                        chamarRelatorioClienteEntradas.chamarRelatorioTodasEntradasData(dataInicial, dataFinal);
                    } else if ("SaidasData".equals(identificadorRelatorio)) {
                        chamarRelatorioClienteSaida.chamarRelatorioSaidasData(codControleCliente, dataInicial, dataFinal);
                    }
                    Node source = (Node) t.getSource();
                    Stage stage = (Stage) source.getScene().getWindow();
                    stage.close();
                } catch (SQLException ex) {
                    System.out.println("Erro no banco de dados");
                    Logger.getLogger(DateReportsPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    System.out.println("Erro IO");
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JRException ex) {
                    System.out.println("Error JRE");
                    Logger.getLogger(DateReportsPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void setCliente(ControleCliente cliente) {
        codCliente = cliente.getCodCliente();
        codControleCliente = cliente.getCodControle();
    }
}
