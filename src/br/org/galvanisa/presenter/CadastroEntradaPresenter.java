package br.org.galvanisa.presenter;

import br.org.galvanisa.business.cadastre.entrada.EntradaProdutos;
import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.presentation.entradas.ClienteEntradasService;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import utilities.NumberTextField;
import utilities.fieldsControl;

/**
 *
 * @author eric
 */
public class CadastroEntradaPresenter implements Initializable {

    private BigDecimal comprimento;
    private int tipoEntrada;
    private MateriaisPresenter previousPresenter;
    private ControleCliente cliente1;
    private final char[] tiposEntrada = {'N', 'S', 'T', 'E'};
    private ObservableList<EntradaProdutos> dataTableViewInserir = FXCollections.observableArrayList();
    private String sCodigoProduto;
    private NumberTextField newfieldKg;
    Boolean isSobra = false;
    static BigDecimal somatorio = BigDecimal.ZERO;
    ProdutosPresenter produtosPresenter = new ProdutosPresenter();

    @FXML
    private Button btnSalvar;
    @FXML
    private TextField fieldCodCliente;
    @FXML
    private TextField fieldNumNf;
    @FXML
    private TextField fieldNumCertificado;
    @FXML
    private TextField fieldNumFornecedor;
    @FXML
    private DatePicker fieldDtEntrada;
    @FXML
    private Button btnApagar;
    @FXML
    private ChoiceBox fieldTipoEntrada;
    @FXML
    private TextField fieldTotalKg;
    @FXML
    private TableView<EntradaProdutos> itensTableView;
    @FXML
    private TableColumn quantKgCol;
    @FXML
    private TableColumn quantUnCol;
    @FXML
    private TableColumn comprimentoCol;
    @FXML
    private TableColumn codigoCol;
    @FXML
    private TableColumn descricaoCol;
    @FXML
    private TableColumn fconversaoCol;
    @FXML
    private TextField fieldDescProduto;
    @FXML
    private TextField fieldQuantUn;
    @FXML
    private TextField fieldFator;
    @FXML
    private TextField fieldKg;
    @FXML
    private TextField fieldComprimento;
    @FXML
    private Button btnInserir;
    @FXML
    private VBox fieldKgVBox;
    @FXML
    private HBox footHbox;

    Image imgButtonDelete = new Image(getClass().getResourceAsStream("/view/icons/Delete.png"));
    Image imgButtonInsert = new Image(getClass().getResourceAsStream("/view/icons/Add.png"));
    Image imgButtonSave = new Image(getClass().getResourceAsStream("/view/icons/Save.png"));

    ClienteEntradasService clienteEntradaService = new ClienteEntradasService();
    @FXML
    private StackPane fundo;
    @FXML
    private GridPane gridPane;
    @FXML
    private Label titulo1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fundo.getStyleClass().add("bordePane");
        itensTableView.getStyleClass().add("table");
        gridPane.getStyleClass().add("grid-pane");
        titulo1.getStyleClass().add("titulo");
        fieldDtEntrada.getStyleClass().add("datepicket");
        btnApagar.setGraphic(new ImageView(imgButtonDelete));
        btnSalvar.setGraphic(new ImageView(imgButtonSave));
        btnInserir.setGraphic(new ImageView(imgButtonInsert));

        fieldCodCliente.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumNf.requestFocus();
                }
            }
        });

        fieldNumNf.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumCertificado.requestFocus();
                }
            }
        });

        fieldNumCertificado.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumFornecedor.requestFocus();
                }
            }
        });

        fieldNumFornecedor.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldDtEntrada.requestFocus();
                }
            }
        });
        fieldDtEntrada.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    btnSalvar.requestFocus();
                }
            }
        });
        fieldTipoEntrada.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    btnSalvar.requestFocus();
                }
            }
        });

    }

    void setCliente(ControleCliente cliente1, MateriaisPresenter aThis, int tipo) {
        tipoEntrada = tipo;
        this.previousPresenter = aThis;
        this.cliente1 = cliente1;

        settingFieldsHead();
        configFieldsHead();
        //settingFieldsBotton();
        fieldDescProduto.setEditable(false);

        configFieldDescProduto();
        configFieldsBotton();
        // caracteristicas da entrada passada pelo menu da tela materiais
        if (tipo == 1) {
            isSobra = false;
            newfieldKg.setPromptText("KG");
            fieldTipoEntrada.getSelectionModel().select(0);
            fieldTipoEntrada.setDisable(true);
            fieldComprimento.setDisable(true);
            comprimentoCol.setVisible(false);
            createColumns(tipo);
        } else if (tipo == 2) { // sobras
            isSobra = true;
            newfieldKg.setPromptText("KG");
            fieldTipoEntrada.getSelectionModel().select(1);
            fieldTipoEntrada.setDisable(true);
            fieldKg.setDisable(true);
            fieldQuantUn.setText("1");
            fieldQuantUn.setDisable(true);
            createColumns(tipo);

            // habilita comprimento ( calcula o peso através do comprimento Fator de conversao) !
        } else if (tipo == 3) { // doubleClick Vizualização Transferencia .. outros !

        }
        configButtonInserir();
        configButtonApagar();
        confgButtonSalvar();
    }

    private void settingFieldsHead() {
        fieldCodCliente.setText(cliente1.getCodCliente());
        fieldCodCliente.setEditable(false);
        fieldNumFornecedor.setText(cliente1.getCodCliente());
        fieldDtEntrada.setValue(LocalDate.now());
        fieldTipoEntrada.setItems(FXCollections.observableArrayList("Normal", "Sobra",
                "Transferencia", "Estorno"));
        fieldTotalKg.setDisable(true);
        somatorio = BigDecimal.ZERO;
        fieldTotalKg.setText(somatorio.toString());
    }

    private void configFieldsHead() {

        fieldNumFornecedor.addEventFilter(KeyEvent.KEY_TYPED, fieldsControl.numFilter());
        fieldNumFornecedor.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(9));
        fieldNumFornecedor.textProperty().addListener(new utilities.TextFieldListener(this.fieldNumFornecedor));

        fieldNumNf.addEventFilter(KeyEvent.KEY_TYPED, fieldsControl.numFilter());
        fieldNumNf.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(9));
        fieldNumNf.textProperty().addListener(new utilities.TextFieldListener(this.fieldNumNf));

        fieldNumCertificado.addEventFilter(KeyEvent.KEY_TYPED, fieldsControl.numFilter());
        fieldNumCertificado.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(9));
        fieldNumCertificado.textProperty().addListener(new utilities.TextFieldListener(this.fieldNumCertificado));
    }

    private void createColumns(int tipo) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codProduto"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory<>("descProduto"));
        quantUnCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        fconversaoCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        quantKgCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        comprimentoCol.setCellValueFactory(new PropertyValueFactory<>("comprimento"));
        itensTableView.setItems(dataTableViewInserir);
    }

    private void configFieldsBotton() {
        if (tipoEntrada == 1) {
            footHbox.getChildren().remove(fieldComprimento);
        } else if (tipoEntrada == 2) {
            footHbox.getChildren().remove(fieldQuantUn);
        }
        fieldFator.setDisable(true);
        fieldQuantUn.addEventFilter(KeyEvent.KEY_TYPED, fieldsControl.numFilter());
        fieldQuantUn.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(9));
        fieldQuantUn.textProperty().addListener(new utilities.TextFieldListener(this.fieldQuantUn));
        fieldKgVBox.getChildren().remove(fieldKg);
        newfieldKg = new NumberTextField(BigDecimal.ZERO, new DecimalFormat("#,##0.00")); // esse aqui é outro, nao é o filtro padrao
        newfieldKg.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(9));
        newfieldKg.clear();
        newfieldKg.setPromptText("KG");
        newfieldKg.setAlignment(Pos.CENTER_RIGHT);
        fieldKgVBox.getChildren().addAll(newfieldKg);
        fieldQuantUn.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == true) {
                    if (!fieldQuantUn.getText().isEmpty() && !fieldDescProduto.getText().isEmpty()) {
                        BigDecimal unidade = new BigDecimal(fieldQuantUn.getText());
                        BigDecimal conversao = new BigDecimal(fieldFator.getText());
                        BigDecimal resultado = BigDecimal.ZERO;
                        resultado = unidade.multiply(conversao);
                        newfieldKg.setNumber(resultado);
                        newfieldKg.requestFocus();
                    } else {
                    }
                }
            }
        });

        fieldComprimento.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (oldValue == true) {
                    if (fieldDescProduto.getText().isEmpty()) {
                        fieldDescProduto.requestFocus();
                    } else if (!fieldComprimento.getText().isEmpty()) {
                        BigDecimal unidade = new BigDecimal(fieldComprimento.getText().replace(',', '.'));
                        returnComprimentoSobra();
                        BigDecimal conversao = new BigDecimal(fieldFator.getText());
                        BigDecimal resultado = BigDecimal.ZERO;
                        resultado = unidade.multiply(conversao).divide(returnComprimentoSobra());
                        newfieldKg.setNumber(resultado);
                        newfieldKg.requestFocus();
                    } else {
                        Dialogs.create()
                                .title("Aviso!").masthead("")
                                .message("Por favor, digite o comprimento!").showWarning();
                    }
                }
            }

        });

        if (tipoEntrada == 2 && !fieldDescProduto.getText().isEmpty()) {
            fieldComprimento.focusedProperty().addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                    if (oldValue == true) {

                        BigDecimal unidade = new BigDecimal(fieldComprimento.getText());
                        returnComprimentoSobra();
                        BigDecimal conversao = new BigDecimal(fieldFator.getText());
                        BigDecimal resultado = BigDecimal.ZERO;
                        resultado = unidade.multiply(conversao).divide(returnComprimentoSobra());// pesquisar se contem 6m ou 12m ?
                        newfieldKg.setNumber(resultado);
                        newfieldKg.requestFocus();
                    }
                }
            });
        }

        fieldComprimento.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {

                    btnInserir.requestFocus();

                }
            }
        });
        fieldKg.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {

                    btnInserir.requestFocus();

                }
            }
        });

        fieldQuantUn.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {

                    newfieldKg.requestFocus();

                }
            }
        });

        fieldDescProduto.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldQuantUn.requestFocus();
                }
            }
        });

        fieldKg.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    btnInserir.requestFocus();
                }
            }
        });

        fieldKg.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.TAB) {
                    btnInserir.requestFocus();
                }
            }
        });

        btnInserir.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    inserir();
                }
            }
        });
    }

    private void configFieldDescProduto() {
        fieldDescProduto.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(final MouseEvent mouseEvent) {
                        produtosPresenter.setRelatorioTrue();
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Produtos.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        ProdutosPresenter controller = loader.<ProdutosPresenter>getController();
                        controller.returnProduto(CadastroEntradaPresenter.this);

                        Scene scene = new Scene(root);
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }
                }
        );
    }

    BigDecimal returnComprimentoSobra() {
        this.comprimento = comprimento;
        return this.comprimento;
    }

    void setProdutoField(String codigo, String descricao, BigDecimal fconversao) {
        if (tipoEntrada == 1) {
            sCodigoProduto = codigo;
            fieldQuantUn.clear();
            fieldQuantUn.setPromptText("UN");
            fieldDescProduto.clear();
            fieldDescProduto.setText(descricao);
            fieldFator.clear();
            fieldFator.setText(fconversao.toString());
            newfieldKg.clear();
            newfieldKg.setPromptText("KG");
            newfieldKg.setEditable(false);
        } else if (tipoEntrada == 2) {
            fieldQuantUn.setDisable(true);
            sCodigoProduto = codigo;
            fieldDescProduto.clear();
            fieldDescProduto.setText(descricao);
            if (descricao.contains("6M") || descricao.contains("6000")) {
                comprimento = BigDecimal.valueOf(6000);
            } else if (descricao.contains("12M") || descricao.contains("12000")) {
                comprimento = BigDecimal.valueOf(12000);
            }
            fieldFator.clear();
            fieldFator.setText(fconversao.toString());
            newfieldKg.setPromptText("KG");
            newfieldKg.setEditable(false);
            fieldComprimento.setEditable(true);
        }
    }

    private void configButtonInserir() {
        btnInserir.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                inserir();
            }
        });
    }

    private void configButtonApagar() {
        btnApagar.setOnAction((ActionEvent t) -> {
            if (itensTableView.getItems().isEmpty()) {
                Dialogs.create()
                        .title("Aviso!").masthead("")
                        .message("Nenhum produto Adicionado").showWarning();
            } else if (itensTableView.getSelectionModel().isEmpty()) {
                Dialogs.create()
                        .title("Aviso!").masthead("")
                        .message("Selecione um produto").showWarning();
            } else {
                itensTableView.getSelectionModel().getSelectedItem();
                BigDecimal x = itensTableView.getSelectionModel().getSelectedItem().getQuantKg();
                somatorio = somatorio.subtract(x);
                fieldTotalKg.clear();
                fieldTotalKg.setText(somatorio.toString());
                dataTableViewInserir.remove(itensTableView.getSelectionModel().getSelectedItem());
            }
        });
    }

    private void confgButtonSalvar() {
        btnSalvar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (itensTableView.getItems().isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!").masthead("Nenhum produto adicionado a entrada")
                            .message("Clique no campo de produto para adicionar").showWarning();
                } else {
                    String sdt = treatFieldDtEntrada();
                    Entrada entrada = new Entrada(
                            fieldCodCliente.getText(), fieldNumNf.getText(),
                            fieldNumFornecedor.getText(),
                            fieldNumCertificado.getText(),
                            tiposEntrada[fieldTipoEntrada.getSelectionModel().getSelectedIndex()],
                            new BigDecimal(fieldTotalKg.getText()), sdt);
                    //model.CadastroEntradaModel.saveNewEntrada(entrada, dataTableViewInserir);
                    clienteEntradaService.saveNewEntrada(entrada, dataTableViewInserir);
                    previousPresenter.setCliente(cliente1); // jogar os controllers em um array para atualizar as transferencias
                    Node source = (Node) e.getSource();
                    Stage stage = (Stage) source.getScene().getWindow();
                    stage.close();
                }
            }
        });
    }

    public String treatFieldDtEntrada() {
        String str_pattern = "yyyyMMdd";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(str_pattern);
        fieldDtEntrada.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                return dateFormatter.format(object);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, dateFormatter);
            }
        });
        final StringConverter<LocalDate> defaultConverter = fieldDtEntrada.getConverter();
        //String sdt = new String(defaultConverter.toString(fieldDtEntrada.getValue()));
        return defaultConverter.toString(fieldDtEntrada.getValue());
    }

    private void inserir() {
        if (fieldDescProduto.getText().isEmpty()) {
            Dialogs.create()
                    .title("Aviso!").masthead("")
                    .message("Por favor escolha um produto!").showWarning();
            fieldDescProduto.requestFocus();
        } else {
            if (tipoEntrada == 1) {
                if (fieldQuantUn.getText().equals("")) {
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Para inserir o produto, digite uma quantidade!").showWarning();
                    fieldDescProduto.requestFocus();
                } else {
                    fieldComprimento.setText(BigDecimal.ZERO.toString());
                    dataTableViewInserir.add(new EntradaProdutos(
                            sCodigoProduto, fieldDescProduto.getText(),
                            new BigDecimal(fieldFator.getText()),
                            newfieldKg.getNumber(), new BigDecimal(fieldQuantUn.getText()),
                            new BigDecimal(fieldComprimento.getText()), isSobra));
                    fieldTotalKg.clear();
                    somatorio = somatorio.add(newfieldKg.getNumber());
                    fieldTotalKg.setText(somatorio.toString());
                    fieldDescProduto.clear();
                    fieldDescProduto.setPromptText("Produto");
                    fieldFator.clear();
                    fieldFator.setPromptText("f. conversao");
                    fieldQuantUn.clear();
                    fieldQuantUn.setPromptText("UN");
                    newfieldKg.clear();
                    newfieldKg.setPromptText("KG");
                }
            } else if (tipoEntrada == 2) {
                if (fieldComprimento.getText().equals("")) {
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Para inserir o produto, digite o comprimento!").showWarning();
                    fieldDescProduto.requestFocus();
                } else {
                    dataTableViewInserir.add(new EntradaProdutos(
                            sCodigoProduto, fieldDescProduto.getText(),
                            new BigDecimal(fieldFator.getText()),
                            newfieldKg.getNumber(), new BigDecimal(1),
                            new BigDecimal((fieldComprimento.getText().replace(',', '.'))), isSobra));
                    fieldTotalKg.clear();
                    somatorio = somatorio.add(newfieldKg.getNumber());
                    fieldTotalKg.setText(somatorio.toString());
                    fieldDescProduto.clear();
                    fieldDescProduto.setPromptText("Produto");
                    fieldFator.clear();
                    fieldFator.setPromptText("f. conversao");
                    fieldQuantUn.clear();
                    fieldQuantUn.setPromptText("UN");
                    newfieldKg.clear();
                    newfieldKg.setPromptText("KG");
                    fieldComprimento.clear();
                    fieldComprimento.setPromptText("Comprimento");
                }
            }
        }
    }

}
