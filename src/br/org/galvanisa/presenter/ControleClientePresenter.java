package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.controleof.ControleOfService;
import br.org.galvanisa.sysmetal.SysMetalPresenter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControleClientePresenter implements Initializable {

    ControleClienteService clienteService = new ControleClienteService();
    ControleOfService ofsServices = new ControleOfService();
    private final SysMetalPresenter sysMetalPresenter = new SysMetalPresenter();
    private final MateriaisPresenter materiaisPresenter = new MateriaisPresenter();
    private final ControleClienteService controleClienteService = new ControleClienteService();
    private ControleCliente cliente1 = new ControleCliente();
    private ObservableList<ControleCliente> controleCliente = FXCollections.observableArrayList();

    @FXML
    private TableColumn codigoCol;
    @FXML
    private TableColumn descricaoCol;
    @FXML
    private TableColumn contratoCol;
    @FXML
    private TableColumn opCol;
    @FXML
    private TableColumn statusCol;
    @FXML
    private Button statusButton;
    @FXML
    private TextField buscaClienteLabel;
    @FXML
    private Button editarButton;
    @FXML
    private Button novoButton;
    @FXML
    private TableView clienteTableView;
    @FXML
    private TableColumn terceiroCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        statusButton.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                cliente1 = (ControleCliente) clienteTableView.getSelectionModel().getSelectedItem();
                if (cliente1.getStatus()) {
                    cliente1.setStatus(Boolean.FALSE);
                } else if (!cliente1.getStatus()) {
                    cliente1.setStatus(Boolean.TRUE);
                }
                controleClienteService.alterStatus(cliente1);
                populateTableView();
                try {
                    sysMetalPresenter.init();
                } catch (IOException ex) {
                    Logger.getLogger(ControleClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

        novoButton.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroCliente.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                CadastroClientePresenter controller = loader.<CadastroClientePresenter>getController();
                controller.setController(ControleClientePresenter.this);
                Scene scene = new Scene(root);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();

            }

        });

    }

    public void populateTableView() {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codCliente"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory<>("descCliente"));
        contratoCol.setCellValueFactory(new PropertyValueFactory<>("numContrato"));
        opCol.setCellValueFactory(new PropertyValueFactory<>("numOp"));
        statusCol.setCellValueFactory(new PropertyValueFactory<ControleCliente, Boolean>("status"));
        terceiroCol.setCellValueFactory(new PropertyValueFactory<ControleCliente, Boolean>("terceiro"));
        controleCliente = controleClienteService.getAllControleCliente();
        clienteTableView.setItems(controleCliente);
    }

    public void populateTableView2() {

        populateTableView();
        //clienteTableView.setItems(controleCliente);

    }

    public void atualizeTableView(ObservableList<ControleCliente> controleClienteList) {

        populateTableView2();

    }
}