package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.controleof.ControleOfService;
import br.org.galvanisa.presentation.produtos.LoadProdutosTerceiroService;
import br.org.galvanisa.presentation.saidas.SaidasService;
import br.org.galvanisa.sysmetal.SysMetalPresenter;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;
import utilities.NumberTextField;

/**
 *
 * @author eric
 */
public class CadastroSaidaPresenter implements Initializable {

    private MateriaisPresenter previousController;
    private SaidasPresenter previousSaidaController;
    private ControleCliente cliente1;
    private LoadProdutosTerceiroService loadProdutos = new LoadProdutosTerceiroService();
    private ObservableList<Produtos> produtos = FXCollections.observableArrayList();
    private ObservableList<ControleOf> ofs = null;
    private ObservableList ofsList = FXCollections.observableArrayList();
    private ObservableList pcsList = FXCollections.observableArrayList();
    private int codigoControleOf = 0;
    private ControleOfService controleOfService = new ControleOfService();
    private final char[] tiposSaidas = {'N', 'T', 'E', 'S', 'O', 'D'};
    private ObservableList<MateriaisData> materiaisItens;
    private ObservableList transfClientes = FXCollections.observableArrayList();
    private SaidasService saidasService = new SaidasService();
    private ControleClienteService controleClienteService = new ControleClienteService();
    private ObservableList<ControleCliente> clientes;
    private int tipo;
    private List<Object> saidas = new ArrayList<>();
    BigDecimal value;
    private NumberTextField nt = new NumberTextField();
    private SysMetalPresenter sysMetalPresenter = new SysMetalPresenter();
    Image imgButtonSave = new Image(getClass().getResourceAsStream("/view/icons/Save.png"));

    @FXML
    private VBox mainVBox;
    @FXML
    private GridPane saidaGrid;
    @FXML
    private Button btnSalvar;
    @FXML
    private TextField fieldCodCliente;
    @FXML
    private TextField fieldNumOp;
    @FXML
    private DatePicker fieldDtSaida;
    @FXML
    private ChoiceBox fieldTpOperacao;
    @FXML
    private ChoiceBox fieldNumOf;
    @FXML
    private ChoiceBox fieldNumPc;
    @FXML
    private TextField fieldTotalKg;
    @FXML
    private ChoiceBox fieldTransferencia;
    @FXML
    private TableView<MateriaisData> itensTableView;
    @FXML
    private TableColumn quantUnCol;
    @FXML
    private TableColumn quantKgCol;
    @FXML
    private TableColumn comprimentoCol;
    @FXML
    private TableColumn codProdutoCol;
    @FXML
    private TableColumn descProdutoCol;
    @FXML
    private TableColumn quantLiberadaCol;
    @FXML
    private TableColumn fatorCol;
    @FXML
    private StackPane fundo;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fundo.getStyleClass().add("bordePane");
        saidaGrid.getStyleClass().add("grid-pane");
        fieldDtSaida.getStyleClass().add("datepicket");
        btnSalvar.setGraphic(new ImageView(imgButtonSave));
        fieldTpOperacao.setItems(FXCollections.observableArrayList("Normal", "Transferencia",
                "Empenho", "Sobra", "Estorno", "Devolução"));

        fieldCodCliente.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumOp.requestFocus();
                }
            }
        });

        fieldNumOp.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumOf.requestFocus();
                }
            }
        });

        fieldNumOf.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldDtSaida.requestFocus();
                }
            }
        });

        fieldNumOf.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldNumPc.requestFocus();
                }
            }
        });

        fieldNumPc.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    fieldDtSaida.requestFocus();
                }
            }
        });

        fieldDtSaida.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    btnSalvar.requestFocus();
                }
            }
        });
    }

    void setFields(ControleCliente cliente1, ObservableList<MateriaisData> treatedSaidaItens,
            MateriaisPresenter aThis, int tipo) {

        this.previousController = aThis;
        this.cliente1 = cliente1;
        this.materiaisItens = treatedSaidaItens; //only that was market. But only save if something was pass !
        this.tipo = tipo;
        // settingFields

        fieldCodCliente.setText(cliente1.getCodCliente());
        fieldCodCliente.setEditable(false);

        fieldNumOp.setEditable(false);
        fieldTpOperacao.setItems(FXCollections.observableArrayList("Normal", "Transferencia",
                "Empenho", "Sobra", "Estorno", "Devolução"));
        fieldTpOperacao.setDisable(true);
        fieldTotalKg.setDisable(true);
        fieldDtSaida.setValue(LocalDate.now());

        if (tipo == 1) { //tipo normal
            fieldTpOperacao.getSelectionModel().select(0);
            fieldNumOp.setText(cliente1.getNumOp());
            fieldTransferencia.setDisable(true);
            fillOfsChoiceBox();
            treatOfsChoiceBox();

        } else if (tipo == 2) { // tipo transferencia
            fieldTpOperacao.getSelectionModel().select(1);
            fieldTransferencia.setDisable(false);
            fillTransferenciaChoiceBox();

        } else if (tipo == 3) { // tipo empenho
            fieldTpOperacao.getSelectionModel().select(2);
            fieldNumOp.setText(cliente1.getNumOp());
            fieldTransferencia.setDisable(true);
            fillOfsChoiceBox();
            treatOfsChoiceBox();
        } else if (tipo == 5) { // tipo devolução
            fieldTpOperacao.getSelectionModel().select(5);
            fieldNumOp.setText(cliente1.getNumOp());
            fieldCodCliente.setDisable(true);
            fieldTransferencia.setDisable(true);
            fieldNumOf.setDisable(true);
            fieldNumOp.setDisable(true);
            fieldNumPc.setDisable(true);
            fillOfsChoiceBox();
            treatOfsChoiceBox();
        }
        createColumns();

        btnSalvar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                //Converter Data
                //tipo == 2 &&
                
                 if (tipo == 1 && fieldNumOf.getSelectionModel().isEmpty()) {
                 Dialogs.create()
                 .title("Aviso!")
                 .message("Preencha os campos OF e PC!").showWarning();
                 } else 
                 
                if (fieldTransferencia.getSelectionModel().isEmpty() && tipo == 2) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("cliente para transferencia nao selecionado!").showWarning();
                } else if ((fieldNumOf.getSelectionModel().isEmpty() || fieldNumPc.getSelectionModel().isEmpty()) && (tipo == 3)) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Para Empenho, selecione a OF e o Plano de Corte!").showWarning();
                } else {
                    //String clienteTransferencia = null;

                    for (MateriaisData u : materiaisItens) {
                        if (u.getQuantUn().compareTo(BigDecimal.ZERO) <= 0) {
                            Dialogs.create()
                                    .title("Aviso!")
                                    .message("Algum produto com quantidade zero  ou  negativa!").showWarning();
                            return;
                        }
                    }
                    Action response = Dialogs.create()
                            .owner(btnSalvar)
                            .title("Confirmação de Saida")
                            .masthead("Deseja confirmar esta saída?")
                            .message("")
                            .showConfirm();
                    if (response == Dialog.ACTION_YES) {
                        String sdt = treatFieldDtSaida();
                        codigoControleOf = getControleOfs();
                        BigDecimal totalKg = getTotalKgFromItens();

                        Saida saida = new Saida(cliente1.getCodControle(), // esse já está ok.
                                codigoControleOf,
                                getClienteTransf(),
                                tiposSaidas[fieldTpOperacao.getSelectionModel().getSelectedIndex()],
                                totalKg, sdt);
                        if (tipo == 1) {
                            saidasService.saveNewSaida(cliente1.getCodCliente(), saida, materiaisItens);
                        } else if (tipo == 2) {
                            saidasService.saveNewSaida(cliente1.getCodCliente(), saida, materiaisItens);
                            saidasService.saveEntradaTransferencia(cliente1.getCodCliente(), saida, materiaisItens);

                        } else if (tipo == 3) {
                            //salva tambem os itens, porem nao da update oficial no estoque.
                            saidasService.saveNewEmpenho(cliente1.getCodCliente(), saida, materiaisItens);
                        } else if (tipo == 5) {
                            //salva tambem os itens, porem nao da update oficial no estoque.
                            saidasService.saveNewSaida(cliente1.getCodCliente(), saida, materiaisItens);
                        }
                        previousController.setCliente(cliente1);
                        Node source = (Node) e.getSource();
                        Stage stage = (Stage) source.getScene().getWindow();
                        stage.close();
                    } else {

                    }
                }

            }

            private int getControleOfs() {
                if (fieldTpOperacao.getSelectionModel().getSelectedIndex() != 1 && fieldTpOperacao.getSelectionModel().getSelectedIndex() != 5) {
                    for (ControleOf u : ofs) {
                        try {
                            if (u.getNumPc().equals(fieldNumPc.getSelectionModel().getSelectedItem().toString())) {
                                return u.getCodControleOf();
                            }
                        } catch (Exception a) {
                            //saiu uma excessao pois fieldNumPc esta vazio
                        }
                    }
                } else {
                    return 0;
                }
                return 0;
            }

            private String getClienteTransf() {
                if (tipo == 1) {
                    return null;
                } else if (tipo == 2) {
                    int x = fieldTransferencia.getSelectionModel().getSelectedIndex();
                    String string = fieldTransferencia.getSelectionModel().getSelectedItem().toString();
                    String clienteTransferenciaSelecionado = string.toString().substring(0, string.indexOf("-"));
                    String opClienteTransferenciaSelecionado = string.substring(string.indexOf("-") + 2);
                    String str = new String();
                    for (int i = 0; i < clientes.size(); i++) {
                        if (clienteTransferenciaSelecionado.trim().equals((clientes.get(i).getDescCliente().trim())) && opClienteTransferenciaSelecionado.trim().equals((clientes.get(i).getNumOp().trim()))) {
                            str = clientes.get(i).getCodCliente();
                        }
                    }
                    return str;
                } else {   // se for tipo3, é empenho !!!!
                    return null;
                }
            }

        });

    }

    private void createColumns() {

        codProdutoCol.setCellValueFactory(new PropertyValueFactory<>("codProduto"));
        descProdutoCol.setCellValueFactory(new PropertyValueFactory<>("descProduto"));
        quantUnCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        quantLiberadaCol.setCellValueFactory(new PropertyValueFactory<>("quantLiberada"));
        fatorCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        quantKgCol.setCellValueFactory(new PropertyValueFactory<>("QuantKg"));
        comprimentoCol.setCellValueFactory(new PropertyValueFactory<>("comprimento"));
        //dataTableViewInserir = treatedSaidaItens;
        for (int i = 0; i < materiaisItens.size(); i++) {

            itensTableView.setItems(materiaisItens);

            quantUnCol.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<BigDecimal>() {
                @Override
                public String toString(BigDecimal object) {
                    return object.toString();
                }

                @Override
                public BigDecimal fromString(String string) {
                    return new BigDecimal(string);
                }
            }));

            quantUnCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<MateriaisData, BigDecimal>>() {
                @Override

                public void handle(TableColumn.CellEditEvent<MateriaisData, BigDecimal> t) {
                    if (t.getNewValue()
                            .compareTo(t.getTableView().getItems().get(t.getTablePosition().getRow())
                                    .getQuantLiberada()) == 1) {
                        t.getTableView().getItems().get(t.getTablePosition().getRow()).setQuantUn(t.getOldValue());
                        Dialogs.create()
                                .title("Aviso!")
                                .message("Quantidade maior que a liberada para baixa. Verifique empenho!").showWarning();
                    } else {

                        BigDecimal peso = null;
                        if (t.getTableView().getItems().get(t.getTablePosition().getRow()).getSobra()) {
                            BigDecimal comprimento = t.getTableView().getItems().get(t.getTablePosition().getRow()).getComprimento();
                            BigDecimal pesoReal = nt.tamanhoReal(t.getTableView().getItems().get(t.getTablePosition().getRow()).getDescProduto());
                            BigDecimal fator = t.getTableView().getItems().get(t.getTablePosition().getRow()).getFconversao();
                            BigDecimal result;
                            result = comprimento.divide(pesoReal, BigDecimal.ROUND_HALF_UP);
                            peso = result.multiply(fator);
                        } else {
                            peso = t.getNewValue()
                                    .multiply(t.getTableView()
                                            .getItems()
                                            .get(t.getTablePosition()
                                                    .getRow())
                                            .getFconversao());
                        }
                        t.getTableView().getItems().get(t.getTablePosition().getRow()).setQuantKg(peso);
                        t.getTableView().getItems().get(t.getTablePosition().getRow()).setQuantUn(t.getNewValue());

                    }

                }
            });
        }
    }

    //methods of support.
    private void fillOfsChoiceBox() {

        try {
            ofs = FXCollections.observableArrayList(controleOfService.getOfsAtivasPorCliente(cliente1.getCodControle()));
        } catch (Exception e) {
            System.out.println("-");
        }

        if (ofs.size() != 0) {
            ofsList.add(ofs.get(0).getNumOf());
            for (int i = 1; i < ofs.size(); i++) {
                for (int x = 0; x < ofsList.size(); x++) {
                    if (ofsList.contains((ofs.get(i).getNumOf()))) {

                        continue;
                    } else {
                        ofsList.add(ofs.get(i).getNumOf());
                    }
                }
            }
            fieldNumOf.setItems(ofsList);

        } else {
            System.out.println("--!");
        }
    }

    private void treatOfsChoiceBox() {

        fieldNumOf.getSelectionModel()
                .selectedIndexProperty()
                .addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        fillPcsChoiceBox(ofsList.get(newValue.intValue()).toString());
                    }

                    private void fillPcsChoiceBox(String theOf) {
                        //percorre o ControleOf filtrando as ofs
                        //adicionando os pc que baterem no choiceBox e salvando
                        // em um array que contenha o numero do controle, que sera
                        // salvo na base de dados !!!! no botao salvar !!!
                        //fieldNumPc.setDisable(false);
                        pcsList.removeAll(ofs);
                        fieldNumPc.getItems().removeAll(pcsList);
                        for (int i = 0; i < ofs.size(); i++) {
                            if (ofs.get(i).getNumOf().equals(theOf)) {
                                pcsList.add(ofs.get(i).getNumPc());
                            }
                        }
                        itensTableView.requestFocus();
                        fieldNumPc.setItems(pcsList);
                    }

                });
    }

    public String treatFieldDtSaida() {
        String str_pattern = "yyyyMMdd";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(str_pattern);
        fieldDtSaida.setConverter(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate object) {
                return dateFormatter.format(object);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, dateFormatter);
            }
        });
        final StringConverter<LocalDate> defaultConverter = fieldDtSaida.getConverter();
        //String sdt = new String(defaultConverter.toString(fieldDtEntrada.getValue()));
        return defaultConverter.toString(fieldDtSaida.getValue());
    }

    private BigDecimal getTotalKgFromItens() {
        BigDecimal somatorioKg = BigDecimal.ZERO;
        for (MateriaisData u : materiaisItens) {
            somatorioKg = somatorioKg.add(u.getQuantKg());
        }
        return somatorioKg;
    }

    private void fillTransferenciaChoiceBox() {
        clientes = FXCollections.observableArrayList(controleClienteService.getControleCliente());
        for (int i = 0; i < clientes.size(); i++) {
            if (!clientes.get(i).getCodCliente().equals(cliente1.getCodCliente())) {
                transfClientes.add(clientes.get(i).getDescCliente().trim() + " - " + clientes.get(i).getNumOp().trim());
            }
        }
        fieldTransferencia.setItems(transfClientes);
    }

    void setFieldsEmpenhoToSaida(Saida saida, ObservableList<MateriaisData> itensEmpenho,
            SaidasPresenter aThis, int tipoSaida, List<Object> objetosSaida, List<String> saidaCampos, ControleCliente cliente) {
        this.saidas = objetosSaida;
        this.previousSaidaController = aThis;
        this.materiaisItens = itensEmpenho;
        this.tipo = tipo;

        fieldCodCliente.setText(saidaCampos.get(0));
        fieldCodCliente.setDisable(true);
        fieldNumOp.setText(saidaCampos.get(1));
        fieldNumOp.setDisable(true);
        ObservableList shit = FXCollections.observableArrayList(saidaCampos.get(2));
        fieldNumOf.setItems(shit);
        fieldNumOf.getSelectionModel().select(0);
        fieldNumOf.setDisable(true);
        ObservableList shit2 = FXCollections.observableArrayList(saidaCampos.get(3));
        fieldNumPc.setItems(shit2);
        fieldNumPc.getSelectionModel().select(0);
        fieldNumPc.setDisable(true);
        String str_pattern = "yyyyMMdd";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(str_pattern);
        fieldDtSaida.setValue(LocalDate.parse(saida.getDtSaida(), dateFormatter));
        //fieldDtSaida.setDisable(true);
        fieldTpOperacao.getSelectionModel().select(0);
        fieldTpOperacao.setDisable(true);
        fieldTotalKg.setText(saida.getTotalKg().toString());
        // o que fazer para atualizar o peso, só no final    ????? o somatorio faz a conta tranquilo e
        // atualizao na base de dados !!
        fieldTotalKg.setDisable(true);

        createColumns(); // esse paramentro nao ta servindo de navata, já está no setCliente.

        btnSalvar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                //String clienteTransferencia = null;
                for (MateriaisData u : materiaisItens) {
                    if (u.getQuantUn().compareTo(BigDecimal.ZERO) <= 0) {
                        Dialogs.create()
                                .title("Aviso!")
                                .message("Algum produto com quantidade zero  ou  negativa!").showWarning();
                        return;
                    }
                }

                String sdt = treatFieldDtSaida();
                //codigoControleOf = getControleOfs();
                BigDecimal totalKg = getTotalKgFromItens();

                Saida saida = new Saida(0, //cliente1.getCodControle(), // esse já está ok.
                        //codigoControleOf,
                        0,
                        "", //getClienteTransf(),
                        tiposSaidas[fieldTpOperacao.getSelectionModel().getSelectedIndex()],
                        totalKg, sdt);

                saidasService.updateEmpenhoToSaida(saidaCampos.get(0),
                        //cliente1.getCodCliente()
                        saida, materiaisItens, objetosSaida);
                previousSaidaController.setCliente(cliente);
                // finaliza
                //previousController.setCliente(cliente1); // tem que habilitar todos os cliente atraves de hash com controllers !!!!
                Node source = (Node) e.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
            }

            private int getControleOfs() {

                if (fieldTpOperacao.getSelectionModel().getSelectedIndex() != 1) {
                    for (ControleOf u : ofs) {
                        try {
                            if (u.getNumPc().equals(fieldNumPc.getSelectionModel().getSelectedItem().toString())) {
                                return u.getCodControleOf();
                            }
                        } catch (Exception a) {
                            Dialogs.create()
                                    .title("Aviso!")
                                    .message("Preencha o Plano de Corte!").showWarning();
                        }
                    }
                } else {
                    return 0;
                }
                return 0;
            }

            private String getClienteTransf() {
                if (tipo == 1) {
                    return null;
                } else if (tipo == 2) {
                    int x = fieldTransferencia.getSelectionModel().getSelectedIndex();
                    String clienteTransferenciaSelecionado = fieldTransferencia.getSelectionModel().getSelectedItem().toString();
                    String opClienteTransferenciaSelecionado = clienteTransferenciaSelecionado.substring(clienteTransferenciaSelecionado.indexOf("-"));
                    String str = new String();
                    for (int i = 0; i < clientes.size(); i++) {
                        if (clienteTransferenciaSelecionado.equals((clientes.get(i).getDescCliente())) && opClienteTransferenciaSelecionado.equals((clientes.get(i).getNumOp()))) {
                            str = clientes.get(i).getCodCliente();
                        }
                    }
                    return str;
                } else {   // se for tipo3, é empenho !!!!
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Você não pode Tranferir produto empenhado!").showWarning();
                    return null;
                }

            }

        });
    }
}
