package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.reports.materiais.ChamarRelatorioMateriais;
import br.org.galvanisa.presentation.materiais.ClienteMateriaisService;
import br.org.galvanisa.sysmetal.SysMetalPresenter;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.dialog.Dialogs;

public class MateriaisPresenter implements Initializable {

    private ChamarRelatorioMateriais chamarRelatorioMateriais = new ChamarRelatorioMateriais();
    private ControleCliente cliente1 = new ControleCliente();
    private List<MateriaisData> materiasList = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> materiaisCliente = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> materiaisCliente2 = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> materiaisClienteSobra = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> allMateriaisCliente = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> materiaisClienteFilter = FXCollections.observableArrayList();
    private final ClienteMateriaisService materiaisService = new ClienteMateriaisService();
    private SysMetalPresenter previusController;
    Stage stage1 = null;

    @FXML
    private VBox vboxTab;
    @FXML
    private Label descricaoLabel;
    @FXML
    private Label contratoLabel;
    @FXML
    private Label opLabel;
    @FXML
    private Label codClienteLabel;
    @FXML
    private CheckBox sobrasFilter;
    @FXML
    private TableView materiaisTableView;
    @FXML
    private TableColumn codigoCol;
    @FXML
    private TableColumn descricaoCol;
    @FXML
    private TableColumn kgCol;
    @FXML
    private TableColumn fatorCol;
    @FXML
    private TableColumn unCol;
    @FXML
    private TableColumn empenhoCol;
    @FXML
    private TableColumn liberadoCol;
    @FXML
    private TableColumn comprimentoCol;
    @FXML
    private TableColumn sobraCol;
    @FXML
    private TableColumn escolhaCol;
    @FXML
    private MenuItem acaoEntradaNormal;
    @FXML
    private MenuItem acaoSaidaNormal;
    @FXML
    private MenuItem acaoSaidaTransferencia;
    @FXML
    private MenuItem acaoCliente;
    @FXML
    private MenuItem acaoSaidaEmpenho;
    @FXML
    private MenuButton acoesMenuButton;
    @FXML
    private MenuItem acaoEntradaSobra;
    @FXML
    private TableView sobrasTableView;
    @FXML
    private TableColumn sobraCodigoCol;
    @FXML
    private TableColumn sobraDescricaoCol;
    @FXML
    private TableColumn sobraKgCol;
    @FXML
    private TableColumn sobraFatorCol;
    @FXML
    private TableColumn sobraUniadeCol;
    @FXML
    private TableColumn sobraEmpenhoCol;
    @FXML
    private TableColumn sobraLiberadoCol;
    @FXML
    private TableColumn sobraComprimentoCol;
    @FXML
    private TableColumn sobraSobraCol;
    @FXML
    private TableColumn sobraEscolhaCol;
    @FXML
    private TextField findTextField;
    @FXML
    private Button relatorioButton;
    @FXML
    private MenuItem acaoSaidaDevolucao;
    @FXML
    private Label headLabel1;
    @FXML
    private Label headLabel2;
    @FXML
    private Label headLabel3;
    @FXML
    private Label headLabel4;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        headLabel1.getStyleClass().add("headLabel");
        headLabel2.getStyleClass().add("headLabel");
        headLabel3.getStyleClass().add("headLabel");
        headLabel4.getStyleClass().add("headLabel");
        materiaisTableView.getStyleClass().add("table");
        sobrasTableView.getStyleClass().add("table");
        vboxTab.getStyleClass().add("bordePane");

        initFilter();
        Image imgAcoesMenuButton = new Image(getClass().getResourceAsStream("/view/icons/Config.png"));
        acoesMenuButton.setGraphic(new ImageView(imgAcoesMenuButton));
        Image imgRelMenuButton = new Image(getClass().getResourceAsStream("/view/icons/rel.png"));
        relatorioButton.setGraphic(new ImageView(imgRelMenuButton));
        sobrasFilter.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (sobrasFilter.isSelected()) {
                    materiasList = materiaisCliente.stream().filter(p -> p.getSobra() == true)
                            .collect(Collectors.<MateriaisData>toList());
                    materiaisClienteFilter = FXCollections.observableArrayList(materiasList);
                    materiaisTableView.setItems(applyFilters(materiaisClienteFilter));
                } else if (!sobrasFilter.isSelected()) {
                    materiaisTableView.setItems(applyFilters(materiaisCliente));
                }
            }
        });

        relatorioButton.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {

                allMateriaisCliente = materiaisService.loadClienteMateriais(cliente1.getCodCliente());
                try {
                    chamarRelatorioMateriais.chamarRelatorioSaidasClass(cliente1.getCodControle(), allMateriaisCliente, cliente1.getDescCliente());
                } catch (JRException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        acaoEntradaNormal.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                int tipoEntrada = 1;
                // tem que arranjar um jeito mais facil de passar esse parametro.
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroEntrada.fxml"));
                Parent root = null;
                stage1 = new Stage();
                try {
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                CadastroEntradaPresenter controller = loader.<CadastroEntradaPresenter>getController();
                controller.setCliente(cliente1, MateriaisPresenter.this, tipoEntrada);
                Scene scene = new Scene(root);
                stage1.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Entrada - Entrada Normal");
                stage1.initOwner(acoesMenuButton.getScene().getWindow());
                stage1.initModality(Modality.APPLICATION_MODAL);
                stage1.setScene(scene);
                stage1.show();
            }
        });

        acaoEntradaSobra.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                int tipoEntrada = 2;
                // tem que arranjar um jeito mais facil de passar esse parametro.
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroEntrada.fxml"));
                Parent root = null;
                stage1 = new Stage();
                try {
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                CadastroEntradaPresenter controller = loader.<CadastroEntradaPresenter>getController();
                controller.setCliente(cliente1, MateriaisPresenter.this, tipoEntrada);
                Scene scene = new Scene(root);
                stage1.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Entrada - Sobra");
                stage1.initOwner(acoesMenuButton.getScene().getWindow());
                stage1.initModality(Modality.APPLICATION_MODAL);
                stage1.setScene(scene);
                stage1.show();
            }
        });

        acaoSaidaNormal.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // tem que arranjar um jeito mais facil de passar esse parametro.
                if (materiaisService._treatEstoqueNovaSaida(materiaisCliente2).isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Nenhum material selecionado!").showWarning();
                } else {
                    int tipoSaida = 1; // saida Normal
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroSaida.fxml"));
                    Parent root = null;
                    Stage stage = new Stage();
                    try {
                        root = loader.load();
                    } catch (IOException ex) {
                        Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    CadastroSaidaPresenter controller = loader.<CadastroSaidaPresenter>getController();
                    //controller.setFields(cliente1, materiaisService
                    //         .treatEstoqueNovaSaida(materiaisDoCLiente), MateriaisPresenter.this);
                    controller.setFields(cliente1, materiaisService
                            ._treatEstoqueNovaSaida(materiaisCliente2), MateriaisPresenter.this, tipoSaida);
                    /*
                     for(MateriaisData x : list1)
                     System.out.println(x.getEscolha());
                     */
                    Scene scene = new Scene(root);
                    stage.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Saida - Saida Normal");
                    stage.initOwner(acoesMenuButton.getScene().getWindow());
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.show();
                }
            }
        });

        acaoSaidaTransferencia.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Boolean isSobra = false;
                Boolean notIsSobra = false;
                if (materiaisService._treatEstoqueNovaSaida(materiaisCliente2).isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Nenhum material selecionado!").showWarning();
                } else if (!materiaisService._treatEstoqueNovaSaida(materiaisCliente2).isEmpty()) {
                    for (int i = 0; i < materiaisService._treatEstoqueNovaSaida(materiaisCliente2).size(); i++) {
                        if (materiaisService._treatEstoqueNovaSaida(materiaisCliente2).get(i).getSobra()) {
                            isSobra = true;
                        } else if (!materiaisService._treatEstoqueNovaSaida(materiaisCliente2).get(i).getSobra()) {
                            notIsSobra = true;
                        }
                    }
                    if (isSobra && notIsSobra) {
                        Dialogs.create()
                                .title("Aviso!")
                                .message("Sobras devem ser transferidas separadamente!").showWarning();
                    } else {
                        int tipoSaida = 2; // transferencia
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroSaida.fxml"));
                        Parent root = null;
                        Stage stage = new Stage(StageStyle.DECORATED);
                        try {
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        CadastroSaidaPresenter controller = loader.<CadastroSaidaPresenter>getController();
                        controller.setFields(cliente1, materiaisService
                                ._treatEstoqueNovaSaida(materiaisCliente2), MateriaisPresenter.this, tipoSaida);
                        Scene scene = new Scene(root);
                        stage.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Saida - Transferência");
                        stage.initOwner(acoesMenuButton.getScene().getWindow());
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }
                }
            }
        });

        acaoSaidaEmpenho.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (materiaisService._treatEstoqueNovaSaida(materiaisCliente2).isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Nenhum material selecionado!").showWarning();
                } else {
                    int tipoSaida = 3; // Saida Empenho !
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroSaida.fxml"));
                    Parent root = loader.getController();
                    Stage stage = new Stage();
                    try {
                        root = loader.load();
                    } catch (IOException ex) {
                        Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    CadastroSaidaPresenter controller = loader.<CadastroSaidaPresenter>getController();
                    controller.setFields(cliente1, materiaisService
                            ._treatEstoqueNovaSaida(materiaisCliente2), MateriaisPresenter.this, tipoSaida);
                    Scene scene = new Scene(root);
                    stage.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Saida - Empenho");
                    stage.initOwner(acoesMenuButton.getScene().getWindow());
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.show();
                }
            }
        });

        acaoSaidaDevolucao.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (materiaisService._treatEstoqueNovaSaida(materiaisCliente2).isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Nenhum material selecionado!").showWarning();
                } else {
                    int tipoSaida = 5; // Saida Empenho !
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroSaida.fxml"));
                    Parent root = loader.getController();
                    Stage stage = new Stage();
                    try {
                        root = loader.load();
                    } catch (IOException ex) {
                        Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    CadastroSaidaPresenter controller = loader.<CadastroSaidaPresenter>getController();
                    controller.setFields(cliente1, materiaisService
                            ._treatEstoqueNovaSaida(materiaisCliente2), MateriaisPresenter.this, tipoSaida);
                    Scene scene = new Scene(root);
                    stage.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Nova Saida - Devolução");
                    stage.initOwner(acoesMenuButton.getScene().getWindow());
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.show();
                }
            }
        });

        acaoCliente.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Clientes.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                ControleClientePresenter controller = loader.<ControleClientePresenter>getController();
                controller.populateTableView();
                Scene scene = new Scene(root);
                stage.setTitle("Clientes Cadastrados");
                stage.initOwner(acoesMenuButton.getScene().getWindow());
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            }
        });

        materiaisTableView.addEventFilter(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                //System.out.println("item: "+ ++i);
                /*
                 System.out.println("linha Selecionada            "+
                 materiaisTableView.getSelectionModel().getTableView().getItems());
                 System.out.println("Aqui "+codigoCol.getCellData(0));   
                 */
                // materiaisTableView.getItems().
                //System.out.println(materiaisTableView.onMouseMovedProperty().getValue()+"");
            }
        });

        materiaisTableView.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

            }
        });

    }

    private void initFilter() {
        //produtosSearchField = TextFields.createSearchField();
        findTextField.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (findTextField.textProperty().get().isEmpty()) {
                    materiaisTableView.setItems(materiaisCliente);
                    return;
                }
                ObservableList<MateriaisData> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<MateriaisData, ?>> cols = materiaisTableView.getColumns();

                for (int i = 0; i < materiaisCliente.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(materiaisCliente.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(findTextField.textProperty().get().toUpperCase())) {
                            tableItems.add(materiaisCliente.get(i));
                            break;
                        }
                    }
                }
                materiaisTableView.setItems(tableItems);
            }
        });

        findTextField.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (findTextField.textProperty().get().isEmpty()) {
                    sobrasTableView.setItems(materiaisClienteSobra);
                    return;
                }
                ObservableList<MateriaisData> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<MateriaisData, ?>> cols = sobrasTableView.getColumns();

                for (int i = 0; i < materiaisClienteSobra.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(materiaisClienteSobra.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(findTextField.textProperty().get().toUpperCase())) {
                            tableItems.add(materiaisClienteSobra.get(i));
                            break;
                        }
                    }
                }
                sobrasTableView.setItems(tableItems);
            }
        });
    }

    public void setCliente(ControleCliente cliente) {
        this.cliente1 = cliente;

        materiaisCliente.clear();
        materiaisCliente2.clear();
        materiaisClienteFilter.clear();
        codClienteLabel.setText(cliente.getCodCliente());
        descricaoLabel.setText(cliente.getDescCliente());
        contratoLabel.setText(cliente.getNumContrato());
        opLabel.setText(cliente.getNumOp());
        createFilters();
        materiaisTableView.getItems().clear();
        sobrasTableView.getItems().clear();
        populateTableView(cliente.getCodCliente());
        populateTableViewSobras(cliente.getCodCliente());

    }

    private void createFilters() {

    }

    private void populateTableView(String codCliente) {

        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codProduto"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory<>("descProduto"));
        kgCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        fatorCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        unCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        empenhoCol.setCellValueFactory(new PropertyValueFactory<>("QuantEmpenho"));
        liberadoCol.setCellValueFactory(new PropertyValueFactory<>("QuantLiberada"));
        comprimentoCol.setCellValueFactory(new PropertyValueFactory<>("comprimento"));
        sobraCol.setCellValueFactory(new PropertyValueFactory<>("sobra"));
        escolhaCol.setCellValueFactory(new PropertyValueFactory<MateriaisData, Boolean>("escolha"));
        escolhaCol.setEditable(true);
        escolhaCol.setCellFactory(CheckBoxTableCell.forTableColumn(escolhaCol));
        materiaisCliente = materiaisService.loadClienteMateriaisSobrasOff(codCliente);
        materiaisCliente2.addAll(materiaisCliente);
        materiaisTableView.setItems(materiaisCliente);

    }

    private void populateTableViewSobras(String codCliente) {

        sobraCodigoCol.setCellValueFactory(new PropertyValueFactory<>("codProduto"));
        sobraDescricaoCol.setCellValueFactory(new PropertyValueFactory<>("descProduto"));
        sobraKgCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        sobraFatorCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        sobraUniadeCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        sobraEmpenhoCol.setCellValueFactory(new PropertyValueFactory<>("QuantEmpenho"));
        sobraLiberadoCol.setCellValueFactory(new PropertyValueFactory<>("QuantLiberada"));
        sobraComprimentoCol.setCellValueFactory(new PropertyValueFactory<>("comprimento"));
        sobraSobraCol.setCellValueFactory(new PropertyValueFactory<>("sobra"));
        sobraEscolhaCol.setCellValueFactory(new PropertyValueFactory<MateriaisData, Boolean>("escolha"));
        sobraEscolhaCol.setEditable(true);
        sobraEscolhaCol.setCellFactory(CheckBoxTableCell.forTableColumn(sobraEscolhaCol));
        materiaisClienteSobra = materiaisService.loadClienteMateriaisSobras(codCliente);
        materiaisCliente2.addAll(materiaisClienteSobra);
        sobrasTableView.setItems(materiaisClienteSobra);
        //*/
    }

    private ObservableList applyFilters(ObservableList<MateriaisData> materiaisdoCliente) {
        // futuros 
        return materiaisdoCliente;
    }

    public void setCliente(ControleCliente cliente, SysMetalPresenter aThis) {
        this.previusController = aThis;
        this.setCliente(cliente);
    }

    void _setCliente(ControleCliente cliente1) throws IOException {
        this.previusController.loadMateriais();
        this.previusController.setTab(cliente1.getDescCliente());

    }
}
