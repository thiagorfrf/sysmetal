package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.produtos.ProdutosSearch;
import br.org.galvanisa.business.reports.transfer.ChamarRelatoriosGerais;
import br.org.galvanisa.presentation.produtos.LoadProdutosTerceiroService;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;

public class ProdutosPresenter implements Initializable {

    private ObservableList<ProdutosSearch> searchList = FXCollections.observableArrayList();
    private CadastroEntradaPresenter previousController;
    private LoadProdutosTerceiroService loadProdutosService = new LoadProdutosTerceiroService();
    private ObservableList<Produtos> produtos = loadProdutosService.loadProdutos();
    private ChamarRelatoriosGerais chamarRelatoriosGerais = new ChamarRelatoriosGerais();
    static String codCliente;
    static int codControleCliente;
    static Boolean relatorio;

    @FXML
    private VBox produtosVBox;
    @FXML
    private TextField produtosSearchField;
    @FXML
    private TableView<ProdutosSearch> produtosTableView;
    @FXML
    private TableColumn<ProdutosSearch, String> codigoCol;
    @FXML
    private TableColumn<ProdutosSearch, String> descricaoCol;
    @FXML
    private TableColumn<ProdutosSearch, ObjectProperty<BigDecimal>> fconversaoCol;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        produtosVBox.getStyleClass().add("bordePane");
        initFilter();
        initTable();

        produtosTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {

                    if (relatorio == null || !relatorio) {
                        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                            previousController.setProdutoField(
                                    produtosTableView.getSelectionModel().getSelectedItem().getCodigo(),
                                    produtosTableView.getSelectionModel().getSelectedItem().getDescricao(),
                                    produtosTableView.getSelectionModel().getSelectedItem().getFconversao());
                            Node source = (Node) mouseEvent.getSource();
                            Stage stage = (Stage) source.getScene().getWindow();
                            stage.close();
                        }
                    }
                } else if (relatorio) {

                    if (produtosTableView.getSelectionModel().getSelectedItem() == null) {
                        System.out.println("Selecione Novamente");
                    } else {
                        Node source = (Node) mouseEvent.getSource();
                        Stage stage = (Stage) source.getScene().getWindow();
                        stage.close();
                        try {
                            if (produtosTableView.getSelectionModel().getSelectedItem().getCodigo() != null) {
                                chamarRelatoriosGerais.chamarRelatorioProdutosMovimentacao(produtosTableView.getSelectionModel().getSelectedItem().getCodigo(),
                                        codCliente, codControleCliente);
                            }

                        } catch (JRException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (SQLException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
    }

    private void initFilter() {
        //produtosSearchField = TextFields.createSearchField();
        produtosSearchField.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (produtosSearchField.textProperty().get().isEmpty()) {
                    produtosTableView.setItems(searchList);
                    return;
                }
                ObservableList<ProdutosSearch> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<ProdutosSearch, ?>> cols = produtosTableView.getColumns();

                for (int i = 0; i < searchList.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(searchList.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(produtosSearchField.textProperty().get().toUpperCase())) {
                            tableItems.add(searchList.get(i));
                            break;
                        }
                    }

                }
                produtosTableView.setItems(tableItems);
            }
        });

    }

    private void initTable() {

        codigoCol.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<ProdutosSearch, String>, ObservableValue<String>>() {

                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ProdutosSearch, String> p) {
                        return p.getValue().codigoProperty();
                    }
                });

        descricaoCol.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<ProdutosSearch, String>, ObservableValue<String>>() {

                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ProdutosSearch, String> p) {
                        return p.getValue().descricaoProperty();
                    }
                });

        fconversaoCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        searchList = populateSearchList();
        produtosTableView.setItems(FXCollections.observableArrayList(searchList));

    }

    private ObservableList<ProdutosSearch> populateSearchList() {
        ObservableList<ProdutosSearch> list = FXCollections.observableArrayList();

        for (int i = 0; i < produtos.size(); i++) {
            ProdutosSearch swap = new ProdutosSearch();
            swap.setCodigo(produtos.get(i).getCodigo());
            swap.setDescricao(produtos.get(i).getDescricao());
            swap.setFconversao(produtos.get(i).getFconversao());
            list.add(swap);
        }
        return list;
    }

    void returnProduto(CadastroEntradaPresenter aThis) {
        this.previousController = aThis;
    }

    public void setCliente(String codCliente, int codControleCliente, Boolean relatorio) {
        this.relatorio = Boolean.TRUE;
        this.codCliente = codCliente;
        this.codControleCliente = codControleCliente;
    }

    public void setRelatorioTrue() {
        relatorio = Boolean.FALSE;
    }
}
