package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.entrada.EntradaBusinessEntity;
import br.org.galvanisa.business.entity.produtosdescricao.ProdutosDescricao;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.business.reports.entradas.ChamarRelatorioClienteEntradas;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.entradaitens.EntradaItensService;
import br.org.galvanisa.presentation.entradas.ClienteEntradasService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

/**
 *
 * @author eric
 */
public class EntradasPresenter implements Initializable {

    private ControleClienteService controleClienteService = new ControleClienteService();
    private EntradaItensService entradaItensService = new EntradaItensService();
    private ClienteEntradasService clienteEntradasService = new ClienteEntradasService();
    private EntradasModel clienteEntradasModel = new EntradasModel();
    private ChamarRelatorioClienteEntradas chamarRelatorioClienteEntradas = new ChamarRelatorioClienteEntradas();
    private MateriaisPresenter materiaisPresenter = new MateriaisPresenter();
    private ControleCliente cliente1 = new ControleCliente();
    private EntradaBusinessEntity entrada = new EntradaBusinessEntity();
    private EntradaBusinessEntity entrada2 = new EntradaBusinessEntity();
    private ObservableList<ProdutosDescricao> entradaItens = FXCollections.observableArrayList();
    private ObservableList<EntradaBusinessEntity> entradaItensEstorno = FXCollections.observableArrayList();
    private ObservableList<EntradaBusinessEntity> clienteEntradas = FXCollections.observableArrayList();
    private ObservableList<EntradaBusinessEntity> clienteEntradas2 = FXCollections.observableArrayList();
    private ObservableList<EntradaBusinessEntity> clienteEntradasFilter = FXCollections.observableArrayList();
    public String filtrosSelecionados = "Todas";

    @FXML
    private VBox vboxTab;
    @FXML
    private Label codClienteLabel;
    @FXML
    private Label descricaoLabel;
    @FXML
    private Label contratoLabel;
    @FXML
    private Label opLabel;
    @FXML
    public CheckBox sobrasFilter;
    @FXML
    public CheckBox transferenciaFilter;
    @FXML
    public CheckBox normalFilter;
    @FXML
    private TableView entradasTableView;
    @FXML
    private TableColumn entradaCol;
    @FXML
    private TableColumn nfCol;
    @FXML
    private TableColumn fornecedorCol;
    @FXML
    private TableColumn certificadoCol;
    @FXML
    private TableColumn tipoOperacaoCol;
    @FXML
    private TableColumn totalKgCol;
    @FXML
    private TableColumn dataEntradaCol;
    @FXML
    private MenuItem relTotalEntradas;
    @FXML
    private TableColumn codProdutoCol;
    @FXML
    private TableColumn descricaoCol;
    @FXML
    private TableColumn totalEntradaCol;
    @FXML
    private TableColumn quantidadeEntradaCol;
    @FXML
    private TableView<ProdutosDescricao> produtosEntradaTableView;
    @FXML
    private MenuButton acoesMenuButton;
    @FXML
    private MenuItem estornarEntrada;
    @FXML
    private Button relatoriosButton;
    @FXML
    private CheckBox empenhoFilter;
    @FXML
    private Label headLabel1;
    @FXML
    private Label headLabel2;
    @FXML
    private Label headLabel3;
    @FXML
    private Label headLabel4;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        headLabel1.getStyleClass().add("headLabel");
        headLabel2.getStyleClass().add("headLabel");
        headLabel3.getStyleClass().add("headLabel");
        headLabel4.getStyleClass().add("headLabel");
        entradasTableView.getStyleClass().add("table");
        produtosEntradaTableView.getStyleClass().add("table");
        vboxTab.getStyleClass().add("bordePane");

        Image imgAcoesMenuButton = new Image(getClass().getResourceAsStream("/view/icons/Config.png"));
        Image imgRelMenuButton = new Image(getClass().getResourceAsStream("/view/icons/rel.png"));
        relatoriosButton.setGraphic(new ImageView(imgRelMenuButton));
        acoesMenuButton.setGraphic(new ImageView(imgAcoesMenuButton));
        empenhoFilter.setVisible(false);

        estornarEntrada.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                entrada2 = (EntradaBusinessEntity) entradasTableView.getSelectionModel().getSelectedItem();
                if (entradasTableView.getSelectionModel().getSelectedItem() == null) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Por favor, selecione uma das entradas!").showWarning();
                } else if (entrada2.getTipoOperacao() == 'T') {
                    Dialogs.create()
                            .title("Aviso!").masthead("Atenção")
                            .message("Estornos de transferências devem ser realizados no cliente que as originou!").showInformation();
                } else {
                    
                    Action response = Dialogs.create()
                            .owner(acoesMenuButton)
                            .title("Confirmação de Estorno")
                            .masthead("Deseja realmente estornar a Entrada abaixo?")
                            .message("Cod Entrada : " + entrada2.getCodEntrada() + " com o peso total de : " + entrada2.getTotalKg())
                            .showConfirm();

                    if (response == Dialog.ACTION_YES) {

                        entradaItensService.estornoEntradaApply(entrada2, produtosEntradaTableView.getItems());
                        setCliente(cliente1);
                        produtosEntradaTableView.getItems().clear();

                    } else {

                    }
                            
                }
            }
        });

        relTotalEntradas.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    chamarRelatorioClienteEntradas.chamarRelatorioEntradasCliente(cliente1.getCodCliente());
                } catch (JRException ex) {
                    Logger.getLogger(EntradasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(EntradasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(EntradasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        sobrasFilter.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                populateTableViewFilter(cliente1.getCodCliente());

            }
        });

        transferenciaFilter.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                populateTableViewFilter(cliente1.getCodCliente());
            }
        });

        normalFilter.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                populateTableViewFilter(cliente1.getCodCliente());

            }
        });
        empenhoFilter.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                populateTableViewFilter(cliente1.getCodCliente());

            }
        });

        entradasTableView.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                setProdutosInTable();
            }
        });
        entradasTableView.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override

            public void handle(KeyEvent keyevent) {

                setProdutosInTable();
            }
        });

        relatoriosButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                List entradasList = new ArrayList();
                entradasList = entradasTableView.getItems();

                int cod = cliente1.getCodControle();
                System.out.println("cliente 1 " + cliente1.getCodCliente());
                try {
                    chamarRelatorioClienteEntradas.chamarRelatorioEntradasClass(cod, entradasList, filtrosSelecionados, cliente1.getDescCliente());
                } catch (JRException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    public void setProdutosInTable() {
        if (entradasTableView.getSelectionModel().getSelectedItem() == null) {
            //Botar um Dialog aqui
            //Nenhum Item Selecionado
            System.out.println("--");
        } else {
            produtosEntradaTableView.getItems().removeAll(entradaItens);
            entrada = (EntradaBusinessEntity) entradasTableView.getSelectionModel().getSelectedItem();
            entradaItens = entradaItensService.loadEntradaItens(entrada.getCodEntrada());
            populateTableViewEntradaItens(entradaItens);
        }
    }

    public void setCliente(ControleCliente cliente) {
        this.cliente1 = cliente;
        if (!cliente1.getTerceiro()) {
            sobrasFilter.setVisible(false);
            normalFilter.setVisible(false);
            transferenciaFilter.setVisible(false);
        }
        codClienteLabel.setText(cliente.getCodCliente());
        descricaoLabel.setText(cliente.getDescCliente());
        contratoLabel.setText(cliente.getNumContrato());
        opLabel.setText(cliente.getNumOp());
        populateTableView(cliente.getCodCliente());
    }

    private void populateTableView(String codCliente) {
        entradaCol.setCellValueFactory(new PropertyValueFactory<>("codEntrada"));
        nfCol.setCellValueFactory(new PropertyValueFactory<>("numNf"));
        fornecedorCol.setCellValueFactory(new PropertyValueFactory<>("descFornecedor"));
        certificadoCol.setCellValueFactory(new PropertyValueFactory<>("numCertificado"));
        tipoOperacaoCol.setCellValueFactory(new PropertyValueFactory<>("tipoOperacaoString"));
        totalKgCol.setCellValueFactory(new PropertyValueFactory<>("totalKg"));
        dataEntradaCol.setCellValueFactory(new PropertyValueFactory<>("dataFormatada"));
        entradasTableView.setItems(getClientesTableViewFilter(codCliente));
    }

    private void populateTableViewFilter(String codCliente) {
        entradaCol.setCellValueFactory(new PropertyValueFactory<>("codEntrada"));
        nfCol.setCellValueFactory(new PropertyValueFactory<>("numNf"));
        fornecedorCol.setCellValueFactory(new PropertyValueFactory<>("descFornecedor"));
        certificadoCol.setCellValueFactory(new PropertyValueFactory<>("numCertificado"));
        tipoOperacaoCol.setCellValueFactory(new PropertyValueFactory<>("tipoOperacaoString"));
        totalKgCol.setCellValueFactory(new PropertyValueFactory<>("totalKg"));
        dataEntradaCol.setCellValueFactory(new PropertyValueFactory<>("dataFormatada"));
        entradasTableView.setItems(getClientesTableViewFilter(codCliente));
        produtosEntradaTableView.getItems().clear();
    }

    private void populateTableViewEntradaItens(ObservableList<ProdutosDescricao> obj) {

        codProdutoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        totalEntradaCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        quantidadeEntradaCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        produtosEntradaTableView.setItems(obj);

    }

    List<EntradaBusinessEntity> getEntradasPorTipo(List<EntradaBusinessEntity> entradas, char tipoOperacao) {
        return entradas.stream().filter(p -> p.getTipoOperacao() == tipoOperacao)
                .collect(Collectors.<EntradaBusinessEntity>toList());
    }

    private ObservableList<EntradaBusinessEntity> getClientesTableViewFilter(String codCliente) {
        filtrosSelecionados = "Todas";
        clienteEntradas = clienteEntradasService.loadClienteEntradasFilter(codCliente);

        if (sobrasFilter.isSelected() && !normalFilter.isSelected() && !transferenciaFilter.isSelected() && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Sobras";
            return clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'S'));
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Transferências";
            return clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'T'));
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Normal";
            return clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'N'));
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            filtrosSelecionados = "Empenho";
            return clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'E'));
            //Normal e transferencia selecionados
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {

            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'N'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'T'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Normal e Traneferências";
            return clienteEntradasFilter;
            //Normal e Sobra Selecionados    
        } else if (sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {

            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'N'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'S'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Normal e Sobras";
            return clienteEntradasFilter;
            // Normal e Estorno Selecionados
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'N'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'E'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Normal e Empenho";
            return clienteEntradasFilter;
            //Sobra e Transferencia Selecionados
        } else if (sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'S'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'T'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Sobras e Transferências";
            return clienteEntradasFilter;
            //Sobra e Estorno Selecionados
        } else if (sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'S'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'E'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Sobras e Empenhos";
            return clienteEntradasFilter;
            //Transferencia e Estorno Selecionados
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            clienteEntradasFilter = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'E'));
            clienteEntradas2 = FXCollections.observableArrayList(getEntradasPorTipo(clienteEntradas, 'T'));
            clienteEntradasFilter.addAll(clienteEntradas2);
            filtrosSelecionados = "Empenhos e Transferências";
            return clienteEntradasFilter;
        }
        return clienteEntradas;
    }
}
