package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.estoquegalvanisa.ProdutosGalvanisa;
import br.org.galvanisa.business.entity.pedidocompra.ProdutosPedidoCompra;
import br.org.galvanisa.business.entity.produtos.ProdutosEstoqueTerceiros;
import br.org.galvanisa.presentation.estoquegalvanisa.EstoqueGalvanisaService;
import br.org.galvanisa.presentation.pedidocompra.PedidoCompraService;
import br.org.galvanisa.presentation.produtos.LoadProdutosTerceiroService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author eric
 */
public class EstoqueGalvanisaPresenter implements Initializable {

    private ObservableList<ProdutosPedidoCompra> searchListPedidoCompras = FXCollections.observableArrayList();
    private ObservableList<ProdutosGalvanisa> searchList = FXCollections.observableArrayList();
    private ObservableList<ProdutosEstoqueTerceiros> searchListTerceiros = FXCollections.observableArrayList();
    private EstoqueGalvanisaService estoqueGalvanisaService = new EstoqueGalvanisaService();
    private LoadProdutosTerceiroService loadProdutosService = new LoadProdutosTerceiroService();
    private PedidoCompraService pedidoCompraService = new PedidoCompraService();

    @FXML
    private TextField fieldSearch;
    @FXML
    private TableView<ProdutosGalvanisa> produtosTableView;
    @FXML
    private TableColumn<ProdutosGalvanisa, String> codigoCol;
    @FXML
    private TableColumn<ProdutosGalvanisa, String> descricaoCol;
    @FXML
    private TableColumn estoqueKgCol;
    @FXML
    private TableColumn fatorCol;
    @FXML
    private TableColumn estoqueUnCol;
    @FXML
    private TableColumn empKgCol;
    @FXML
    private TableColumn empUnCol;
    @FXML
    private TableColumn quantLibUnCol;
    @FXML
    private TableView<ProdutosEstoqueTerceiros> terceirosTableView;
    @FXML
    private TableColumn clenteTerceiroCol;
    @FXML
    private TableColumn<ProdutosEstoqueTerceiros, String> codigoterceiroCol;
    @FXML
    private TableColumn<ProdutosEstoqueTerceiros, String> descricaoTerceiroCol;
    @FXML
    private TableColumn kgTerceiroCol;
    @FXML
    private TableColumn fatorTerceiroCol;
    @FXML
    private TableColumn unidadeTerceiroCol;
    @FXML
    private TableColumn previsaoComprasCol;
    @FXML
    private TableColumn codigoComprasCol;
    @FXML
    private TableColumn descricaoComprasCol;
    @FXML
    private TableColumn qtdPedidoKgCol;
    @FXML
    private TableColumn qtdPedidoUniCol;
    @FXML
    private TableColumn kgSolicitadaCol;
    @FXML
    private TableColumn numPedidoCol;
    @FXML
    private TableView<ProdutosPedidoCompra> pedidoComprasTableView;
    @FXML
    private VBox estoqueGalvanisaVBox;
    @FXML
    private HBox fundo;
    @FXML
    private Label titulo1;
    @FXML
    private Label titulo2;
    @FXML
    private Label titulo3;
    @FXML
    private Label titulo4;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        titulo1.getStyleClass().add("titulo");
        titulo2.getStyleClass().add("titulo");
        titulo3.getStyleClass().add("titulo");
        titulo4.getStyleClass().add("titulo");
        qtdPedidoUniCol.getStyleClass().setAll("colunaColorida", "column-header1");
        estoqueUnCol.getStyleClass().setAll("colunaColorida", "column-header1");
        unidadeTerceiroCol.getStyleClass().setAll("colunaColorida", "column-header1");

        initFilter();
        initTable();
        initTableTerceiros();
        initFilterTerceiros();
        initTableCompras();
        initFilterPedidosCompras();
        //setFocusInTables();
    }

    private void initFilter() {
        //produtosSearchField = TextFields.createSearchField();
        fieldSearch.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (fieldSearch.textProperty().get().isEmpty()) {
                    produtosTableView.setItems(searchList);
                    return;
                }
                ObservableList<ProdutosGalvanisa> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<ProdutosGalvanisa, ?>> cols = produtosTableView.getColumns();

                for (int i = 0; i < searchList.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(searchList.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(fieldSearch.textProperty().get().toUpperCase())) {
                            tableItems.add(searchList.get(i));
                            break;
                        }
                    }
                }
                produtosTableView.setItems(tableItems);
            }
        });
    }

    private void initFilterTerceiros() {
        //produtosSearchField = TextFields.createSearchField();
        fieldSearch.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (fieldSearch.textProperty().get().isEmpty()) {
                    terceirosTableView.setItems(searchListTerceiros);
                    return;
                }
                ObservableList<ProdutosEstoqueTerceiros> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<ProdutosEstoqueTerceiros, ?>> cols = terceirosTableView.getColumns();

                for (int i = 0; i < searchListTerceiros.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(searchListTerceiros.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(fieldSearch.textProperty().get().toUpperCase())) {
                            tableItems.add(searchListTerceiros.get(i));
                            break;
                        }
                    }
                }
                terceirosTableView.setItems(tableItems);
            }
        });
    }

    private void initFilterPedidosCompras() {
        //produtosSearchField = TextFields.createSearchField();
        fieldSearch.textProperty().addListener(new InvalidationListener() {

            @Override
            public void invalidated(Observable o) {
                if (fieldSearch.textProperty().get().isEmpty()) {
                    pedidoComprasTableView.setItems(searchListPedidoCompras);
                    return;
                }
                ObservableList<ProdutosPedidoCompra> tableItems = FXCollections.observableArrayList();
                ObservableList<TableColumn<ProdutosPedidoCompra, ?>> cols = pedidoComprasTableView.getColumns();

                for (int i = 0; i < searchListPedidoCompras.size(); i++) {

                    for (int j = 0; j < cols.size(); j++) {
                        TableColumn col = cols.get(j);
                        String cellValue = col.getCellData(searchListPedidoCompras.get(i)).toString();
                        cellValue = cellValue.toUpperCase();
                        if (cellValue.contains(fieldSearch.textProperty().get().toUpperCase())) {
                            tableItems.add(searchListPedidoCompras.get(i));
                            break;
                        }
                    }
                }
                pedidoComprasTableView.setItems(tableItems);
            }
        });
    }

    private void populateSearhList() {
        searchList = estoqueGalvanisaService.getProdutosGalvanisa();
        searchListTerceiros = loadProdutosService.loadProdutosProperty();
        searchListPedidoCompras = pedidoCompraService.getPedidosCompras();

    }

    private void initTable() {
        codigoCol.setCellValueFactory(
                (TableColumn.CellDataFeatures<ProdutosGalvanisa, String> p) -> p.getValue().codigoProperty());
        descricaoCol.setCellValueFactory(
                (TableColumn.CellDataFeatures<ProdutosGalvanisa, String> p) -> p.getValue().descricaoProperty());
        estoqueKgCol.setCellValueFactory(new PropertyValueFactory<>("estoqueKg"));
        fatorCol.setCellValueFactory(new PropertyValueFactory<>("fator"));
        estoqueUnCol.setCellValueFactory(new PropertyValueFactory<>("estoqueUn"));
        empKgCol.setCellValueFactory(new PropertyValueFactory<>("empKg"));
        empUnCol.setCellValueFactory(new PropertyValueFactory<>("empUn"));
        quantLibUnCol.setCellValueFactory(new PropertyValueFactory<>("quantLibUn"));
        searchList = estoqueGalvanisaService.getProdutosGalvanisa();
        produtosTableView.setItems(searchList);

    }

    private void initTableTerceiros() {

        clenteTerceiroCol.setCellValueFactory(new PropertyValueFactory<>("clienteDescricao"));
        codigoterceiroCol.setCellValueFactory(
                (TableColumn.CellDataFeatures<ProdutosEstoqueTerceiros, String> p) -> p.getValue().codigoProperty());
        descricaoTerceiroCol.setCellValueFactory(
                (TableColumn.CellDataFeatures<ProdutosEstoqueTerceiros, String> p) -> p.getValue().descricaoProperty());
        kgTerceiroCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        unidadeTerceiroCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        fatorTerceiroCol.setCellValueFactory(new PropertyValueFactory<>("fconversao"));
        searchListTerceiros = loadProdutosService.loadEstoqueProperty();
        terceirosTableView.setItems(searchListTerceiros);
    }

    private void initTableCompras() {

        previsaoComprasCol.setCellValueFactory(new PropertyValueFactory<>("dataEntregaEstimada"));
        codigoComprasCol.setCellValueFactory(new PropertyValueFactory<>("codProduto"));
        descricaoComprasCol.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        kgSolicitadaCol.setCellValueFactory(new PropertyValueFactory<>("qtdSolicitada"));
        qtdPedidoKgCol.setCellValueFactory(new PropertyValueFactory<>("qtdPedidoKg"));
        numPedidoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        qtdPedidoUniCol.setCellValueFactory(new PropertyValueFactory<>("qtdPedidoUn"));
        searchListPedidoCompras = pedidoCompraService.getPedidosCompras();
        pedidoComprasTableView.setItems(searchListPedidoCompras);
    }

    private void setFocusInTables() {
        terceirosTableView.getSelectionModel().clearAndSelect(0, fatorCol);
        produtosTableView.getSelectionModel().clearAndSelect(0, descricaoCol);
        pedidoComprasTableView.getSelectionModel().clearAndSelect(0, fatorCol);
        fieldSearch.requestFocus();
    }
}
