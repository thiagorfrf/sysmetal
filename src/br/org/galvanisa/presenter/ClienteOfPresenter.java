/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.presentation.controleof.ControleOfService;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.controlsfx.dialog.Dialogs;
import utilities.fieldsControl;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class ClienteOfPresenter implements Initializable {

    private ClientesPresenter clientesPresenter = new ClientesPresenter();
    public ControleCliente controleCliente = new ControleCliente();
    private ObservableList<ControleOf> controleOfList = FXCollections.observableArrayList();
    private ControleOf controleOf = new ControleOf();
    private ControleOfService controleOfsService = new ControleOfService();
    ObservableList<Boolean> statusList = FXCollections.observableArrayList();

    @FXML
    private Label codigoLabel;
    @FXML
    private Label descricaoLabel;
    @FXML
    private Label opLabel;
    @FXML
    private Label contratoLabel;
    @FXML
    private Label statusLabel;
    @FXML
    private Button btnInserir;
    @FXML
    private TableColumn ofCol;
    @FXML
    private TableColumn pcCol;
    @FXML
    private TableView ofTableView;
    @FXML
    private Label tituloLabel;
    @FXML
    private Label codControleLabel;
    @FXML
    private TableColumn statusCol;
    @FXML
    private TextField ofTxt;
    @FXML
    private TextField pcTxt;
    @FXML
    private ComboBox statusComboBox;
    @FXML
    private HBox fundo;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Label label3;
    @FXML
    private Label label4;
    @FXML
    private Label label5;
    @FXML
    private Label label;
    @FXML
    private Label label6;
    @FXML
    private Label label7;
    @FXML
    private Label label8;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        setStyle();
        configureFields();

        btnInserir.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                setOf();
            }

        });

        ofTxt.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    pcTxt.requestFocus();
                }
            }
        });

        pcTxt.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    statusComboBox.requestFocus();
                }
            }
        });

        statusComboBox.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    btnInserir.requestFocus();
                }
            }
        });

        btnInserir.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.ENTER) {
                    setOf();
                }
            }
        });

        ofTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override

            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {

                        controleOf = (ControleOf) ofTableView.getSelectionModel().getSelectedItem();
                        if (controleOf.getStatus()) {
                            controleOf.setStatus(Boolean.FALSE);
                        } else if (!controleOf.getStatus()) {
                            controleOf.setStatus(Boolean.TRUE);
                        }
                        controleOfsService.alterStatus(controleOf);
                        populateTableView();
                        ofTxt.requestFocus();
                    }
                }
            }

        });

    }

    private ControleCliente clienteCampos(ControleCliente cliente) {
        descricaoLabel.setText(cliente.getDescCliente().trim());
        codigoLabel.setText(cliente.getCodCliente());
        opLabel.setText(cliente.getNumOp());
        contratoLabel.setText(cliente.getNumContrato());
        statusLabel.setText(cliente.getStatus().toString());
        contratoLabel.setText(cliente.getNumContrato());
        codControleLabel.setText(Integer.toString(cliente.getCodControle()));
        return cliente;
    }

    public void setController(ClientesPresenter clientesPresenter) {
        this.clientesPresenter = clientesPresenter;

    }

    public void setCliente(ControleCliente controleCliente) {
        this.controleCliente = controleCliente;
        clienteCampos(controleCliente);
        populateTableView();
    }

    public void populateTableView() {

        ofCol.setCellValueFactory(new PropertyValueFactory<>("numOf"));
        pcCol.setCellValueFactory(new PropertyValueFactory<>("numPc"));
        statusCol.setCellValueFactory(new PropertyValueFactory<ControleOf, Boolean>("status"));
        controleOfList = controleOfsService._getOfsPorCodigoCliente(Integer.parseInt(codControleLabel.getText()));
        ofTableView.setItems(controleOfList);
    }

    public void setOf() {
        if (ofTxt.getText().isEmpty() || pcTxt.getText().isEmpty()) {
            Dialogs.create().title("Aviso")
                    .masthead("Por favor, preencha todos os campos.")
                    .showWarning();

        } else {
            controleOf.setStatus((Boolean) statusComboBox.getValue());
            controleOf.setNumOf(ofTxt.getText());
            controleOf.setNumPc(pcTxt.getText());
            controleOf.setCodControleCliente(Integer.parseInt(codControleLabel.getText()));
            controleOfsService.addOf(controleOf);
            populateTableView();
            ofTxt.clear();
            pcTxt.clear();
        }
    }

    private void configureFields() {
        ofTxt.textProperty().addListener(new utilities.TextFieldListener(this.ofTxt));
        pcTxt.textProperty().addListener(new utilities.TextFieldListener(this.pcTxt));
        statusList.setAll(Boolean.FALSE, Boolean.TRUE);
        statusComboBox.setItems(statusList);
        statusComboBox.getSelectionModel().select(1);
        ofTxt.addEventHandler(KeyEvent.KEY_TYPED, fieldsControl.maxLength(2));

    }

    public void setStyle() {
        fundo.getStyleClass().add("bordePane-clienteof");

        label.getStyleClass().clear();
        label1.getStyleClass().clear();
        label2.getStyleClass().clear();
        label3.getStyleClass().clear();
        label3.getStyleClass().clear();
        label4.getStyleClass().clear();
        label5.getStyleClass().clear();
        label6.getStyleClass().clear();
        label7.getStyleClass().clear();
        label8.getStyleClass().clear();

        label.getStyleClass().add("titulo");
        label1.getStyleClass().add("titulo");
        label2.getStyleClass().add("titulo");
        label3.getStyleClass().add("titulo");
        label3.getStyleClass().add("titulo");
        label4.getStyleClass().add("titulo");
        label5.getStyleClass().add("titulo");
        label6.getStyleClass().add("titulo");
        label7.getStyleClass().add("titulo");
        label8.getStyleClass().add("titulo");

    }

}
