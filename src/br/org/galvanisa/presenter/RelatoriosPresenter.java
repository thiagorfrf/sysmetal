package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.reports.transfer.ChamarRelatoriosGerais;
import br.org.galvanisa.business.reports.entradas.ChamarRelatorioClienteEntradas;
import br.org.galvanisa.business.reports.saidas.ChamarRelatorioClienteSaidas;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;

/**
 *
 * @author eric
 */
public class RelatoriosPresenter implements Initializable {

    private ChamarRelatorioClienteEntradas chamarRelatorioClienteEntradas = new ChamarRelatorioClienteEntradas();
    private ChamarRelatorioClienteSaidas chamarRelatorioClienteSaidas = new ChamarRelatorioClienteSaidas();
    private ProdutosPresenter produtosPresenter = new ProdutosPresenter();
    private DateReportsPresenter dateReportsPresenter = new DateReportsPresenter();
    private OFsReportsPresenter oFsReportsPresenter = new OFsReportsPresenter();
    private ChamarRelatoriosGerais chamarRelatoriosGerais = new ChamarRelatoriosGerais();
    private String codCliente;
    private int codControleCliente;

    @FXML
    private Label labelTodasTransferencias;
    @FXML
    private Label LabelTodasEntradas;
    @FXML
    private Label LabelTodasSaidas;
    @FXML
    private Label LabelTodosEpenhos;
    @FXML
    private Label LabelTodosEpenhosData;
    @FXML
    private Label LabelTodasSaidasData;
    @FXML
    private Label LabelTodasEntradasData;
    @FXML
    private Label labelTodasTransferenciasData;
    @FXML
    private Label labelTodasEntradasClienteData;
    @FXML
    private Label labelTodasEntradasCliente;
    @FXML
    private Label labelTodasSaidasClienteData;
    @FXML
    private Label labelTodasSaidasCliente;
    @FXML
    private Label labelTodasSaidasClienteOF;
    @FXML
    private Label labelTransferenciasDestino;
    @FXML
    private Label movimentacoesPorProduto;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label titulo1;
    @FXML
    private Label titulo2;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fundo.getStyleClass().add("report");
        titulo1.getStyleClass().add("titulo");
        titulo2.getStyleClass().add("titulo");

        movimentacoesPorProduto.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                movimentacoesPorProduto.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasTransferencias.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasTransferencias.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTransferenciasDestino.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTransferenciasDestino.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodasEntradas.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasEntradas.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodasSaidas.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasSaidas.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodosEpenhos.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodosEpenhos.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasTransferenciasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasTransferenciasData.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodasEntradasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasEntradasData.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodasSaidasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasSaidasData.setCursor(Cursor.OPEN_HAND);
            }
        });
        LabelTodosEpenhosData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodosEpenhosData.setCursor(Cursor.OPEN_HAND);
            }
        });

        labelTodasEntradasCliente.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasEntradasCliente.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasEntradasClienteData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasEntradasClienteData.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasSaidasCliente.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasCliente.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasSaidasClienteOF.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasClienteOF.setCursor(Cursor.OPEN_HAND);
            }
        });
        labelTodasSaidasClienteData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasClienteData.setCursor(Cursor.OPEN_HAND);
            }
        });
        //Mouse Clicked
        movimentacoesPorProduto.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Produtos.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {

                    produtosPresenter.setCliente(codCliente, codControleCliente, Boolean.TRUE);

                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                Scene scene = new Scene(root);
                stage.setTitle("Escolher produto para relatório");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            }
            /*
             public void handle(MouseEvent mouseEvent) {
             try {
                    
             } catch (JRException ex) {
             Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
             } catch (SQLException ex) {
             Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
             } catch (IOException ex) {
             Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
             }
             }
             */
        });

        labelTodasTransferencias.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    chamarRelatoriosGerais.chamarRelatorioTotalTransferencia();
                } catch (JRException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        labelTransferenciasDestino.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    chamarRelatorioClienteEntradas.chamarRelatorioTotalTransferenciaDestino();
                } catch (JRException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        LabelTodasEntradas.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

            }
        });
        LabelTodasSaidas.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    dateReportsPresenter.identificadorRelatorio = "TodasSaidas";
                    chamarRelatorioClienteSaidas.chamarRelatorioTotalSaidas();
                } catch (JRException ex) {
                    System.out.println("Erro no JReport " + ex);
                } catch (SQLException ex) {
                    System.out.println("Impossivel conectar ao banco " + ex);
                } catch (IOException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        LabelTodosEpenhos.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("Ação");
            }
        });
        labelTodasTransferenciasData.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DateReports.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {
                    dateReportsPresenter.identificadorRelatorio = "TotalTransferenciaData";
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                Scene scene = new Scene(root);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            }
        });
        LabelTodasEntradasData.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("Ação");
            }
        });
        LabelTodasSaidasData.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DateReports.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {
                    dateReportsPresenter.identificadorRelatorio = "TotalSaidasData";
                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
                Scene scene = new Scene(root);
                stage.setTitle("Intervalo de Tempo - Relatório de Movimentação");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();
            }
        });
        LabelTodosEpenhosData.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("Ação");
            }
        });

        labelTodasEntradasCliente.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    chamarRelatorioClienteEntradas.chamarRelatorioEntradasCliente(codCliente);
                } catch (JRException ex) {
                    System.out.println("Erro no JReport " + ex);
                } catch (SQLException ex) {
                    System.out.println("Impossivel conectar ao banco " + ex);
                } catch (IOException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        LabelTodasEntradas.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    chamarRelatorioClienteEntradas.chamarRelatorioEntradas();
                } catch (JRException ex) {
                    System.out.println("Erro no JReport " + ex);
                } catch (SQLException ex) {
                    System.out.println("Impossivel conectar ao banco " + ex);
                } catch (IOException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        labelTodasSaidasCliente.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    chamarRelatorioClienteSaidas.chamarRelatorioSaidasCliente(codControleCliente);
                } catch (JRException ex) {
                    System.out.println("Erro no JReport " + ex);
                } catch (SQLException ex) {
                    System.out.println("Impossivel conectar ao banco " + ex);
                } catch (IOException ex) {
                    Logger.getLogger(RelatoriosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        LabelTodasEntradasData.setOnMouseClicked(
                new EventHandler<MouseEvent>() {

                    public void handle(MouseEvent event) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DateReports.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            dateReportsPresenter.identificadorRelatorio = "TotalEntradasData";
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        stage.setTitle("Intervalo de Tempo - Relatório de Movimentação");
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }

                }
        );

        labelTodasSaidasClienteData.setOnMouseClicked(
                new EventHandler<MouseEvent>() {

                    public void handle(MouseEvent event) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DateReports.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            dateReportsPresenter.identificadorRelatorio = "SaidasData";
                            dateReportsPresenter.codControleCliente = codControleCliente;
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        stage.setTitle("Intervalo de Tempo - Relatório de Movimentação");
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }

                }
        );

        labelTodasEntradasClienteData.setOnMouseClicked(
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/DateReports.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            dateReportsPresenter.identificadorRelatorio = "EntradasData";
                            dateReportsPresenter.codCliente = codCliente;
                            dateReportsPresenter.codControleCliente = codControleCliente;
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        stage.setTitle("Intervalo de Tempo - Relatório de Movimentação");
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }

                }
        );

        labelTodasSaidasClienteOF.setOnMouseClicked(
                new EventHandler<MouseEvent>() {

                    @Override
                    public void handle(MouseEvent event) {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/OFsReports.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            oFsReportsPresenter.identificadorRelatorio = "Entradas";
                            oFsReportsPresenter.codCliente = codCliente;
                            oFsReportsPresenter.codControleCliente = codControleCliente;
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(ProdutosPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        Scene scene = new Scene(root);
                        stage.setTitle("Relatório de Saidas por OF e PC");
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }

                }
        );
/*
        movimentacoesPorProduto.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                movimentacoesPorProduto.setCursor(Cursor.OPEN_HAND);
                movimentacoesPorProduto.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasSaidasClienteData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasClienteData.setCursor(Cursor.OPEN_HAND);
                labelTodasSaidasClienteData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasSaidasClienteData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasSaidasClienteData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasEntradasCliente.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasEntradasCliente.setCursor(Cursor.OPEN_HAND);
                labelTodasEntradasCliente.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasEntradasCliente.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasEntradasCliente.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasEntradasClienteData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasEntradasClienteData.setCursor(Cursor.OPEN_HAND);
                labelTodasEntradasClienteData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasEntradasClienteData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasEntradasClienteData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasSaidasCliente.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasCliente.setCursor(Cursor.OPEN_HAND);
                labelTodasSaidasCliente.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasSaidasCliente.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasSaidasCliente.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasTransferencias.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasTransferencias.setCursor(Cursor.OPEN_HAND);
                labelTodasTransferencias.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasTransferencias.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasTransferencias.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodasEntradas.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasEntradas.setCursor(Cursor.OPEN_HAND);
                LabelTodasEntradas.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodasEntradas.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodasEntradas.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodasEntradasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasEntradasData.setCursor(Cursor.OPEN_HAND);
                LabelTodasEntradasData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodasEntradasData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodasEntradasData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodasSaidas.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasSaidas.setCursor(Cursor.OPEN_HAND);
                LabelTodasSaidas.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodasSaidas.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodasSaidas.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodasSaidasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodasSaidasData.setCursor(Cursor.OPEN_HAND);
                LabelTodasSaidasData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodasSaidasData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodasSaidasData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodosEpenhos.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodosEpenhos.setCursor(Cursor.OPEN_HAND);
                LabelTodosEpenhos.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodosEpenhos.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodosEpenhos.setStyle("-fx-text-fill: #1285f8");
            }
        });

        LabelTodosEpenhosData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                LabelTodosEpenhosData.setCursor(Cursor.OPEN_HAND);
                LabelTodosEpenhosData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        LabelTodosEpenhosData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                LabelTodosEpenhosData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasTransferenciasData.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasTransferenciasData.setCursor(Cursor.OPEN_HAND);
                labelTodasTransferenciasData.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasTransferenciasData.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasTransferenciasData.setStyle("-fx-text-fill: #1285f8");
            }
        });

        labelTodasSaidasClienteOF.addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                labelTodasSaidasClienteOF.setCursor(Cursor.OPEN_HAND);
                labelTodasSaidasClienteOF.setStyle("-fx-font: 13px Arial;-fx-text-fill: #0f07f5");
            }
        });

        labelTodasSaidasClienteOF.addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                labelTodasSaidasClienteOF.setStyle("-fx-text-fill: #1285f8");
            }
        });
*/
    }

    public void setCliente(ControleCliente cliente) {
        codCliente = cliente.getCodCliente();
        codControleCliente = cliente.getCodControle();
    }
}
