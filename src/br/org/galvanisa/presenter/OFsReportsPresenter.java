/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.reports.entradas.ChamarRelatorioClienteEntradas;
import br.org.galvanisa.business.reports.saidas.ChamarRelatorioClienteSaidas;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.controleof.ControleOfService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import net.sf.jasperreports.engine.JRException;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class OFsReportsPresenter implements Initializable {

    static String identificadorRelatorio;
    static int codControleCliente;
    static String codCliente;
    private ControleClienteService controleClienteService = new ControleClienteService();
    private ChamarRelatorioClienteSaidas chamarRelatorioClienteSaidas = new ChamarRelatorioClienteSaidas();
    private ObservableList<ControleOf> ofs = null;
    private DateReportsPresenter dateReportsPresenter = new DateReportsPresenter();
    private ObservableList ofsList = FXCollections.observableArrayList();
    private ObservableList pcsList = FXCollections.observableArrayList();
    private List<ControleOf> ofList3 = new ArrayList();
    private int codigoControleOf = 0;
    private ControleOfService controleOfService = new ControleOfService();
    ChamarRelatorioClienteEntradas chamarRelatorioClienteEntradas = new ChamarRelatorioClienteEntradas();

    @FXML
    private ChoiceBox pcChoiceBox;
    @FXML
    private Button abrirRelatorioButton;
    @FXML
    private ChoiceBox ofChoiceBox;
    @FXML
    private AnchorPane fundo;
    @FXML
    private Label titulo1;
    @FXML
    private Label titulo2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fundo.getStyleClass().add("bordePane");
        titulo1.getStyleClass().add("titulo");
        titulo2.getStyleClass().add("titulo");
        fillOfsChoiceBox();
        treatOfsChoiceBox();

        abrirRelatorioButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {

                try {
                    //   List<ControleOf> ofsList2 = ofsList;
                    //  System.out.println("Lista Entranha itens :"+ofsList2.get(0).getNumOf()+ "  "+ ofsList2.get(0).getNumPc());
                    chamarRelatorioClienteSaidas.chamarRelatorioSaidasOF(codControleCliente, getOfs(), getOfs2(), getDescricaoCliente());
                } catch (JRException ex) {
                    Logger.getLogger(OFsReportsPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(OFsReportsPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(OFsReportsPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

        });

    }

    private List<ControleOf> getOfs() {
        List<ControleOf> controleOfList = new ArrayList<>();
        for (ControleOf u : ofs) {
            if (u.getNumOf().equals(ofChoiceBox.getSelectionModel().getSelectedItem().toString())) {
                controleOfList.add(u);
                return controleOfList;

            }
        }
        return controleOfList;

    }

    private int getControleOfs() {
        if (pcChoiceBox.getSelectionModel().isEmpty()) {
            ofChoiceBox.getItems();
            chamarRelatorioClienteSaidas.ofPreenchida = false;

        } else {

            for (ControleOf u : ofs) {
                try {
                    if (u.getNumPc().equals(pcChoiceBox.getSelectionModel().getSelectedItem().toString())) {
                        return u.getCodControleOf();

                    }
                } catch (Exception a) {
                    System.out.println("--");
                }
            }
        }

        return 0;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public String getDescricaoCliente() {
        ControleCliente cliente = controleClienteService.getAllControleClientePorCodigoControle(codControleCliente);
        return cliente.getDescCliente();
    }

    void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public void setCliente(ControleCliente cliente) {
        codCliente = cliente.getCodCliente();
        codControleCliente = cliente.getCodControle();
    }

    //methods of support.
    private void fillOfsChoiceBox() {

        try {
            ofs = FXCollections.observableArrayList(controleOfService.getOfs(codControleCliente));
        } catch (Exception e) {
            System.out.println("-");
        }

        if (ofs.size() != 0) {
            ofsList.add(ofs.get(0).getNumOf());
            for (int i = 1; i < ofs.size(); i++) {
                for (int x = 0; x < ofsList.size(); x++) {
                    if (ofsList.contains((ofs.get(i).getNumOf()))) {
                        continue;
                    } else {
                        ofsList.add(ofs.get(i).getNumOf());
                    }
                }
            }
            ofChoiceBox.setItems(ofsList);

        } else {
            System.out.println("---");
        }
    }

    private void treatOfsChoiceBox() {

        ofChoiceBox.getSelectionModel()
                .selectedIndexProperty()
                .addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        //System.out.println(newValue.intValue());
                        fillPcsChoiceBox(ofsList.get(newValue.intValue()).toString());
                    }

                    private void fillPcsChoiceBox(String theOf) {
                        //percorre o ControleOf filtrando as ofs
                        //adicionando os pc que baterem no choiceBox e salvando
                        // em um array que contenha o numero do controle, que sera
                        // salvo na base de dados !!!! no botao salvar !!!
                        //fieldNumPc.setDisable(false);
                        pcsList.removeAll(ofs);
                        pcChoiceBox.getItems().removeAll(pcsList);
                        for (int i = 0; i < ofs.size(); i++) {
                            if (ofs.get(i).getNumOf().equals(theOf)) {
                                pcsList.add(ofs.get(i).getNumPc());
                            }
                        }
                        pcChoiceBox.setItems(pcsList);
                    }

                });
    }

    private List<ControleOf> getOfs2() {
        List<ControleOf> controleOfList = new ArrayList<>();

        for (int i = 0; i < ofs.size(); i++) {
            if (ofs.get(i).getNumOf().equals(ofChoiceBox.getSelectionModel().getSelectedItem())) {
                controleOfList.add(ofs.get(i));
            }

        }
        return controleOfList;
    }
}
