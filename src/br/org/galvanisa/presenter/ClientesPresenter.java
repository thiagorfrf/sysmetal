/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class ClientesPresenter implements Initializable {

    Image imgBtnNovoCliente = new Image(getClass().getResourceAsStream("/view/icons/Add.png"));
    Image imgBtnAlterarCliente = new Image(getClass().getResourceAsStream("/view/icons/alterar.png"));

    /**
     * Initializes the controller class.
     */
    public ControleCliente cliente = new ControleCliente();

    ObservableList<ControleCliente> controleCliente = FXCollections.observableArrayList();

    ControleClienteService controleClienteService = new ControleClienteService();
    @FXML
    private TableColumn codigoTerceirosCol;
    @FXML
    private TableColumn descricaoTerceirosCol;
    @FXML
    private TableColumn opTerceirosCol;
    @FXML
    private TableColumn contratoTerceirosCol;
    @FXML
    private TableColumn statusTerceirosCol;
    @FXML
    private TableColumn codigoCol;
    @FXML
    private TableColumn decricaoCol;
    @FXML
    private TableColumn opCol;
    @FXML
    private TableColumn contratoCol;
    @FXML
    private TableColumn statusCol;
    @FXML
    private TableView clienteTerceirosTableView;
    @FXML
    private TableView clienteTableView;
    @FXML
    private Button btnNovoCLiente;
    @FXML
    private Button btnAlterarCliente;
    @FXML
    private Label headLabel1;
    @FXML
    private Label headLabel2;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        headLabel1.getStyleClass().add("headLabel");
        headLabel2.getStyleClass().add("headLabel");
        btnNovoCLiente.setGraphic(new ImageView(imgBtnNovoCliente));
        btnAlterarCliente.setGraphic(new ImageView(imgBtnAlterarCliente));

        populateTableViewInterna();
        populateTableViewTerceiros();

        btnNovoCLiente.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroCliente.fxml"));
                Parent root = null;
                Stage stage = new Stage();
                try {

                    root = loader.load();
                } catch (IOException ex) {
                    Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

                CadastroClientePresenter controller = loader.<CadastroClientePresenter>getController();
                controller.setController(ClientesPresenter.this);

                Scene scene = new Scene(root);
                stage.setTitle("Novo Cliente");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.show();

            }

        });

        btnAlterarCliente.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (clienteTableView.getSelectionModel().isEmpty() && clienteTerceirosTableView.getSelectionModel().isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Selecione um Cliente!").showWarning();
                } else {

                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroCliente.fxml"));
                    Parent root = null;
                    Stage stage = new Stage();
                    try {

                        root = loader.load();
                    } catch (IOException ex) {
                        Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (!clienteTableView.getSelectionModel().isEmpty()) {
                        cliente = (ControleCliente) clienteTableView.getSelectionModel().getSelectedItem();
                    } else if (!clienteTerceirosTableView.getSelectionModel().isEmpty()) {
                        cliente = (ControleCliente) clienteTerceirosTableView.getSelectionModel().getSelectedItem();
                    }

                    CadastroClientePresenter controller = loader.<CadastroClientePresenter>getController();
                    controller.setController(ClientesPresenter.this, cliente);

                    Scene scene = new Scene(root);
                    stage.setTitle("Alterar Cliente");
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setScene(scene);
                    stage.show();
                }

            }

        });

        clienteTableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                clienteTerceirosTableView.getSelectionModel().clearSelection();
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        if (clienteTableView.getSelectionModel().isEmpty()) {
                            Dialogs.create()
                                    .title("DICA!").masthead("Dica")
                                    .message("Clique duas vezes em um cliente para adicionar OF's e OP's!").showWarning();
                        } else {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ClienteOf.fxml"));
                            Parent root = null;
                            Stage stage = new Stage();
                            try {
                                root = loader.load();
                            } catch (IOException ex) {
                                Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            cliente = (ControleCliente) clienteTableView.getSelectionModel().getSelectedItem();
                            ClienteOfPresenter controller = loader.<ClienteOfPresenter>getController();
                            controller.setController(ClientesPresenter.this);
                            controller.setCliente(cliente);
                            Scene scene = new Scene(root);
                            stage.setTitle("Adicionar OF's a " + cliente.getDescCliente().trim());
                            stage.initModality(Modality.APPLICATION_MODAL);
                            stage.setScene(scene);
                            stage.show();
                        }
                    }
                }
            }
        });

        clienteTerceirosTableView.setOnMouseClicked(
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        clienteTableView.getSelectionModel().clearSelection();
                        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                            if (mouseEvent.getClickCount() == 2) {
                                if (clienteTerceirosTableView.getSelectionModel().isEmpty()) {
                                    Dialogs.create()
                                    .title("DICA!").masthead("Dica")
                                    .message("Clique duas vezes em um cliente para adicionar OF's e OP's!").showWarning();
                                } else {
                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ClienteOf.fxml"));
                                    Parent root = null;
                                    Stage stage = new Stage();
                                    try {
                                        root = loader.load();
                                    } catch (IOException ex) {
                                        Logger.getLogger(CadastroClientePresenter.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    cliente = (ControleCliente) clienteTerceirosTableView.getSelectionModel().getSelectedItem();
                                    ClienteOfPresenter controller = loader.<ClienteOfPresenter>getController();
                                    controller.setController(ClientesPresenter.this);
                                    controller.setCliente(cliente);
                                    Scene scene = new Scene(root);
                                    stage.setTitle("Adicionar OF's a " + cliente.getDescCliente().trim());
                                    stage.initModality(Modality.APPLICATION_MODAL);
                                    stage.setScene(scene);
                                    stage.show();
                                }
                            }
                        }
                    }

                }
        );
    }

    public void populateTableViewInterna() {

        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codCliente"));
        decricaoCol.setCellValueFactory(new PropertyValueFactory<>("descCliente"));
        contratoCol.setCellValueFactory(new PropertyValueFactory<>("numContrato"));
        opCol.setCellValueFactory(new PropertyValueFactory<>("numOp"));
        statusCol.setCellValueFactory(new PropertyValueFactory<ControleCliente, Boolean>("status"));
        controleCliente = controleClienteService.getAllClientesTerceirosAtivos();
        clienteTableView.setItems(controleCliente);
        clienteTerceirosTableView.getSelectionModel().clearSelection();
        clienteTableView.getSelectionModel().clearSelection();
    }

    public void populateTableViewTerceiros() {

        codigoTerceirosCol.setCellValueFactory(new PropertyValueFactory<>("codCliente"));
        descricaoTerceirosCol.setCellValueFactory(new PropertyValueFactory<>("descCliente"));
        contratoTerceirosCol.setCellValueFactory(new PropertyValueFactory<>("numContrato"));
        opTerceirosCol.setCellValueFactory(new PropertyValueFactory<>("numOp"));
        statusTerceirosCol.setCellValueFactory(new PropertyValueFactory<ControleCliente, Boolean>("status"));
        controleCliente = controleClienteService.getAllClientesMPInterna();
        clienteTerceirosTableView.setItems(controleCliente);
        clienteTerceirosTableView.getSelectionModel().clearSelection();
        clienteTableView.getSelectionModel().clearSelection();
    }

    public ControleCliente getControleCliente() {
        return cliente;
    }
}
