
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presenter;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.entity.produtosdescricao.ProdutosDescricao;
import br.org.galvanisa.business.entity.saida.SaidaBusinessEntity;
import br.org.galvanisa.business.reports.saidas.ChamarRelatorioClienteSaidas;
import br.org.galvanisa.presentation.saidaitens.SaidaItensService;
import br.org.galvanisa.presentation.saidas.SaidasService;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

/**
 *
 * @author eric
 */
public class SaidasPresenter implements Initializable {

    private ChamarRelatorioClienteSaidas chamarRelatorioClienteSaidas = new ChamarRelatorioClienteSaidas();
    private SaidaItensService saidaItensService = new SaidaItensService();
    private Saida saida = new Saida();
    private SaidaBusinessEntity saidaBusiness = new SaidaBusinessEntity();
    private SaidaBusinessEntity saidaBusiness2 = new SaidaBusinessEntity();
    private ObservableList<ProdutosDescricao> saidaItens = FXCollections.observableArrayList();
    private ControleCliente cliente1 = new ControleCliente();
    private SaidasService saidasService = new SaidasService();
    private ObservableList<SaidaBusinessEntity> clienteSaidas = FXCollections.observableArrayList();
    private ObservableList<SaidaBusinessEntity> clienteSaidasFilter = FXCollections.observableArrayList();
    private ObservableList<SaidaBusinessEntity> clienteSaidas2 = FXCollections.observableArrayList();
    private ObservableList<MateriaisData> itensEmpenho = FXCollections.observableArrayList();
    private List<Object> saidaList = new ArrayList<Object>();
    private List<Object> saidaList2 = new ArrayList<Object>();
    public String filtrosSelecionados = "Todas";
    public String objString;
    SaidaBusinessEntity objSaidaBusinnesEntity = new SaidaBusinessEntity();
    @FXML
    private VBox vboxTab;
    @FXML
    private Label codClienteLabel;
    @FXML
    private Label descricaoLabel;
    @FXML
    private Label contratoLabel;
    @FXML
    private Label opLabel;
    @FXML
    private TableView saidasTableView;
    @FXML
    private TableColumn saidaCol;
    @FXML
    private TableColumn CodClienteTransfCol;
    @FXML
    private TableColumn tipoOperCol;
    @FXML
    private TableColumn totalKgCol;
    @FXML
    private TableColumn dtSaidaCol;
    @FXML
    private MenuItem acaoBaixaEmpenho;
    @FXML
    private CheckBox sobrasFilter;
    @FXML
    private CheckBox transferenciaFilter;
    @FXML
    private CheckBox normalFilter;
    @FXML
    private TableView produtosSaidaTableView;
    @FXML
    private TableColumn codProdutoCol;
    @FXML
    private TableColumn descricaoCol;
    @FXML
    private TableColumn totalEntradaCol;
    @FXML
    private TableColumn quantidadeCol;
    @FXML
    private MenuButton acoesMenuButton;
    @FXML
    private MenuItem estornoSaida;
    @FXML
    private Button relatorioButton;
    @FXML
    private TableColumn of;
    @FXML
    private TableColumn pc;
    @FXML
    private CheckBox empenhoFilter;
    @FXML
    private Label headLabel1;
    @FXML
    private Label headLabel2;
    @FXML
    private Label headLabel3;
    @FXML
    private Label headLabel4;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        vboxTab.getStyleClass().add("bordePane");
        headLabel1.getStyleClass().add("headLabel");
        headLabel2.getStyleClass().add("headLabel");
        headLabel3.getStyleClass().add("headLabel");
        headLabel4.getStyleClass().add("headLabel");
        produtosSaidaTableView.getStyleClass().add("table");
        saidasTableView.getStyleClass().add("table");
        Image imgAcoesMenuButton = new Image(getClass().getResourceAsStream("/view/icons/Config.png"));
        Image imgRelMenuButton = new Image(getClass().getResourceAsStream("/view/icons/rel.png"));
        acoesMenuButton.setGraphic(new ImageView(imgAcoesMenuButton));
        relatorioButton.setGraphic(new ImageView(imgRelMenuButton));

        consultarBanco();
        eventFilter(sobrasFilter);
        eventFilter(transferenciaFilter);
        eventFilter(empenhoFilter);
        eventFilter(normalFilter);

        estornoSaida.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                saidaBusiness = (SaidaBusinessEntity) saidasTableView.getSelectionModel().getSelectedItem();
                if (saidasTableView.getSelectionModel().getSelectedItem() == null) {
                    Dialogs.create()
                            .title("Aviso!")
                            .message("Por favor, selecione uma das saidas!").showWarning();
                } else if (saidaBusiness.getTipoOperacao() == 'N') {

                    Action response = Dialogs.create()
                            .owner(acoesMenuButton)
                            .title("Confirmação de Estorno")
                            .masthead("Deseja realmente estornar a saida abaixo?")
                            .message("Cod Saída : " + saidaBusiness.getCodSaida() + " com o peso total de : " + saidaBusiness.getTotalKg())
                            .showConfirm();

                    if (response == Dialog.ACTION_YES) {

                        saidaItensService.estornoSaidaApply(saidaBusiness, produtosSaidaTableView.getItems(), "N");
                        setCliente(cliente1);
                        produtosSaidaTableView.getItems().clear();

                    }
                } else if (saidaBusiness.getTipoOperacao() == 'E') {
                    Action response = Dialogs.create()
                            .owner(acoesMenuButton)
                            .title("Confirmação de Estorno")
                            .masthead("Deseja realmente estornar o empenho abaixo?")
                            .message("Cod Saída : " + saidaBusiness.getCodSaida() + " com o peso total de : " + saidaBusiness.getTotalKg())
                            .showConfirm();

                    if (response == Dialog.ACTION_YES) {

                        saidaItensService.estornoSaidaApply(saidaBusiness, produtosSaidaTableView.getItems(), "E");
                        setCliente(cliente1);
                        produtosSaidaTableView.getItems().clear();

                    }
                } else if (saidaBusiness.getTipoOperacao() == 'D') {
                    Action response = Dialogs.create()
                            .owner(acoesMenuButton)
                            .title("Confirmação de Estorno")
                            .masthead("Deseja realmente estornar a devolução abaixo?")
                            .message("Cod Saída : " + saidaBusiness.getCodSaida() + " com o peso total de : " + saidaBusiness.getTotalKg())
                            .showConfirm();

                    if (response == Dialog.ACTION_YES) {

                        saidaItensService.estornoSaidaApply(saidaBusiness, produtosSaidaTableView.getItems(), "D");
                        setCliente(cliente1);
                        produtosSaidaTableView.getItems().clear();

                    }
                } else if (saidaBusiness.getTipoOperacao() == 'T') {
                    Action response = Dialogs.create()
                            .owner(acoesMenuButton)
                            .title("Confirmação de Estorno")
                            .masthead("Deseja realmente estornar a transferência abaixo?")
                            .message("Cod Saída : " + saidaBusiness.getCodSaida()
                                    + " com o peso total de : " + saidaBusiness.getTotalKg()
                                    + " Fornecedor : " + saidaBusiness.getClienteTransfString())
                            .showConfirm();

                    if (response == Dialog.ACTION_YES) {

                        saidaItensService.estornoSaidaApply(saidaBusiness, produtosSaidaTableView.getItems(), "T");
                        setCliente(cliente1);
                        produtosSaidaTableView.getItems().clear();

                    }
                } else {
                }
            }
        });

        acaoBaixaEmpenho.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                if (saidasTableView.getSelectionModel().isEmpty()) {
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Por favor, Selecione uma saída do tipo Empenho").showWarning();
                } else if (!produtosSaidaTableView.getSelectionModel().isEmpty()) {
                    saidasTableView.getSelectionModel().clearSelection();
                    produtosSaidaTableView.getSelectionModel().clearSelection();
                    Dialogs.create()
                            .title("Aviso!").masthead("")
                            .message("Por favor, selecione uma saída").showWarning();
                } else {
                    objSaidaBusinnesEntity = (SaidaBusinessEntity) saidasTableView.getSelectionModel().getSelectedItem();
                    if (!objSaidaBusinnesEntity.getTipoOperacao().equals('E')) {
                        Dialogs.create()
                                .title("Aviso!").masthead("")
                                .message("Por favor, selecione uma saída do tipo empenho").showWarning();
                    } else {
                        itensEmpenho = FXCollections.observableArrayList();
                        Saida saida = new Saida();

                        saida = saidasService.getSaida(objSaidaBusinnesEntity.getCodSaida());
                        saidaList.add(saida);
                        saidasService.getObjectsFromSaida(saidaList);
                        List<String> saidaCampos = new ArrayList<>();
                        saidaCampos = saidasService.transformSaidaInToCadastroView(saida);
                        itensEmpenho = saidasService.getItensFromSaida(saida);
                        int tipoSaida = 4; // saida Empenho to Normal
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroSaida.fxml"));
                        Parent root = null;
                        Stage stage = new Stage();
                        try {
                            root = loader.load();
                        } catch (IOException ex) {
                            Logger.getLogger(MateriaisPresenter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        CadastroSaidaPresenter controller = loader.<CadastroSaidaPresenter>getController();
                        //passar a saida e passar os itens já alterados
                        //controller.setFields(cliente1, materiaisService
                        //._treatEstoqueNovaSaida(materiaisDoCLiente), SaidasPresenter.this, tipoSaida);
                        // controller.setFields(cliente1, itensEmpenho, null, tipoSaida);
                        controller.setFieldsEmpenhoToSaida(saida, itensEmpenho, SaidasPresenter.this, tipoSaida, saidaList, saidaCampos, cliente1);
                        Scene scene = new Scene(root);
                        stage.setTitle(cliente1.getDescCliente().replaceAll(" ", "") + " - Baixa Empenho");
                        stage.initOwner(acoesMenuButton.getScene().getWindow());
                        stage.initModality(Modality.APPLICATION_MODAL);
                        stage.setScene(scene);
                        stage.show();
                    }
                }
            }
        });

        saidasTableView.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                setProdutosInTable();
            }
        });

        saidasTableView.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyevent) {
                setProdutosInTable();
            }
        });

        relatorioButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                List saidasList = new ArrayList();
                saidasList = saidasTableView.getItems();

                int cod = cliente1.getCodControle();
                try {
                    chamarRelatorioClienteSaidas.chamarRelatorioSaidasClass(cod, saidasList, filtrosSelecionados);
                } catch (JRException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SaidasPresenter.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    public void setProdutosInTable() {
        if (saidasTableView.getSelectionModel().getSelectedItem() == null) {
            //Tabela selecionada, produto não selecionado.
        } else {
            produtosSaidaTableView.getItems().removeAll(saidaItens);
            saidaBusiness2 = null;
            saidaBusiness2 = (SaidaBusinessEntity) saidasTableView.getSelectionModel().getSelectedItem();
            saidaItens = saidaItensService.loadSaidaItens(saidaBusiness2.getCodSaida());
            populateTableViewSaidaItens(saidaItens);
        }
    }

    public void setCliente(ControleCliente cliente) {
        this.cliente1 = cliente;
        if (cliente1 != null) {
            if (!cliente1.getTerceiro()) {
                empenhoFilter.setVisible(false);
                sobrasFilter.setVisible(false);
                normalFilter.setVisible(false);
                transferenciaFilter.setVisible(false);
            }
            codClienteLabel.setText(cliente.getCodCliente());
            descricaoLabel.setText(cliente.getDescCliente());
            contratoLabel.setText(cliente.getNumContrato());
            opLabel.setText(cliente.getNumOp());
        }
        produtosSaidaTableView.getItems().clear();
        saidasTableView.getItems().clear();
        populateTableView(cliente1.getCodControle());
    }

    private void populateTableView(int codControle) {
        saidaCol.setCellValueFactory(new PropertyValueFactory<>("codSaida"));
        of.setCellValueFactory(new PropertyValueFactory<>("of"));
        pc.setCellValueFactory(new PropertyValueFactory<>("pc"));
        CodClienteTransfCol.setCellValueFactory(new PropertyValueFactory<>("clienteTransfString"));
        tipoOperCol.setCellValueFactory(new PropertyValueFactory<>("tipoOperacaoString"));
        totalKgCol.setCellValueFactory(new PropertyValueFactory<>("totalKg"));
        dtSaidaCol.setCellValueFactory(new PropertyValueFactory<>("dtSaidaFormatada"));
        clienteSaidas = saidasService.loadSaidasFilter(codControle);
        saidasTableView.setItems(clienteSaidas);
    }

    private void populateTableViewFilter() {
        saidaCol.setCellValueFactory(new PropertyValueFactory<>("codSaida"));
        of.setCellValueFactory(new PropertyValueFactory<>("of"));
        pc.setCellValueFactory(new PropertyValueFactory<>("pc"));
        CodClienteTransfCol.setCellValueFactory(new PropertyValueFactory<>("clienteTransfString"));
        tipoOperCol.setCellValueFactory(new PropertyValueFactory<>("tipoOperacaoString"));
        totalKgCol.setCellValueFactory(new PropertyValueFactory<>("totalKg"));
        dtSaidaCol.setCellValueFactory(new PropertyValueFactory<>("dtSaidaFormatada"));
        saidasTableView.setItems(getClientesTableViewFilter());
        produtosSaidaTableView.getItems().clear();
    }

    private void populateTableViewSaidaItens(ObservableList<ProdutosDescricao> obj) {
        codProdutoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        descricaoCol.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        totalEntradaCol.setCellValueFactory(new PropertyValueFactory<>("quantKg"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantUn"));
        produtosSaidaTableView.setItems(obj);

    }

    List<SaidaBusinessEntity> getSaidasPorTipo(List<SaidaBusinessEntity> saidas, char tipoOperacao) {
        return saidas.stream().filter(p -> p.getTipoOperacao() == tipoOperacao)
                .collect(Collectors.<SaidaBusinessEntity>toList());
    }

    private ObservableList<SaidaBusinessEntity> getClientesTableViewFilter() {

        filtrosSelecionados = "Todas";
        if (sobrasFilter.isSelected() && !normalFilter.isSelected() && !transferenciaFilter.isSelected() && !empenhoFilter.isSelected()) {
            return clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'S'));
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'T'));
            filtrosSelecionados = "Transferências";
            return clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'T'));
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Normal";
            return clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'N'));
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            filtrosSelecionados = "Estorno";
            return clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'E'));
            //Normal e transferencia selecionados
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Normal e Transferências ";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'N'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'T'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
            //Normal e Sobra Selecionados    
        } else if (sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Normal e Sobras";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'N'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'S'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
            //Normal e Estorno Selecionados
        } else if (!sobrasFilter.isSelected()
                && normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            filtrosSelecionados = "Normal e Estorno";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'N'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'E'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
            //Sobra e Transferencia Selecionados
        } else if (sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && !empenhoFilter.isSelected()) {
            filtrosSelecionados = "Sobras e Transferências";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'S'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'T'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
            //Sobra e Estorno Selecionados
        } else if (sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && !transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            filtrosSelecionados = "Sobras e Estorno";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'S'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'E'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
            //Transferencia e Estorno Selecionados
        } else if (!sobrasFilter.isSelected()
                && !normalFilter.isSelected()
                && transferenciaFilter.isSelected()
                && empenhoFilter.isSelected()) {
            filtrosSelecionados = "Estorno e Transferências";
            clienteSaidasFilter = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'E'));
            clienteSaidas2 = FXCollections.observableArrayList(getSaidasPorTipo(clienteSaidas, 'T'));
            clienteSaidasFilter.addAll(clienteSaidas2);
            return clienteSaidasFilter;
        }
        return clienteSaidas;
    }

    private void consultarBanco() {
        clienteSaidas = saidasService.loadSaidasFilter(cliente1.getCodControle());
    }

    private void eventFilter(CheckBox filtro) {
        filtro.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                populateTableViewFilter();

            }
        });
    }
}
