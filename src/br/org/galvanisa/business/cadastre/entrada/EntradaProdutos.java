package br.org.galvanisa.business.cadastre.entrada;

import java.math.BigDecimal;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author eric
 */
public class EntradaProdutos {

    IntegerProperty codigo = new SimpleIntegerProperty();
    IntegerProperty codEntrada = new SimpleIntegerProperty();
    IntegerProperty numItem = new SimpleIntegerProperty();
    StringProperty codProduto = new SimpleStringProperty();
    StringProperty descProduto = new SimpleStringProperty();
    ObjectProperty<BigDecimal> fconversao = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> quantKg = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> quantUn = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> comprimento = new SimpleObjectProperty<>();
    BooleanProperty sobra = new SimpleBooleanProperty();

    public EntradaProdutos() {
    }

    public EntradaProdutos(String codProduto,
            String descProduto,
            BigDecimal fconversao,
            BigDecimal quantKg,
            BigDecimal quantUn,
            BigDecimal comprimento,
            Boolean sobra) {
        this.codProduto.set(codProduto);
        this.descProduto.set(descProduto);
        this.fconversao.set(fconversao);
        this.quantKg.set(quantKg);
        this.quantUn.set(quantUn);
        this.comprimento.set(comprimento);
        this.sobra.set(sobra);
    }

    public Integer getCodigo() {
        return this.codigo.get();
    }

    public void setCodigo(int codigo) {
        this.codigo = new SimpleIntegerProperty(codigo);
    }

    public IntegerProperty codigoProperty() {
        return codigo;
    }

    public Integer getCodEntrada() {
        return this.codEntrada.get();
    }

    public void setCodEntrada(Integer codEntrada) {
        this.codEntrada = new SimpleIntegerProperty(codEntrada);
    }

    public IntegerProperty codEntradaProperty() {
        return codEntrada;
    }

    public Integer getNumItem() {
        return this.numItem.get();
    }

    public void setNumItem(Integer numItem) {
        this.numItem = new SimpleIntegerProperty(numItem);
    }

    public IntegerProperty numItemProperty() {
        return numItem;
    }

    public String getCodProduto() {
        return this.codProduto.get();
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = new SimpleStringProperty(codProduto);
    }

    public StringProperty codProdutoProperty() {
        return codProduto;
    }

    public String getDescProduto() {
        return this.descProduto.get();
    }

    public void setDescProduto(String descProduto) {
        this.descProduto = new SimpleStringProperty(descProduto);
    }

    public StringProperty descProdutoProperty() {
        return descProduto;
    }

    public BigDecimal getFconversao() {
        return this.fconversao.get();
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao = new SimpleObjectProperty<>(fconversao);
    }

    public ObjectProperty<BigDecimal> fconversaoProperty() {
        return fconversao;
    }

    public BigDecimal getQuantKg() {
        return this.quantKg.get();
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg = new SimpleObjectProperty<>(quantKg);
    }

    public ObjectProperty<BigDecimal> quantKgProperty() {
        return quantKg;
    }

    public BigDecimal getQuantUn() {
        return this.quantUn.get();
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn = new SimpleObjectProperty<>(quantUn);
    }

    public ObjectProperty<BigDecimal> quantUnProperty() {
        return quantUn;
    }

    public BigDecimal getComprimento() {
        return this.comprimento.get();
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = new SimpleObjectProperty<>(comprimento);
    }

    public ObjectProperty<BigDecimal> comprimentoProperty() {
        return comprimento;
    }

    public Boolean getSobra() {
        return this.sobra.get();
    }

    public void setSobra(Boolean sobra) {
        this.sobra = new SimpleBooleanProperty(sobra);
    }

    public BooleanProperty sobraProperty() {
        return sobra;
    }

}
