/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.saidaitens;

import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.SaidaItens;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class SaidaItensModel {

    private final String QUERY_BUSCAR_SAIDA_ITENS_POR_SAIDA = "from SaidaItens where cod_saida = :cod1";
    private final String QUERY_BUSCAR_PRODUTOS_SAIDA = "from Produtos where codigo = :cod1";
    private ObservableList<SaidaItens> saidasItens;
    private ObservableList<Produtos> produtos;

    public ObservableList<SaidaItens> loadProdutosSaida(int codSaida) {
        List<SaidaItens> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_SAIDA_ITENS_POR_SAIDA;
            Query q = session.createQuery(hql);
            q.setParameter("cod1", codSaida);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        saidasItens = FXCollections.observableArrayList(list);
        return saidasItens;
    }

    public Produtos loadProdutosSaidaItens(String codProduto) {
        List<Produtos> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_PRODUTOS_SAIDA;
            Query q = session.createQuery(hql);
            q.setParameter("cod1", codProduto);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {

        }
        produtos = FXCollections.observableArrayList(list);
        return produtos.get(0);
    }
}