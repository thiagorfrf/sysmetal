package br.org.galvanisa.business.entity.produtos;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author eric
 */
public class ProdutosSearch {

    private SimpleStringProperty codigo = new SimpleStringProperty();
    private SimpleStringProperty descricao = new SimpleStringProperty();
    private ObjectProperty<BigDecimal> fconversao = new SimpleObjectProperty<>();

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public String getCodigo() {
        return codigo.get();
    }

    public SimpleStringProperty descricaoProperty() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    public String getDescricao() {
        return descricao.get();
    }

    public SimpleObjectProperty<BigDecimal> fconversaoProperty() {
        return (SimpleObjectProperty<BigDecimal>) fconversao;
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao.set(fconversao);
    }

    public BigDecimal getFconversao() {
        return fconversao.get();
    }
}
