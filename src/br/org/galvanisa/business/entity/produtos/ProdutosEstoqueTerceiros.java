/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.produtos;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author thiago
 */
public class ProdutosEstoqueTerceiros {

    SimpleStringProperty clienteDescricao = new SimpleStringProperty();

    SimpleStringProperty codigo = new SimpleStringProperty();
    SimpleStringProperty descricao = new SimpleStringProperty();
    ObjectProperty<BigDecimal> fconversao = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> quantKg = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> quantUn = new SimpleObjectProperty<>();

    public ProdutosEstoqueTerceiros() {

    }

    public ProdutosEstoqueTerceiros(String clienteDescricao, String codigo, String descricao, BigDecimal fconversao,
            BigDecimal quantKg, BigDecimal quantUn, BigDecimal quantEmpenho, BigDecimal quantLiberada
    ) {
        this.clienteDescricao = new SimpleStringProperty(clienteDescricao);
        this.codigo = new SimpleStringProperty(codigo);
        this.descricao = new SimpleStringProperty(descricao);
        this.fconversao = new SimpleObjectProperty<BigDecimal>(fconversao);
        this.quantKg = new SimpleObjectProperty<BigDecimal>(quantKg);
        this.quantUn = new SimpleObjectProperty<BigDecimal>(quantUn);

    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }

    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    public SimpleStringProperty descricaoProperty() {
        return descricao;
    }

    public String getClienteDescricao() {
        return clienteDescricao.get();
    }

    public void setClienteDescricao(String clienteDescricao) {
        this.clienteDescricao.set(clienteDescricao);
    }

    public void setCodigo(SimpleStringProperty codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(SimpleStringProperty descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getFconversao() {
        return fconversao.get();
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao.set(fconversao);
    }

    public BigDecimal getQuantKg() {
        return quantKg.get();
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg.set(quantKg);
    }

    public BigDecimal getQuantUn() {
        return quantUn.get();
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn.set(quantUn);
    }
}
