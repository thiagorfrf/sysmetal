
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.produtos;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
/**
 *
 * @author Thiago
 */
public class ProdutosProperty {
    SimpleStringProperty codigo = new SimpleStringProperty();
    SimpleStringProperty descricao = new SimpleStringProperty();
    ObjectProperty<BigDecimal> fator = new SimpleObjectProperty<BigDecimal>();

    public ProdutosProperty() {

    }

    public ProdutosProperty(String codigo, String descricao, BigDecimal estoqueKg,
            BigDecimal fator, BigDecimal estoqueUn, BigDecimal empKg, BigDecimal empUn,
            BigDecimal quantLibUn, BigDecimal solicitadoUn) {
        this.codigo = new SimpleStringProperty(codigo);
        this.descricao = new SimpleStringProperty(descricao);
        this.fator = new SimpleObjectProperty<BigDecimal>(fator);
    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }

    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    public SimpleStringProperty descricaoProperty() {
        return descricao;
    }

    public BigDecimal getFator() {
        return fator.get();
    }

    public void setFator(BigDecimal fator) {
        this.fator.set(fator);
    }
}