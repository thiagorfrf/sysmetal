/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.entrada;

import java.math.BigDecimal;

/**
 *
 * @author thiago
 */
public class EntradaBusinessEntity {

    private int codEntrada;
    private String codCliente;
    private String numNf;
    private String numFornecedor;
    private String numCertificado;
    private Character tipoOperacao;
    private BigDecimal totalKg;
    private String dtEntrada;

    private String tipoOperacaoString;
    private String descFornecedor;
    private String dataFormatada;

    public EntradaBusinessEntity() {
    }

    public EntradaBusinessEntity(int codEntrada, String codCliente, String numNf, String numFornecedor, String numCertificado, Character tipoOperacao, BigDecimal totalKg, String dtEntrada) {
        this.codEntrada = codEntrada;
        this.codCliente = codCliente;
        this.numNf = numNf;
        this.numFornecedor = numFornecedor;
        this.numCertificado = numCertificado;
        this.tipoOperacao = tipoOperacao;
        this.totalKg = totalKg;
        this.dtEntrada = dtEntrada;
    }

    public EntradaBusinessEntity(int codEntrada, String codCliente, String numNf, String numFornecedor, String numCertificado, Character tipoOperacao, BigDecimal totalKg, String dtEntrada, String tipoOperacaoString, String descFornecedor, String dataFormatada) {
        this.codEntrada = codEntrada;
        this.codCliente = codCliente;
        this.numNf = numNf;
        this.numFornecedor = numFornecedor;
        this.numCertificado = numCertificado;
        this.tipoOperacao = tipoOperacao;
        this.totalKg = totalKg;
        this.dtEntrada = dtEntrada;
        this.tipoOperacaoString = tipoOperacaoString;
        this.descFornecedor = descFornecedor;
        this.dataFormatada = dataFormatada;
    }

    public int getCodEntrada() {
        return codEntrada;
    }

    public void setCodEntrada(int codEntrada) {
        this.codEntrada = codEntrada;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getNumNf() {
        return numNf;
    }

    public void setNumNf(String numNf) {
        this.numNf = numNf;
    }

    public String getNumFornecedor() {
        return numFornecedor;
    }

    public void setNumFornecedor(String numFornecedor) {
        this.numFornecedor = numFornecedor;
    }

    public String getNumCertificado() {
        return numCertificado;
    }

    public void setNumCertificado(String numCertificado) {
        this.numCertificado = numCertificado;
    }

    public Character getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(Character tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }

    public BigDecimal getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(BigDecimal totalKg) {
        this.totalKg = totalKg;
    }

    public String getDtEntrada() {
        return dtEntrada;
    }

    public void setDtEntrada(String dtEntrada) {
        this.dtEntrada = dtEntrada;
    }

    public String getTipoOperacaoString() {
        return tipoOperacaoString;
    }

    public void setTipoOperacaoString(String tipoOperacaoString) {
        this.tipoOperacaoString = tipoOperacaoString;
    }

    public String getDescFornecedor() {
        return descFornecedor;
    }

    public void setDescFornecedor(String descFornecedor) {
        this.descFornecedor = descFornecedor;
    }

    public String getDataFormatada() {
        return dataFormatada;
    }

    public void setDataFormatada(String dataFormatada) {
        this.dataFormatada = dataFormatada;
    }
}
