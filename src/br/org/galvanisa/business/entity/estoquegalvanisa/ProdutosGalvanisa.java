/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.org.galvanisa.business.entity.estoquegalvanisa;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
/**
 *
 * @author eric
 */
public class ProdutosGalvanisa {
    SimpleStringProperty codigo = new SimpleStringProperty();
    SimpleStringProperty descricao = new SimpleStringProperty();
    ObjectProperty<BigDecimal> estoqueKg = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> fator = new SimpleObjectProperty<BigDecimal>();
    ObjectProperty<BigDecimal> estoqueUn = new SimpleObjectProperty<BigDecimal>();
    ObjectProperty<BigDecimal> empKg = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> empUn = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> quantLibUn = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> solicitadoUn = new SimpleObjectProperty<>();
    
    public ProdutosGalvanisa(){
        
    }
    
    public ProdutosGalvanisa(String codigo, String descricao, BigDecimal estoqueKg,
            BigDecimal fator, BigDecimal estoqueUn, BigDecimal empKg, BigDecimal empUn,
            BigDecimal quantLibUn, BigDecimal solicitadoUn) {
        this.codigo= new SimpleStringProperty(codigo);
        this.descricao = new SimpleStringProperty(descricao);
        this.estoqueKg = new SimpleObjectProperty<BigDecimal>(estoqueKg);
        this.fator = new SimpleObjectProperty<BigDecimal>(fator);
        this.estoqueUn = new SimpleObjectProperty<BigDecimal>(estoqueUn);
        this.empKg = new SimpleObjectProperty<BigDecimal>(empKg);
        this.empUn = new SimpleObjectProperty<BigDecimal>(empUn);
        this.quantLibUn = new SimpleObjectProperty<BigDecimal>(quantLibUn);
        this.solicitadoUn = new SimpleObjectProperty<BigDecimal>(solicitadoUn);
    }
    
    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }
    
    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    public SimpleStringProperty descricaoProperty() {
        return descricao;
    }
    
    public BigDecimal getEstoqueKg() {
        return estoqueKg.get();
    }

    public void setEstoqueKg(BigDecimal estoqueKg) {
        this.estoqueKg.set(estoqueKg);
    }

    public BigDecimal getFator() {
        return fator.get();
    }

    public void setFator(BigDecimal fator) {
        this.fator.set(fator);
    }

    public BigDecimal getEstoqueUn() {
        return estoqueUn.get();
    }

    public void setEstoqueUn(BigDecimal estoqueUn) {
        this.estoqueUn.set(estoqueUn);
    }

    public BigDecimal getEmpKg() {
        return empKg.get();
    }

    public void setEmpKg(BigDecimal empKg) {
        this.empKg.set(empKg);
    }

    public BigDecimal getEmpUn() {
        return empUn.get();
    }

    public void setEmpUn(BigDecimal empUn) {
        this.empUn.set(empUn);
    }

    public BigDecimal getQuantLibUn() {
        return quantLibUn.get();
    }

    public void setQuantLibUn(BigDecimal quantUn) {
        this.quantLibUn.set(quantUn);
    }

    public BigDecimal getSolicitadoUn() {
        return solicitadoUn.get();
    }

    public void setSolicitadoUn(BigDecimal solicitadoUn) {
        this.solicitadoUn.set(solicitadoUn);
    }
    
}