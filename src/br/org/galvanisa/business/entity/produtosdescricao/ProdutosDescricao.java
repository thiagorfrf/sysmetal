/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.produtosdescricao;

import java.math.BigDecimal;

/**
 *
 * @author thiago
 */
public class ProdutosDescricao {
    
    private int codigoMovimentacao;
    
    private int codEstoque;
    private String codigo;
    private String descricao;
    private BigDecimal quantKg;
    private BigDecimal fconversao;
    private BigDecimal quantUn;

    public ProdutosDescricao(String codigo, int codEstoque, String descricao, BigDecimal quantKg, BigDecimal fconversao, BigDecimal quantUn) {
        this.codEstoque = codEstoque;
        this.codigo = codigo;
        this.descricao = descricao;
        this.quantKg = quantKg;
        this.fconversao = fconversao;
        this.quantUn = quantUn;
    }

    public ProdutosDescricao() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCodEstoque() {
        return codEstoque;
    }

    public void setCodEstoque(int codEstoque) {
        this.codEstoque = codEstoque;
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getQuantKg() {
        return quantKg;
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg = quantKg;
    }

    public BigDecimal getFconversao() {
        return fconversao;
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao = fconversao;
    }

    public BigDecimal getQuantUn() {
        return quantUn;
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn = quantUn;
    }

    public int getCodigoMovimentacao() {
        return codigoMovimentacao;
    }

    public void setCodigoMovimentacao(int codigoMovimentaco) {
        this.codigoMovimentacao = codigoMovimentaco;
    }
}