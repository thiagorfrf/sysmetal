/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.saida;

import java.math.BigDecimal;

/**
 *
 * @author thiago
 */
public class SaidaBusinessEntity {

    private int codSaida;
    private Integer codControleCliente;
    private Integer codControleOf;
    private String codClienteTransf;
    private Character tipoOperacao;
    private BigDecimal totalKg;
    private String dtSaida;

    private String of;
    private String pc;
    private String tipoOperacaoString;
    private String controleClienteString;
    private String clienteTransfString;
    private String dtSaidaFormatada;

    public SaidaBusinessEntity() {
    }

    public SaidaBusinessEntity(int codSaida, Integer codControleCliente, Integer codControleOf, String codClienteTransf, Character tipoOperacao, BigDecimal totalKg, String dtSaida) {
        this.codSaida = codSaida;
        this.codControleCliente = codControleCliente;
        this.codControleOf = codControleOf;
        this.codClienteTransf = codClienteTransf;
        this.tipoOperacao = tipoOperacao;
        this.totalKg = totalKg;
        this.dtSaida = dtSaida;
    }

    public SaidaBusinessEntity(int codSaida, Integer codControleCliente, Integer codControleOf, String pc, String of, String codClienteTransf, Character tipoOperacao, BigDecimal totalKg, String dtSaida, String clienteString, String clienteTransfString, String dtSaidaFormatada) {
        this.codSaida = codSaida;
        this.codControleCliente = codControleCliente;
        this.codControleOf = codControleOf;
        this.codClienteTransf = codClienteTransf;
        this.tipoOperacao = tipoOperacao;
        this.totalKg = totalKg;
        this.dtSaida = dtSaida;

        this.pc = pc;
        this.of = of;
        this.controleClienteString = clienteString;
        this.clienteTransfString = clienteTransfString;
        this.dtSaidaFormatada = dtSaidaFormatada;
    }

    public int getCodSaida() {
        return codSaida;
    }

    public void setCodSaida(int codSaida) {
        this.codSaida = codSaida;
    }

    public Integer getCodControleCliente() {
        return codControleCliente;
    }

    public void setCodControleCliente(Integer codControleCliente) {
        this.codControleCliente = codControleCliente;
    }

    public String getCodClienteTransf() {
        return codClienteTransf;
    }

    public void setCodClienteTransf(String codClienteTransf) {
        this.codClienteTransf = codClienteTransf;
    }

    public Character getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(Character tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }

    public BigDecimal getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(BigDecimal totalKg) {
        this.totalKg = totalKg;
    }

    public String getDtSaida() {
        return dtSaida;
    }

    public void setDtSaida(String dtSaida) {
        this.dtSaida = dtSaida;
    }

    public String getControleClienteString() {
        return controleClienteString;
    }

    public void setControleClienteString(String clienteString) {
        this.controleClienteString = clienteString;
    }

    public String getTipoOperacaoString() {
        return tipoOperacaoString;
    }

    public void setTipoOperacaoString(String tipoOperacaoString) {
        this.tipoOperacaoString = tipoOperacaoString;
    }

    public String getClienteTransfString() {
        return clienteTransfString;
    }

    public void setClienteTransfString(String clienteTransfString) {
        this.clienteTransfString = clienteTransfString;
    }

    public String getDtSaidaFormatada() {
        return dtSaidaFormatada;
    }

    public void setDtSaidaFormatada(String dtSaidaFormatada) {
        this.dtSaidaFormatada = dtSaidaFormatada;
    }

    public Integer getCodControleOf() {
        return codControleOf;
    }

    public void setCodControleOf(Integer codControleOf) {
        this.codControleOf = codControleOf;
    }

    public String getOf() {
        return of;
    }

    public void setOf(String of) {
        this.of = of;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }
}