package br.org.galvanisa.business.entity.materiais;

import java.math.BigDecimal;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 *
 * @author eric
 */
public class MateriaisData implements java.io.Serializable {

    private int codigo;
    private int codEstoque;
    private String codCliente;
    private String codProduto;
    private String descProduto;
    private BigDecimal quantKg;
    private BigDecimal fconversao;
    private BigDecimal quantUn;
    private BigDecimal quantEmpenho;
    private BigDecimal quantLiberada;
    private BigDecimal comprimento;
    private Boolean sobra;
    private BooleanProperty escolha;

    public MateriaisData() {
    }

    public MateriaisData(int codigo) {
        this.codigo = codigo;
    }

    public MateriaisData(int codigo, int codEstoque, String codCliente, String codProduto,
            String descProduto, BigDecimal quantKg, BigDecimal fconversao,
            BigDecimal quantUn, BigDecimal quantEmpenho,
            BigDecimal quantLiberada, BigDecimal comprimento,
            Boolean sobra, boolean escolha) {
        this.codigo = codigo;
        this.codEstoque = codEstoque;
        this.codCliente = codCliente;
        this.codProduto = codProduto;
        this.descProduto = descProduto;
        this.quantKg = quantKg;
        this.fconversao = fconversao;
        this.quantUn = quantUn;
        this.quantEmpenho = quantEmpenho;
        this.quantLiberada = quantLiberada;
        this.comprimento = comprimento;
        this.sobra = sobra;
        this.escolha.set(escolha);
    }

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodEstoque() {
        return codEstoque;
    }

    public void setCodEstoque(int codEstoque) {
        this.codEstoque = codEstoque;
    }
    
    public String getCodCliente() {
        return this.codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodProduto() {
        return this.codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public String getDescProduto() {
        return this.descProduto;
    }

    public void setDescProduto(String descProduto) {
        this.descProduto = descProduto;
    }

    public BigDecimal getQuantKg() {
        return this.quantKg;
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg = quantKg;
    }

    public BigDecimal getFconversao() {
        return this.fconversao;
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao = fconversao;
    }

    public BigDecimal getQuantUn() {
        return this.quantUn;
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn = quantUn;
    }

    public BigDecimal getQuantEmpenho() {
        return this.quantEmpenho;
    }

    public void setQuantEmpenho(BigDecimal quantEmpenho) {
        this.quantEmpenho = quantEmpenho;
    }

    public BigDecimal getQuantLiberada() {
        return this.quantLiberada;
    }

    public void setQuantLiberada(BigDecimal quantLiberada) {
        this.quantLiberada = quantLiberada;
    }

    public BigDecimal getComprimento() {
        return this.comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    public Boolean getSobra() {
        return this.sobra;
    }

    public void setSobra(Boolean sobra) {
        this.sobra = sobra;
    }

    public boolean getEscolha() {
        return this.escolha.get();
    }

    public void setEscolha(boolean escolha) {
        this.escolha = new SimpleBooleanProperty(escolha);
    }

    public BooleanProperty escolhaProperty() {
        return escolha;
    }
}
