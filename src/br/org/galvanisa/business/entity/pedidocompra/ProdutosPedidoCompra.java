/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entity.pedidocompra;

import java.math.BigDecimal;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author thiago
 */
public class ProdutosPedidoCompra {

    SimpleStringProperty codigo = new SimpleStringProperty();
    SimpleStringProperty descricao = new SimpleStringProperty();
    SimpleStringProperty codProduto = new SimpleStringProperty();
    SimpleStringProperty codPedido = new SimpleStringProperty();
    ObjectProperty<BigDecimal> quantSolicitado = new SimpleObjectProperty<>();
    ObjectProperty<BigDecimal> fator = new SimpleObjectProperty<BigDecimal>();
    ObjectProperty<BigDecimal> qtdPedidoKg = new SimpleObjectProperty<BigDecimal>();
    ObjectProperty<BigDecimal> qtdPedidoUn = new SimpleObjectProperty<BigDecimal>();
    ObjectProperty<BigDecimal> qtdSolicitada = new SimpleObjectProperty<>();
    SimpleStringProperty dataEntregaEstimada = new SimpleStringProperty();

    public ProdutosPedidoCompra(String codigo, String descricao, String codProduto, String codPedido, BigDecimal quantSolicitado,
            BigDecimal fator, BigDecimal qtdPedidoKg, BigDecimal empKg,
            BigDecimal solicitadoUn, BigDecimal qtdPedidoUn, String dataEntregaEstimada) {
        this.codigo = new SimpleStringProperty(codigo);
        this.descricao = new SimpleStringProperty(descricao);
        this.codPedido = new SimpleStringProperty(codPedido);
        this.codProduto = new SimpleStringProperty(codProduto);
        this.quantSolicitado = new SimpleObjectProperty<BigDecimal>(quantSolicitado);
        this.fator = new SimpleObjectProperty<BigDecimal>(fator);
        this.qtdPedidoKg = new SimpleObjectProperty<BigDecimal>(qtdPedidoKg);
        this.qtdPedidoUn = new SimpleObjectProperty<BigDecimal>(qtdPedidoUn);
        this.qtdSolicitada = new SimpleObjectProperty<BigDecimal>(solicitadoUn);
        this.dataEntregaEstimada = new SimpleStringProperty(dataEntregaEstimada);
    }

    public ProdutosPedidoCompra() {
    }

    public String getCodigo() {
        return codigo.get();
    }

    public void setCodigo(String codigo) {
        this.codigo.set(codigo);
    }

    public SimpleStringProperty codigoProperty() {
        return codigo;
    }

    public String getDescricao() {
        return descricao.get();
    }

    public void setDescricao(String descricao) {
        this.descricao.set(descricao);
    }

    public SimpleStringProperty descricaoProperty() {
        return descricao;
    }

    public String getCodProduto() {
        return codProduto.get();
    }

    public void setCodProduto(String codProduto) {
        this.codProduto.set(codProduto);
    }

    public String getCodPedido() {
        return codPedido.get();
    }

    public void setCodPedido(String codPedido) {
        this.codPedido.set(codPedido);
    }

    public BigDecimal getEstoqueKg() {
        return quantSolicitado.get();
    }

    public void setEstoqueKg(BigDecimal estoqueKg) {
        this.quantSolicitado.set(estoqueKg);
    }

    public BigDecimal getFator() {
        
        return fator.get();
    }

    public void setFator(BigDecimal fator) {
        this.fator.set(fator);
    }

    public BigDecimal getQtdPedidoKg() {
        return qtdPedidoKg.get();
    }

    public void setQtdPedidoKg(BigDecimal qtdPedidoKg) {
        this.qtdPedidoKg.set(qtdPedidoKg);
    }

    public BigDecimal getQtdSolicitada() {
        return qtdSolicitada.get();
    }

    public void setQtdSolicitada(BigDecimal qtdSolicitada) {
        this.qtdSolicitada.set(qtdSolicitada);
    }

    public String getDataEntregaEstimada() {

        String data = dataEntregaEstimada.get().substring(6, 8) + '/'
                + dataEntregaEstimada.get().substring(4, 6) + '/'
                + dataEntregaEstimada.get().substring(0, 4);

        return data;
    }

    public void setDataEntregaEstimada(String dataEntregaEstimada) {
        this.dataEntregaEstimada.set(dataEntregaEstimada);
    }

    public BigDecimal getQtdPedidoUn() {
        return qtdPedidoUn.get();
    }

    public void setQtdPedidoUn(BigDecimal qtdPedidoUn) {
        this.qtdPedidoUn.set(qtdPedidoUn);
    }

}
