/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entradaitens;

import br.org.galvanisa.business.entity.EntradaItens;
import br.org.galvanisa.business.entity.Produtos;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class EntradaItensModel {

    private final String QUERY_BUSCAR_ENTRADA_ITENS_POR_ENTRADA = "from EntradaItens where cod_Entrada = :cod1";
    private final String QUERY_BUSCAR_PRODUTOS_ENTRADA = "from Produtos where codigo = :cod1";
    private final String QUERY_BUSCAR_TODOS_PRODUTOS_ENTRADA = "from EntradaItens";
    private ObservableList<EntradaItens> entradasItens;
    private ObservableList<Produtos> produtos;

    public ObservableList<EntradaItens> loadProdutosEntrada(int codEntrada) {
        List<EntradaItens> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ENTRADA_ITENS_POR_ENTRADA;
            Query q = session.createQuery(hql);
            q.setParameter("cod1", codEntrada);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        entradasItens = FXCollections.observableArrayList(list);
        return entradasItens;
    }

    public Produtos loadProdutosEntradaItens(String codProduto) {
        List<Produtos> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_PRODUTOS_ENTRADA;
            Query q = session.createQuery(hql);
            q.setParameter("cod1", codProduto);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {

        }
        produtos = FXCollections.observableArrayList(list);
        return produtos.get(0);
    }

    public ObservableList<EntradaItens> loadAllEstoque() {
        List<EntradaItens> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_TODOS_PRODUTOS_ENTRADA;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        entradasItens = FXCollections.observableArrayList(list);
        return entradasItens;
    }

    public ObservableList<EntradaItens> loadAllProdutosEntrada() {
        List<EntradaItens> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_TODOS_PRODUTOS_ENTRADA;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        entradasItens = FXCollections.observableArrayList(list);
        return entradasItens;
    }
    
        public void alterEntradaItem(EntradaItens get) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(get);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

}
