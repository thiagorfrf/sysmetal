package br.org.galvanisa.business.controlecliente;

import br.org.galvanisa.business.entity.ControleCliente;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 * @author thiago
 */
public class ControleClienteModel {

    private ObservableList<ControleCliente> controleClientes;
    final String QUERY_ACTIVE_CLIENTS = "from ControleCliente where status=true";
    final String QUERY_CLIENTES_ATIVOS_E_TERCEIROS = "from ControleCliente where status=true AND terceiro = true";
    final String QUERY_CLIENTES_ATIVOS_E_ESTOQUE_GALVANISA = "from ControleCliente where status=true AND terceiro = false";
    final String QUERY_TODOS_CLIENTS = "from ControleCliente";
    final String QUERY_MUDAR_STATUS = "from ControleCliente where cod_cliente = :codigoCliente";
    final String QUERY_TODOS_CLIENTES_POR_CODIGO = "from ControleCliente where cod_cliente = :cod_cliente";
    final String QUERY_TODOS_CLIENTES_POR_CODIGO_CONTROLE = "from ControleCliente where cod_controle = :cod_controle";
    final String QUERY_CLIENTES_MP_INTERNA = "from ControleCliente where terceiro=false";
    final String QUERY_CLIENTES_MP_TERCEIROS = "from ControleCliente where terceiro=true";

    public void addControleCliente(ControleCliente cliente) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(cliente);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public ObservableList<ControleCliente> getAllControleCliente() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_CLIENTS;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        controleClientes = FXCollections.observableArrayList(resultList);
        return controleClientes;
    }

    public ObservableList<ControleCliente> getAllClientesTerceiros() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CLIENTES_MP_TERCEIROS;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        controleClientes = FXCollections.observableArrayList(resultList);
        return controleClientes;
    }

    public ObservableList<ControleCliente> getAllClientesMPInterna() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CLIENTES_MP_INTERNA;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        controleClientes = FXCollections.observableArrayList(resultList);
        return controleClientes;
    }

    public ControleCliente getAllControleClientePorCodigo(String cod_cliente) {
        List<ControleCliente> resultList = new ArrayList<>();
        ControleCliente resut = new ControleCliente();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_CLIENTES_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("cod_cliente", cod_cliente);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        resut = resultList.get(0);
        return resut;
    }
    

    public List<ControleCliente> getControleCliente() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_ACTIVE_CLIENTS;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return resultList;
    }

    public String alterStatus(ControleCliente cliente) {
        String status = "atualizacao_sucesso";
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {
            session.update(cliente);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            System.out.println("Exceção em Banco de dados - atualizarUsuario");
            status = he.getCause().getLocalizedMessage();
        }
        return status;
    }

    public void updateCliente(ControleCliente cliente) {
   
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(cliente);
            session.getTransaction().commit();
            session.close();

    }

    public ControleCliente getAllControleClientePorCodigoControle(int codigo) {
        List<ControleCliente> resultList = new ArrayList<>();
        ControleCliente resut = new ControleCliente();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_CLIENTES_POR_CODIGO_CONTROLE;
            Query q = session.createQuery(hql);
            q.setParameter("cod_controle", codigo);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        resut = resultList.get(0);
        return resut;
    }

    public List<ControleCliente> getControleClienteAtivosTerceiros() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CLIENTES_ATIVOS_E_TERCEIROS;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return resultList;
    }

    public List<ControleCliente> getControleClienteAtivosGalvanisa() {
        List<ControleCliente> resultList = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CLIENTES_ATIVOS_E_ESTOQUE_GALVANISA;
            Query q = session.createQuery(hql);
            resultList = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return resultList;
    }
}
