
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.estoquegalvanisa;

import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.estoquegalvanisa.ProdutosGalvanisa;
import br.org.galvanisa.presentation.produtos.LoadProdutosTerceiroService;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author eric
 */
public class EstoqueGalvanisaModel {

    private ObservableList<ProdutosGalvanisa> produtosGalvanisa = FXCollections.observableArrayList();
    private Connection connection = null;
    private LoadProdutosTerceiroService loadProdutosService = new LoadProdutosTerceiroService();
    private ObservableList<Produtos> produtos = loadProdutosService.loadProdutos();

    public ObservableList<ProdutosGalvanisa> getProdutosGalvanisa() {

        try {
            connectSqlServer();
            loadProdutos(connection);
        } catch (SQLException ex) {
            Logger.getLogger(EstoqueGalvanisaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        treatProdutosGalvanisa();
        return produtosGalvanisa;
    }

    private void connectSqlServer() throws SQLException {
        System.out.println("----SQLServer connect test");
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your sqlServer jdpc driver ?");
            e.printStackTrace();
            return;
        }
        System.out.println("SQLServer JDBC Driver Registeerd");
        try {
            connection = DriverManager.getConnection(
                    "jdbc:sqlserver://192.168.0.13:1433;databaseName=dadosadv", "sa", "Sa1");
        } catch (SQLException e) {
            System.out.println("connection fail!");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    private void loadProdutos(Connection connection) throws SQLException {

        Statement stmt = null;
        stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery("select B2_COD, B2_QATU, B2_QTSEGUM, B2_QEMPSA "
                + "from SB2010 where B2_FILIAL = '03' AND D_E_L_E_T_ <> '*'\n"
                + "AND B2_QTSEGUM > 0 AND B2_COD in "
                + "(select B1_COD from SB1010 where B1_FILIAL = '03' and D_E_L_E_T_ <> '*' "
                + "and B1_GRUPO = '02');");
        
        while (rs.next()) {
            ProdutosGalvanisa swap = new ProdutosGalvanisa();
            swap.setCodigo(rs.getString("B2_COD"));
            swap.setDescricao(null);
            swap.setEstoqueKg(rs.getBigDecimal("B2_QATU"));
            swap.setFator(BigDecimal.ZERO);
            swap.setEstoqueUn(rs.getBigDecimal("B2_QTSEGUM"));
            swap.setEmpKg(rs.getBigDecimal("B2_QEMPSA"));
            swap.setEmpUn(BigDecimal.ZERO);
            swap.setQuantLibUn(BigDecimal.ZERO);
            swap.setSolicitadoUn(BigDecimal.ZERO);
            produtosGalvanisa.add(swap);
        }
        rs.close();
        stmt.close();
        connection.close();
    }

    private void treatProdutosGalvanisa() {
        for (int i = 0; i < produtosGalvanisa.size(); i++) {
            for (int j = 0; j < produtos.size(); j++) {

                if (produtosGalvanisa.get(i).getCodigo().equals(produtos.get(j).getCodigo())) {
                    produtosGalvanisa.get(i).setDescricao(produtos.get(j).getDescricao());
                    produtosGalvanisa.get(i).setFator(produtos.get(j).getFconversao());

                    if (!(produtosGalvanisa.get(i).getEmpKg().compareTo(BigDecimal.ZERO) == 0)) {
                        produtosGalvanisa.get(i).setEmpUn(
                                produtosGalvanisa.get(i).getEmpKg().divide(
                                        produtos.get(j).getFconversao(), 2));
                    }
                    produtosGalvanisa.get(i).setQuantLibUn(
                            produtosGalvanisa.get(i).getEstoqueUn().subtract(
                                    produtosGalvanisa.get(i).getEmpUn()));
                }
            }
        }
    }
}
