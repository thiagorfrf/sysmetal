package br.org.galvanisa.business.produtos;

import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.produtos.ProdutosProperty;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class LoadProdutosModel {

    final String QUERY_TODOS_PRODUTOS = "from Produtos";
    final String QUERY_PRODUTOS_POR_CODIGO = "from Produtos where codigo = :codigo";

    public ObservableList<Produtos> loadProdutos() {
        List<Produtos> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_PRODUTOS;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return FXCollections.observableArrayList(list);
    }
    
        public List<Produtos> loadProdutosList() {
        List<Produtos> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_PRODUTOS;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public ObservableList<ProdutosProperty> loadProdutosProperty() {

        ObservableList<ProdutosProperty> objProdutosPropertyList = FXCollections.observableArrayList();

        List<Produtos> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODOS_PRODUTOS;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
            for (int i = 0; i < list.size(); i++) {
                ProdutosProperty objProdutosProperty = new ProdutosProperty();
                objProdutosProperty.setCodigo(list.get(i).getCodigo());
                objProdutosProperty.setDescricao(list.get(i).getDescricao());
                objProdutosProperty.setFator(list.get(i).getFconversao());
                objProdutosPropertyList.add(objProdutosProperty);
            }

        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return FXCollections.observableArrayList(objProdutosPropertyList);
    }

    public ObservableList<Produtos> loadProdutosPorCodigo(String codigo) {
        List<Produtos> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_PRODUTOS_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigo", codigo);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return FXCollections.observableArrayList(list);
    }
}