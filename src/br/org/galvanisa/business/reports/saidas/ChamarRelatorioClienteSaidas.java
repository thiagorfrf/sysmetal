/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.reports.saidas;

import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.entity.saida.SaidaBusinessEntity;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import utilities.ConnectionFactory;
import utilities.ReportUtils;

/**
 *
 * @author Thiago Rodrigues
 */
public class ChamarRelatorioClienteSaidas {

    private String path = null;
    private String url = getClass().getResource(getClass().getSimpleName() + ".class").getPath();
    public Boolean ofPreenchida = null;

    public void chamarRelatorioSaidasCliente(int codControleCliente) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("Saidas.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("CLIENTE", codControleCliente);
        ReportUtils.openReport("Saídas", inputStream, parametros, ConnectionFactory.getConexao());
    }
    
    
    public void chamarRelatorioTotalSaidas() throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalSaidas.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        ReportUtils.openReport("Total Saídas", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioSaidasData(int codCliente, String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("SaidaData.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Saidas por Data", inputStream, parametros, ConnectionFactory.getConexao());
    }
    
    
        public void chamarRelatorioTodasSaidasData(String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalSaidasData.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Todas as Saidas por Data", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioSaidasClass(int codCliente, List<SaidaBusinessEntity> saidaBusinessEntity, String tipo) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalSaidasTela.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        parametros.put("TIPO", tipo);

        // criando os dados que serão passados ao datasource
        //List<SaidaBusinessEntity> dados = new ArrayList();
        //dados.add((SaidaBusinessEntity) saidaBusinessEntity);
        // criando o datasource com os dados criados
        JRDataSource ds = new JRBeanCollectionDataSource(saidaBusinessEntity);

        ReportUtils.openReport("Saidas por Data", inputStream, parametros, ds);
    }

    public void chamarRelatorioSaidasOF(int codCliente, List<ControleOf> controleOfsList, List<ControleOf> ofsList, String descricao) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("SaidasOf.jasper");
        Map parametros = new HashMap();

        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        parametros.put("CLIENTE", descricao);

        JRDataSource ds = new JRBeanCollectionDataSource(null);
        ds = new JRBeanCollectionDataSource(ofsList);
        //ds = new JRBeanCollectionDataSource(controleOfsList);

        ReportUtils.openReport("Saidas por OF", inputStream, parametros, ds);
    }

       private String getPath() {
        File dir = new File(url).getParentFile();
        path = dir.getParent();
        if (dir.getPath().contains(".jar")) {
            while (dir.getPath().contains(".jar")) {
                path = dir.getAbsolutePath();
                return findJarParentPath(path);
            }
        }
        return path;
    }

    private String findJarParentPath(String dir) {
        String barra = path.substring(dir.indexOf("sysmetal")-1,dir.indexOf("sysmetal"));
        path = path.substring(0,dir.indexOf("sysmetal")+8) +barra+"src"+barra + path.substring(path.indexOf("!") + 2)+barra;
        return path;
    }
}