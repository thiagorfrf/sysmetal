/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.reports.materiais;

import br.org.galvanisa.business.entity.materiais.MateriaisData;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import utilities.ConnectionFactory;
import utilities.ReportUtils;

/**
 *
 * @author Thiago Rodrigues
 */
public class ChamarRelatorioMateriais {

    private String path = null;
    private String url = getClass().getResource(getClass().getSimpleName() + ".class").getPath();
    public Boolean ofPreenchida = null;

    public void chamarRelatorioSaidasClass(int codCliente, List<MateriaisData> materiaisList, String cliente) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("Materiais.jasper");
        Map parametros = new HashMap();

        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("CLIENTE", cliente);
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        // criando os dados que serão passados ao datasource
        //List<SaidaBusinessEntity> dados = new ArrayList();
        //dados.add((SaidaBusinessEntity) saidaBusinessEntity);
        // criando o datasource com os dados criados
        JRDataSource ds = new JRBeanCollectionDataSource(materiaisList);
        ReportUtils.openReport("Materiais", inputStream, parametros, ds);
    }

   private String getPath() {
        File dir = new File(url).getParentFile();
        path = dir.getParent();
        if (dir.getPath().contains(".jar")) {
            while (dir.getPath().contains(".jar")) {
                path = dir.getAbsolutePath();
                return findJarParentPath(path);
            }
        }
        return path;
    }

    private String findJarParentPath(String dir) {
        String barra = path.substring(dir.indexOf("sysmetal")-1,dir.indexOf("sysmetal"));
        path = path.substring(0,dir.indexOf("sysmetal")+8) +barra+"src"+barra + path.substring(path.indexOf("!") + 2)+barra;
        return path;
    }
}