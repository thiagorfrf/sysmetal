/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.reports.transfer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import utilities.ConnectionFactory;
import utilities.ReportUtils;

/**
 *
 * @author Thiago Rodrigues
 *
 */
public class ChamarRelatoriosGerais {

    private String path = null;
    private String url = getClass().getResource(getClass().getSimpleName() + ".class").getPath();

    public void chamarRelatorioTotalTransferencia() throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalTransferencias.jasper");
        System.out.println("chamarRelatorioTotalTransferencia()");
        Map parametros = new HashMap();
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        parametros.put("SUBREPORT_DIR", getPath());
        ReportUtils.openReport("Todas as Transferências", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioProdutosMovimentacao(String codProduto, String codCliente, int codControleCliente) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("ProdutosMovimentacao.jasper");
        System.out.println("chamarRelatorioTotalTransferencia()");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("CODCLIENTE", codCliente);
        parametros.put("CODCONTROLECLIENTE", codControleCliente);
        parametros.put("CODPRODUTO", codProduto);
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        ReportUtils.openReport("Movimentações por Produto", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioEntradasClienteData(String codCliente, String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradasData.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Entradas", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioTransferenciaData(String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalTransferenciasData.jasper");
        System.out.println("chamarRelatorioTransferenciaData()");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Relatório de Transferências por período", inputStream, parametros, ConnectionFactory.getConexao());
    }

  
       private String getPath() {
        File dir = new File(url).getParentFile();
        path = dir.getParent();
        if (dir.getPath().contains(".jar")) {
            while (dir.getPath().contains(".jar")) {
                path = dir.getAbsolutePath();
                return findJarParentPath(path);
            }
        }
        return path;
    }

    private String findJarParentPath(String dir) {
        String barra = path.substring(dir.indexOf("sysmetal")-1,dir.indexOf("sysmetal"));
        path = path.substring(0,dir.indexOf("sysmetal")+8) +barra+"src"+barra + path.substring(path.indexOf("!") + 2)+barra;
        return path;
    }
}