/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.reports.entradas;

import br.org.galvanisa.business.entity.entrada.EntradaBusinessEntity;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import utilities.ConnectionFactory;
import utilities.ConnectionFactorySiga;
import utilities.ReportUtils;

/**
 *
 * @author Thiago Rodrigues
 *
 */
public class ChamarRelatorioClienteEntradas {

    private String path = null;
    private String url = getClass().getResource(getClass().getSimpleName() + ".class").getPath();

    public void chamarRelatorioEntradasCliente(String codCliente) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradasCliente.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        //parametros.put("SUBREPORT_DIR", "C:\\Users\\user\\Documents\\sysmetal\\src\\br\\org\\galvanisa\\business\\reports\\entradas\\");
        parametros.put("CLIENTE", codCliente);
        ReportUtils.openReport("Entradas", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioEntradas() throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradas.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        ReportUtils.openReport("Entradas", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioEntradasClienteData(String codCliente, String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("EntradasData.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Entradas", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioTodasEntradasData(String dataInicial, String dataFinal) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradasData.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("DATA_INICIAL", dataInicial);
        parametros.put("DATA_FINAL", dataFinal);
        ReportUtils.openReport("Todas as Entradas por Data", inputStream, parametros, ConnectionFactory.getConexao());
    }

    public void chamarRelatorioProdutosGalvanisa() throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalProdutosGalvanisa.jasper");
        Map parametros = new HashMap();
        ReportUtils.openReport("Produtos Galvanisa", inputStream, parametros, ConnectionFactorySiga.getConexao());
    }

    public void chamarRelatorioEntradasClass(int codCliente, List<EntradaBusinessEntity> entradaBusinessEntity, String filtrosSelecionados, String descricao) throws JRException, SQLException, IOException {
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradasTela.jasper");
        Map parametros = new HashMap();
        parametros.put("SUBREPORT_DIR", getPath());
        parametros.put("COD_CLIENTE", codCliente);
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        parametros.put("CLIENTE", descricao);
        parametros.put("TIPO", filtrosSelecionados);

        JRDataSource ds = new JRBeanCollectionDataSource(entradaBusinessEntity);
        ReportUtils.openReport("Entradas", inputStream, parametros, ds);
    }

    public void chamarRelatorioTotalTransferenciaDestino() throws SQLException, JRException {
        System.out.println("chamarRelatoriorTotalTransferenciaDestino()");
        InputStream inputStream = getClass().getResourceAsStream("TotalEntradasTransferencias.jasper");
        Map parametros = new HashMap();
        parametros.put("CONNECTION", ConnectionFactory.getConexao());
        parametros.put("SUBREPORT_DIR", getPath());
        ReportUtils.openReport("Transferências Destino de Materiais", inputStream, parametros, ConnectionFactory.getConexao());
    }

    private String getPath() {
        File dir = new File(url).getParentFile();
        path = dir.getParent();
        if (dir.getPath().contains(".jar")) {
            while (dir.getPath().contains(".jar")) {
                path = dir.getAbsolutePath();
                //path = path + barra;
                return findJarParentPath(path);
            }
        }
        return path;
    }

    private String findJarParentPath(String dir) {
        String barra = path.substring(dir.indexOf("sysmetal")-1,dir.indexOf("sysmetal"));
        path = path.substring(0,dir.indexOf("sysmetal")+8) +barra+"src"+barra + path.substring(path.indexOf("!") + 2)+barra;
        return path;
    }
}