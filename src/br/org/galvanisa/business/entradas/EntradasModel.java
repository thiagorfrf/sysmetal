/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.entradas;

import br.org.galvanisa.business.cadastre.entrada.EntradaProdutos;
import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.EntradaItens;
import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.entradaitens.EntradaItensModel;
import br.org.galvanisa.business.estoque.EstoqueModel;
import br.org.galvanisa.presentation.materiais.ClienteMateriaisService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class EntradasModel {

    boolean sobraClicked;
    boolean transferenciaClicked;
    boolean estornoClicked;
    boolean normalClicked;
    private ObservableList<Entrada> entradas;
    private ClienteMateriaisService clienteMateriaisService = new ClienteMateriaisService();
    private ObservableList<EntradaItens> EntradaItensList;
    private final String QUERY_TODAS_ENTRADAS_POR_CLIENTE = "from Entrada where cod_cliente = :codigoCliente";
    private final String QUERY_TODAS_ENTRADAS_POR_CODIGO = "from Entrada where cod_entrada = :codigoEntrada";
    private final String QUERY_TODAS_ENTRADAS_POR_NUMNF = "from Entrada where num_nf = :numNF";
    private final String QUERY_TODAS_ENTRADAS = "from Entrada";
    private EntradaItensModel entradItensModel = new EntradaItensModel();
    private EstoqueModel estoqueModel = new EstoqueModel();

    public boolean isSobraClicked() {
        return sobraClicked;
    }

    public void setSobraClicked(boolean sobraClicked) {
        this.sobraClicked = sobraClicked;
    }

    public boolean isTransferenciaClicked() {
        return transferenciaClicked;
    }

    public void setTransferenciaClicked(boolean transferenciaClicked) {
        this.transferenciaClicked = transferenciaClicked;
    }

    public boolean isEstornoClicked() {
        return estornoClicked;
    }

    public void setEstornoClicked(boolean estornoClicked) {
        this.estornoClicked = estornoClicked;
    }

    public boolean isNormalClicked() {
        return normalClicked;
    }

    public void setNormalClicked(boolean normalClicked) {
        this.normalClicked = normalClicked;
    }

    public ObservableList<Entrada> loadClienteEntradasFilter(String codCliente) {
        List<Entrada> list = new ArrayList();
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        String hql = QUERY_TODAS_ENTRADAS_POR_CLIENTE;
        Query q = session.createQuery(hql);
        q.setParameter("codigoCliente", codCliente);
        list = q.list();
        session.getTransaction().commit();
        entradas = FXCollections.observableArrayList(list);
        session.close();
        return entradas;
    }

    public ObservableList<Entrada> loadClienteEntradas(String codCliente) {

        List<Entrada> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODAS_ENTRADAS_POR_CLIENTE;
            Query q = session.createQuery(hql);
            q.setParameter("codigoCliente", codCliente);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        entradas = FXCollections.observableArrayList(list);
        return entradas;
    }

    public ObservableList<Entrada> loadEntradasPorCodigo(int codigoEntrada) {

        List<Entrada> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODAS_ENTRADAS_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigoEntrada", codigoEntrada);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        entradas = FXCollections.observableArrayList(list);
        return entradas;
    }

    public List<Entrada> loadEntradasPorNF(String numNF) {

        List<Entrada> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODAS_ENTRADAS_POR_NUMNF;
            Query q = session.createQuery(hql);
            q.setParameter("numNF", numNF);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public ObservableList<Entrada> loadAllEntradas() {

        List<Entrada> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_TODAS_ENTRADAS;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        entradas = FXCollections.observableArrayList(list);
        return entradas;
    }

    /*
     **Salva entradas que se origininam de uma entrada normal
     */
    public void saveNewEntrada(Entrada entrada, ObservableList<EntradaProdutos> dataTableViewInserir) {
        this.saveEntrada(entrada);
        int newCodEntrada = entrada.getCodEntrada();
        int item = 0;

        for (EntradaProdutos i : dataTableViewInserir) {
            item++;
            EntradaItens entradaItens = new EntradaItens();
            entradaItens.setCodEntrada(newCodEntrada);
            entradaItens.setNumItem(item);
            entradaItens.setCodProduto(i.getCodProduto());
            entradaItens.setQuantKg(i.getQuantKg());
            entradaItens.setQuantUn(i.getQuantUn());
            entradaItens.setComprimento(i.getComprimento());
            entradaItens.setSobra(i.getSobra());
            i.setCodEntrada(newCodEntrada);
            // get cliente codigo e atualiza estoque, inserindo o produto se ele nao tiver, ou 
            //alterando sua quantidade em kg e unidade.

            this.saveItensEntrada(entradaItens);
            this.updateEstoque(entrada.getCodCliente(), entradaItens);
        }
    }
    /*
     **Salva uma entrada que se origina de uma entrada de Transferencia 
     */

    public void saveNewEntrada(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {

        Entrada entrada1 = this.saidaToEntrada(codCliente, saida);
        this.saveEntrada(entrada1);
        int newCodEntrada = entrada1.getCodEntrada();
        int item = 0;

        for (MateriaisData u : materiaisItens) {
            EntradaItens swap = new EntradaItens();
            swap.setCodEntrada(newCodEntrada);
            swap.setNumItem(++item);
            swap.setCodProduto(u.getCodProduto());
            swap.setQuantUn(u.getQuantUn());
            swap.setQuantKg(u.getQuantKg());
            swap.setComprimento(u.getComprimento());
            swap.setSobra(u.getSobra()); // tratar isso depois
            this.saveItensEntrada(swap);
            this.updateEstoque(entrada1.getCodCliente(), swap);
        }
    }

    private Entrada saidaToEntrada(String codCliente, Saida saida) {
        Entrada entrada = new Entrada();
        entrada.setCodCliente(saida.getCodClienteTransf());
        entrada.setNumNf(String.valueOf(saida.getCodSaida()));
        entrada.setNumCertificado("");
        entrada.setNumFornecedor(codCliente);
        entrada.setTipoOperacao('T');
        entrada.setTotalKg(saida.getTotalKg());
        entrada.setDtEntrada(saida.getDtSaida());
        return entrada;
    }

    private void saveEntrada(Entrada entrada) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(entrada);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void saveItensEntrada(EntradaItens entradaItens) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(entradaItens);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void updateEstoque(String codCliente, EntradaItens entradaItem) {

        ObservableList<Estoque> clienteEstoqueProduto = FXCollections
                .observableArrayList(clienteMateriaisService.loadEstoque(codCliente, entradaItem.getCodProduto()));
        if (!clienteEstoqueProduto.isEmpty() && entradaItem.getSobra() == false && clienteEstoqueProduto.get(0).getSobra() == false) {
            clienteEstoqueProduto.get(0).setQuantKg(
                    clienteEstoqueProduto.get(0).getQuantKg().add(entradaItem.getQuantKg()));
            clienteEstoqueProduto.get(0).setQuantUn(clienteEstoqueProduto.get(0).getQuantUn().add(entradaItem.getQuantUn()));
            clienteEstoqueProduto.get(0).setQuantLiberada(clienteEstoqueProduto.get(0).getQuantLiberada().add(entradaItem.getQuantUn()));

            clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(0));
            entradaItem.setCodEstoque(clienteEstoqueProduto.get(0).getCodigo());
            entradItensModel.alterEntradaItem(entradaItem);

        } else if (entradaItem.getSobra()) {
            Estoque estoque = new Estoque(
                    codCliente, entradaItem.getCodProduto(),
                    entradaItem.getQuantKg(), BigDecimal.ZERO, entradaItem.getQuantUn(),
                    BigDecimal.ZERO, entradaItem.getQuantUn(), entradaItem.getComprimento(),
                    entradaItem.getSobra());
            cadastroEstoqueCadastroEntradaItens(estoque, entradaItem);
        } else if (clienteEstoqueProduto.isEmpty() && entradaItem.getSobra() == false) {
            Estoque estoque = new Estoque(codCliente, entradaItem.getCodProduto(),
                    entradaItem.getQuantKg(), BigDecimal.ZERO, entradaItem.getQuantUn(),
                    BigDecimal.ZERO, entradaItem.getQuantUn(), entradaItem.getComprimento(),
                    entradaItem.getSobra());
            cadastroEstoqueCadastroEntradaItens(estoque, entradaItem);

        } else if (!clienteEstoqueProduto.isEmpty() && entradaItem.getSobra() == false) {
            int sobras = 0;
            for (int i = 0; i < clienteEstoqueProduto.size(); i++) {
                if (clienteEstoqueProduto.get(i).getSobra() == true) {
                    sobras++;
                }
            }
            if (sobras == clienteEstoqueProduto.size()) {
                Estoque estoque = new Estoque(codCliente, entradaItem.getCodProduto(),
                        entradaItem.getQuantKg(), BigDecimal.ZERO, entradaItem.getQuantUn(),
                        BigDecimal.ZERO, entradaItem.getQuantUn(), entradaItem.getComprimento(),
                        entradaItem.getSobra());
                cadastroEstoqueCadastroEntradaItens(estoque, entradaItem);
            } else {
                for (int i = 0; i < clienteEstoqueProduto.size(); i++) {
                    if (!clienteEstoqueProduto.get(i).getSobra()) {
                        clienteEstoqueProduto.get(i).setQuantKg(clienteEstoqueProduto.get(i).getQuantKg().add(entradaItem.getQuantKg()));
                        clienteEstoqueProduto.get(i).setQuantUn(clienteEstoqueProduto.get(i).getQuantUn().add(entradaItem.getQuantUn()));
                        clienteEstoqueProduto.get(i).setQuantLiberada(clienteEstoqueProduto.get(i).getQuantLiberada().add(entradaItem.getQuantUn()));
                        entradaItem.setCodEstoque(clienteEstoqueProduto.get(i).getCodigo());
                        clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(i));
                        entradItensModel.alterEntradaItem(entradaItem);
                        break;
                    }
                }
            }
        }

    }

    public void deleteEstoque(Entrada get) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(get);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
//Atualiza EntradaItens adicionando o valor do campo codEstoque;

    private void cadastroEstoqueCadastroEntradaItens(Estoque estoque, EntradaItens entradaItem) {
        clienteMateriaisService.insertInToEstoque(estoque);
        ObservableList<Estoque> todasEntradaItens = estoqueModel.loadAllEstoque();
        entradaItem.setCodEstoque(todasEntradaItens.get((estoqueModel.loadAllEstoque().size() - 1)).getCodigo());
        entradItensModel.alterEntradaItem(entradaItem);

    }
}
