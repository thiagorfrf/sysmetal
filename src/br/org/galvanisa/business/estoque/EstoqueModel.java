/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.estoque;

import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Produtos;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class EstoqueModel {

    private final String QUERY_BUSCAR_ESTOQUE = "from Estoque where quant_un > 0";
        private final String QUERY_BUSCAR_TODOS_ESTOQUE = "from Estoque";
    private final String QUERY_BUSCAR_ESTOQUE_CLIENTE = "from Estoque where cod_cliente = :codigoCliente";
        private final String QUERY_BUSCAR_ESTOQUE_POR_CODIGO = "from Estoque where codigo = :codigo";
    private ObservableList<Estoque> estoque = FXCollections.observableArrayList();
    private ObservableList<Produtos> produtos;

    public ObservableList<Estoque> loadAllEstoque() {
        List<Estoque> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        estoque = FXCollections.observableArrayList(list);
        return estoque;
    }
    
       public ObservableList<Estoque> AllEstoque() {
        List<Estoque> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_TODOS_ESTOQUE;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        estoque = FXCollections.observableArrayList(list);
        return estoque;
    }
    

    public ObservableList<Estoque> loadEstoquePorCliente(String codCliente) {
        ObservableList<Estoque> objEstoque2 = FXCollections.observableArrayList();
        List<Estoque> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE_CLIENTE;
            Query q = session.createQuery(hql);
            q.setParameter("codigoCliente", codCliente);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        objEstoque2 = FXCollections.observableArrayList(list);
        return objEstoque2;
    }
    
        public ObservableList<Estoque> loadEstoquePorCodigo(int codigo) {
        ObservableList<Estoque> objEstoque2 = FXCollections.observableArrayList();
        List<Estoque> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigo", codigo);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        objEstoque2 = FXCollections.observableArrayList(list);
        return objEstoque2;
    }

}
