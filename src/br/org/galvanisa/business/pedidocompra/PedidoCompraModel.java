/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.pedidocompra;

import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.pedidocompra.ProdutosPedidoCompra;
import br.org.galvanisa.business.estoquegalvanisa.EstoqueGalvanisaModel;
import br.org.galvanisa.presentation.produtos.LoadProdutosTerceiroService;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class PedidoCompraModel {

    private ObservableList<ProdutosPedidoCompra> produtosPedidoCompra = FXCollections.observableArrayList();
    private LoadProdutosTerceiroService loadProdutosService = new LoadProdutosTerceiroService();
    private ObservableList<Produtos> produtos = loadProdutosService.loadProdutos();
    private Connection connection = null;
    
    public ObservableList<ProdutosPedidoCompra> getPedidosCompra() {

        try {
            connectSqlServer();
            loadPedidoCompras(connection);
        } catch (SQLException ex) {
            Logger.getLogger(EstoqueGalvanisaModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        treatProdutosPedidoCompras();
        return produtosPedidoCompra;
    }

    private void connectSqlServer() throws SQLException {
        System.out.println("----SQLServer connect test");
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your sqlServer jdpc driver ?");
            e.printStackTrace();
            return;
        }
        System.out.println("SQLServer JDBC Driver Registeerd");
        try {
            connection = DriverManager.getConnection(
                    "jdbc:sqlserver://192.168.0.13:1433;databaseName=dadosadv", "sa", "Sa1");
        } catch (SQLException e) {
            System.out.println("connection fail!");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    private void loadPedidoCompras(Connection connection) throws SQLException {

        Statement stmt = null;
        stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery("select C7_NUM, C7_DESCRI, C7_QTDSOL, C7_QUANT, C7_DATPRF,C7_PRODUTO, C7_QTSEGUM from SC7010 where C7_FILIAL = '03'"
                + " and D_E_L_E_T_ <> '*' and C7_EMISSAO > '20140501' and C7_EMITIDO = 'S' and C7_RESIDUO <> 'S' and C7_ENCER <> 'E' and\n"
                + "C7_PRODUTO in (select B1_COD from SB1010 where B1_GRUPO = '02' and D_E_L_E_T_ <> '*' and B1_FILIAL = '03');");

        while (rs.next()) {
            ProdutosPedidoCompra swap = new ProdutosPedidoCompra();
            swap.setCodigo(rs.getString("C7_NUM"));
            swap.setDescricao(rs.getString("C7_DESCRI"));
            swap.setCodProduto(rs.getString("C7_PRODUTO"));
            swap.setQtdSolicitada(rs.getBigDecimal("C7_QTDSOL"));
            swap.setFator(BigDecimal.ZERO);
            swap.setQtdPedidoKg(rs.getBigDecimal("C7_QUANT"));
            swap.setQtdPedidoUn(rs.getBigDecimal("C7_QTSEGUM"));
            swap.setDataEntregaEstimada(rs.getString("C7_DATPRF"));
            produtosPedidoCompra.add(swap);
        }
        rs.close();
        stmt.close();
        connection.close();
    }

    private void treatProdutosPedidoCompras() {
        for (int i = 0; i < produtosPedidoCompra.size(); i++) {
            for (int j = 0; j < produtos.size(); j++) {

                if (produtosPedidoCompra.get(i).getCodigo().equals(produtos.get(j).getCodigo())) {
                    produtosPedidoCompra.get(i).setFator(produtos.get(j).getFconversao());
                }
            }
        }
    }
}
