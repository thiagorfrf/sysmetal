/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.business.saidas;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.SaidaItens;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.estoque.EstoqueModel;
import br.org.galvanisa.business.materiais.ClienteMateriaisModel;
import br.org.galvanisa.presentation.materiais.ClienteMateriaisService;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;
import utilities.NumberTextField;

/**
 *
 * @author thiago
 */
public class SaidasModel {

    private final String QUERY_SAIDAS_POR_CLIENTE = "from Saida where cod_controle_cliente = :codControleCliente";
    private final String QUERY_SAIDA_ITENS_POR_CODIGO = "from SaidaItens where cod_saida = :codigoSaida";
    private final String QUERY_SAIDAS_POR_CODIGO = "from Saida where cod_saida = :codigoSaida";
    private final String QUERY_CONTROLE_CLIENTE_POR_CODIGO_CONTROLE = "from ControleCliente where cod_controle = :codigoControle";
    private final String QUERY_CONTROLEOF_POR_CODIGO_CONTROLE_OF = "from ControleOf where cod_controle_of = :codControleOf";
    private ObservableList<Saida> saidas;
    private ObservableList<SaidaItens> saidaItensList = FXCollections.observableArrayList();
    private ClienteMateriaisService clienteMateriaisService = new ClienteMateriaisService();
    private ClienteMateriaisModel clienteMateriaisModel = new ClienteMateriaisModel();
    EstoqueModel estoqueModel = new EstoqueModel();
    NumberTextField nt = new NumberTextField();

    public ObservableList<Saida> loadSaidas(int codControle) {
        List<Saida> list = new ArrayList();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_SAIDAS_POR_CLIENTE;
            Query q = session.createQuery(hql);
            q.setParameter("codControleCliente", codControle);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        saidas = FXCollections.observableArrayList(list);
        return saidas;
    }

    public void saveNewSaida(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {
        saida.setTotalKg(getTotalKgSaida(materiaisItens));

        saveSaida(saida);
        transformMateriaisDataToSaidaItens(saida.getCodSaida(), materiaisItens);
        //pasa codigo da saida para SaidaItens 
        for (SaidaItens i : saidaItensList) {
            saveSaidaItens(i);
            if ((i.getCodEstoque() == 0)) {
                updateEstoque(codCliente, i);
            } else {
                updateEstoque(i);
            }
        }
        saidaItensList.clear();
    }

    public void saveNewEmpenho(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {
        saidaItensList.removeAll();
        saida.setTotalKg(getTotalKgSaida(materiaisItens));
        saveSaida(saida);
        transformMateriaisDataToSaidaItens(saida.getCodSaida(), materiaisItens);

        for (SaidaItens i : saidaItensList) {
            saveSaidaItens(i);
            updateEstoqueEmpenho(codCliente, i);
        }
    }

    private BigDecimal getTotalKgSaida(ObservableList<MateriaisData> materiaisItens) {
        BigDecimal somatorio = BigDecimal.ZERO;
        for (MateriaisData u : materiaisItens) {
            somatorio = somatorio.add(u.getQuantKg());
        }
        return somatorio;
    }

    private void saveSaida(Saida saida) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(saida);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private ObservableList<SaidaItens> transformMateriaisDataToSaidaItens(
            int codSaida, ObservableList<MateriaisData> materiaisItens) {
        int numItem = 0;
        for (MateriaisData u : materiaisItens) {
            SaidaItens swap = new SaidaItens();
            swap.setCodSaida(codSaida);
            swap.setCodEstoque(u.getCodEstoque());
            swap.setNumItem(++numItem);
            swap.setCodProduto(u.getCodProduto());
            swap.setQuantUn(u.getQuantUn());
            swap.setQuantKg(u.getQuantKg());
            swap.setComprimento(u.getComprimento());
            saidaItensList.add(swap);
        }
        return saidaItensList;
    }

    private void saveSaidaItens(SaidaItens i) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(i);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void updateEstoque(SaidaItens i) {
        ObservableList<Estoque> clienteEstoqueProduto = FXCollections.
                observableArrayList(clienteMateriaisService.
                        loadEstoquePorCodigo(i.getCodEstoque()));
        clienteEstoqueProduto.get(0).setQuantKg(clienteEstoqueProduto.get(0).getQuantKg().subtract(i.getQuantKg()));
        clienteEstoqueProduto.get(0).setQuantUn(clienteEstoqueProduto.get(0).getQuantUn().subtract(i.getQuantUn()));
        clienteEstoqueProduto.get(0).setQuantLiberada(clienteEstoqueProduto.get(0).getQuantLiberada().subtract(i.getQuantUn()));
        clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(0));
    }

    private void updateEstoque(String codCliente, SaidaItens i) {
        ObservableList<Estoque> clienteEstoqueProduto = FXCollections.
                observableArrayList(clienteMateriaisService.loadEstoque(codCliente, i.getCodProduto()));
        i.setCodEstoque(clienteEstoqueProduto.get(0).getCodigo());
        updateSaidaItensSetCodEstoque(i);
        clienteEstoqueProduto.get(0).setQuantKg(clienteEstoqueProduto.get(0).getQuantKg().subtract(i.getQuantKg()));
        clienteEstoqueProduto.get(0).setQuantUn(clienteEstoqueProduto.get(0).getQuantUn().subtract(i.getQuantUn()));
        clienteEstoqueProduto.get(0).setQuantLiberada(clienteEstoqueProduto.get(0).getQuantLiberada().subtract(i.getQuantUn()));
        clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(0));
    }

    private void updateSaidaItensSetCodEstoque(SaidaItens s) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(s);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void updateEstoqueEmpenho(String codCliente, SaidaItens i) {
        ObservableList<Estoque> clienteEstoqueProduto = FXCollections.
                observableArrayList(clienteMateriaisService.
                        loadEstoquePorCodigo(i.getCodEstoque()));
        // se nao for zero!
        clienteEstoqueProduto.get(0).
                setQuantEmpenho(
                        clienteEstoqueProduto.
                        get(0).
                        getQuantEmpenho().
                        add(i.getQuantUn()));
        clienteEstoqueProduto.get(0).
                setQuantLiberada(
                        clienteEstoqueProduto.
                        get(0).getQuantUn().
                        subtract(clienteEstoqueProduto.
                                get(0).getQuantEmpenho()));
        clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(0));
    }

    public ObservableList<MateriaisData> getItensFromSaida(Saida saida) {
        ClienteMateriaisService materiais = new ClienteMateriaisService();
        ObservableList<MateriaisData> swapProdutos = FXCollections.observableArrayList();
        ObservableList<MateriaisData> saidaProdutos = FXCollections.observableArrayList();
        ObservableList<SaidaItens> todosItensSaida = FXCollections.observableArrayList(getSaidaItens(saida.getCodSaida()));
        String codCliente = getClienteCodigo(saida);
        List<MateriaisData> estoqueList = new ArrayList<MateriaisData>();
        saidaProdutos = materiais.loadClienteMateriais(codCliente);
        for (int k = 0; k < todosItensSaida.size(); k++) {
            estoqueList.addAll(getEstoqueSaida(saidaProdutos, todosItensSaida.get(k).getCodEstoque()));
        }
        for (int j = 0; j < estoqueList.size(); j++) {
            if (estoqueList.get(j).getCodProduto().equals(todosItensSaida.get(j).getCodProduto())) {
                swapProdutos.add(estoqueList.get(j));
                //verificar aqui
                swapProdutos.get(j).setQuantLiberada(todosItensSaida.get(j).getQuantUn());
                swapProdutos.get(j).setQuantUn(todosItensSaida.get(j).getQuantUn());
                swapProdutos.get(j).setQuantKg(nt.casasDecimais(2, swapProdutos.get(j).getQuantUn().multiply(swapProdutos.get(j).getFconversao())));
                if (estoqueList.get(j).getSobra()) {
                    swapProdutos.get(j).setQuantKg(nt.casasDecimais(2, swapProdutos.get(j).getComprimento()
                            .divide(nt.tamanhoReal(swapProdutos.get(j).getDescProduto()), BigDecimal.ROUND_UP)
                            .multiply(swapProdutos.get(j).getFconversao())));

                }
            }
        }
        return swapProdutos;
    }

    List<MateriaisData> getEstoqueSaida(List<MateriaisData> materiais, int codEstoque) {
        return materiais.stream().filter(p -> p.getCodEstoque() == codEstoque)
                .collect(Collectors.<MateriaisData>toList());
    }

    public List getSaidaItens(int codSaida) {
        List<SaidaItens> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_SAIDA_ITENS_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigoSaida", codSaida);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public Saida getSaida(int codSaida) {
        Saida objSaida = new Saida();
        List<Saida> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_SAIDAS_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigoSaida", codSaida);
            list = q.list();
            objSaida = list.get(0);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return objSaida;
    }

    private String getClienteCodigo(Saida saida) {
        List<ControleCliente> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CONTROLE_CLIENTE_POR_CODIGO_CONTROLE;
            Query q = session.createQuery(hql);
            q.setParameter("codigoControle", saida.getCodControleCliente());
            list = q.list();
            ControleCliente x = new ControleCliente();
            x = list.get(0);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list.get(0).getCodCliente();
    }

    private ObservableList<MateriaisData> treatItens(ObservableList<MateriaisData> saidaProdutos) {
        ObservableList<MateriaisData> itensSaida = FXCollections.observableArrayList();
        for (MateriaisData itemProduto : saidaProdutos) {
            if (itemProduto.getEscolha()) {
                itensSaida.add(itemProduto);
            }
        }
        return itensSaida;
    }

    public void getObjetcsFromSaida(List<Object> objetosSaida) {
        Saida saida = new Saida();
        saida = (Saida) objetosSaida.get(0);
        objetosSaida.add(getSaidaItens(saida.getCodSaida()));
    }

    public List<String> transformSaidaInToCadastroView(Saida saida) {
        List<String> list = new ArrayList<>();
        //add to list
        //return list
        //get codCliente and OP where codControleCliente = saida.getCodControleCliente;
        ControleCliente controleCliente = new ControleCliente();
        controleCliente = getInfoFromCliente(saida.getCodControleCliente());
        //add list
        list.add(controleCliente.getCodCliente());
        list.add(controleCliente.getNumOp());
        //get numof num pc where codControleOf = saidaGetCodControleOf
        ControleOf controleOf = new ControleOf();
        //validar se retorna vazio  !!!
        controleOf = getInfoFromControleOf(saida.getCodControleOf());
        //add list
        list.add(controleOf.getNumOf());
        list.add(controleOf.getNumPc());
        return list;
    }

    private ControleCliente getInfoFromCliente(Integer codControleCliente) {
        List<ControleCliente> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CONTROLE_CLIENTE_POR_CODIGO_CONTROLE;
            Query q = session.createQuery(hql);
            q.setParameter("codigoControle", codControleCliente);
            list = q.list();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list.get(0);
    }

    private ControleOf getInfoFromControleOf(Integer codControleOf) {
        List<ControleOf> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_CONTROLEOF_POR_CODIGO_CONTROLE_OF;
            Query q = session.createQuery(hql);
            q.setParameter("codControleOf", codControleOf);
            list = q.list();
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list.get(0);
    }

    public void updateEmpenhoToSaida(String codCliente, Saida saida,
            ObservableList<MateriaisData> materiaisItens, List<Object> objetosSaida) {
        //alterar objetos saida
        Saida saidaClean = new Saida();
        saidaClean = (Saida) objetosSaida.get(0);
        // o que alterar tipoOperacao e kgTotal
        saidaClean.setTipoOperacao('N');
        saidaClean.setTotalKg(saida.getTotalKg());
        saidaClean.setDtSaida(saida.getDtSaida());
        updateSaidaClean(saidaClean);
        //alterar objetosSaida Itens Saida
        //ObservableList<SaidaItens> saidaItensClean;
        ObservableList<SaidaItens> saidaItensClean
                = FXCollections.observableArrayList((List<SaidaItens>) objetosSaida.get(1));
        for (int i = 0; i < materiaisItens.size(); i++) {
            updateEstoqueFromEmpenho(saidaItensClean.get(i)); // desfazer o que o empenho fez !!!
            saidaItensClean.get(i).setQuantUn(materiaisItens.get(i).getQuantUn());
            saidaItensClean.get(i).setQuantKg(materiaisItens.get(i).getQuantKg());
            updateSaidaItensClean(saidaItensClean.get(i));
            updateEstoque(saidaItensClean.get(i)); // modelo antigo pra ver se funciona sem mais problemas  !!
        }
    }

    private void updateSaidaClean(Saida saidaClean) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(saidaClean);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void updateSaidaItensClean(SaidaItens get) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(get);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    private void updateEstoqueFromEmpenho(SaidaItens get) {
        ObservableList<Estoque> clienteEstoqueProduto = FXCollections
                .observableArrayList(clienteMateriaisModel.loadEstoquePorCodigo(get.getCodEstoque()));
        // se nao for zero!
        clienteEstoqueProduto.
                get(0).
                setQuantEmpenho(clienteEstoqueProduto.
                        get(0).
                        getQuantEmpenho().
                        subtract(get.
                                getQuantUn()));
        clienteEstoqueProduto.
                get(0).
                setQuantLiberada(clienteEstoqueProduto.
                        get(0).
                        getQuantUn().
                        subtract(clienteEstoqueProduto.
                                get(0).
                                getQuantEmpenho()));
        clienteMateriaisService.changeEstoque(clienteEstoqueProduto.get(0));
    }

    public void deleteSaida(Saida objSaida) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(objSaida);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
}
