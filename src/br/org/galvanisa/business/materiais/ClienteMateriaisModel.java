package br.org.galvanisa.business.materiais;

import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.SaidaItens;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;
import utilities.NumberTextField;

/**
 *
 * @author thiago
 */
public class ClienteMateriaisModel {

    private final String QUERY_BUSCAR_ESTOQUE_CLIENTE_E_CODIGO = "from Estoque where cod_cliente = :codigoCliente and quant_un > 0";
    private final String QUERY_BUSCAR_ESTOQUE_POR_CLIENTE_PRODUTO = "from Estoque where cod_cliente = :cod1 and cod_produto = :prod1";
    private final String QUERY_BUSCAR_ESTOQUE_POR_CODIGO = "from Estoque where codigo = :codigo";
    private final String QUERY_BUSCAR_TODOS_PRODUTOS = "from Produtos";
    ObservableList<Estoque> estoque;
    ObservableList<Produtos> produtos;
    ObservableList<MateriaisData> clienteMateriais;
    NumberTextField nt = new NumberTextField();

    public ObservableList<MateriaisData> loadClienteMateriais(String codCliente) {
        estoque = FXCollections.observableArrayList(loadEstoque(codCliente));
        produtos = FXCollections.observableArrayList(loadProdutos());
        clienteMateriais = FXCollections.observableArrayList(loadMateriaisData());
        return clienteMateriais;
    }

    public List loadEstoquePorCodigo(int codigo) {
        List<Estoque> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigo", codigo);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public List loadEstoque(String codCliente) {
        List<Estoque> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE_CLIENTE_E_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codigoCliente", codCliente);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public List loadEstoque(String codCliente, String codProduto) {
        List list = null;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_ESTOQUE_POR_CLIENTE_PRODUTO;
            Query q = session.createQuery(hql);
            q.setParameter("cod1", codCliente);
            q.setParameter("prod1", codProduto);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
        }
        return list;
    }

    private List<Produtos> loadProdutos() {
        List<Produtos> list = new ArrayList<>();
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_BUSCAR_TODOS_PRODUTOS;
            Query q = session.createQuery(hql);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }
    /*
     **Fazer Melhoria aqui
     */

    private ObservableList<MateriaisData> loadMateriaisData() {
        ObservableList<MateriaisData> clienteMateriaisSwap = FXCollections.observableArrayList();

        for (int i = 0; i < estoque.size(); i++) {
            MateriaisData swap = new MateriaisData();
            swap.setCodigo(estoque.get(i).getCodigo());
            swap.setCodCliente(estoque.get(i).getCodCliente());
            swap.setCodProduto(estoque.get(i).getCodProduto());
            for (int z = 0; z < produtos.size(); z++) {
                if (estoque.get(i).getCodProduto().equals(produtos.get(z).getCodigo())) {
                    swap.setDescProduto(produtos.get(z).getDescricao());
                    swap.setFconversao(produtos.get(z).getFconversao());
                } else {
                    continue;
                }
            }
            swap.setCodEstoque(estoque.get(i).getCodigo());
            swap.setQuantKg(estoque.get(i).getQuantKg());
            swap.setQuantUn(estoque.get(i).getQuantUn());
            swap.setQuantEmpenho(estoque.get(i).getQuantEmpenho());
            swap.setQuantLiberada(estoque.get(i).getQuantLiberada());
            swap.setComprimento(estoque.get(i).getComprimento());
            swap.setSobra(estoque.get(i).getSobra());
            swap.setEscolha(false);
            clienteMateriaisSwap.add(swap);
        }
        return clienteMateriaisSwap;
    }

    public void changeEstoque(Estoque get) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(get);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public void insertInToEstoque(Estoque estoque) {
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(estoque);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }
    /*
     **Metodo treantEstoqueNovaSaida
     **Pega  a list e ve quais estão marcadas e envia para Nova Saida
     */

    public ObservableList<SaidaItens> treatEstoqueNovaSaida(ObservableList<MateriaisData> materiaisSelecionados) {
        ObservableList<SaidaItens> itensSaida = FXCollections.observableArrayList();
        int numItem = 0;
        for (MateriaisData itemProduto : materiaisSelecionados) {
            if (itemProduto.getEscolha()) {
                SaidaItens itemSaida = new SaidaItens();
                itemSaida.setNumItem(++numItem);
                itemSaida.setCodProduto(itemProduto.getCodProduto());
                itemSaida.setQuantKg(itemProduto.getQuantKg());
                itemSaida.setQuantUn(BigDecimal.ZERO);
                itemSaida.setComprimento(BigDecimal.ZERO);
                itensSaida.add(itemSaida);
                if (itemProduto.getSobra()) {
                    itemSaida.setQuantKg(
                            nt.casasDecimais(2, itemProduto.getComprimento()
                                    .divide(nt.tamanhoReal(itemProduto.getDescProduto())).multiply(itemProduto.getFconversao()))
                    );
                }
            }
        }
        return itensSaida;
    }

    public ObservableList<MateriaisData> _treatEstoqueNovaSaida(ObservableList<MateriaisData> materiaisSelecionados) {
        ObservableList<MateriaisData> itensSaida = FXCollections.observableArrayList();
        for (MateriaisData itemProduto : materiaisSelecionados) {
            if (itemProduto.getEscolha()) {
                itemProduto.setQuantUn(BigDecimal.ZERO);
                if (itemProduto.getSobra()) {
                    itemProduto.setQuantKg(nt.casasDecimais(
                            2, itemProduto.getComprimento()
                            .divide(BigDecimal.valueOf(1000))
                            .divide(nt.tamanhoReal(itemProduto.getDescProduto())
                            .divide(BigDecimal.valueOf(1000)), BigDecimal.ROUND_UP)
                            .multiply(itemProduto.getFconversao())));
                }
                itensSaida.add(itemProduto);
            }
        }
        return itensSaida;
    }

}
