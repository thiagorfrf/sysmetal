package br.org.galvanisa.business.controleof;

import br.org.galvanisa.business.entity.ControleOf;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import utilities.NewHibernateUtil;

/**
 *
 * @author thiago
 */
public class ControleOfModel {

    private ObservableList<ControleOf> controleOf;
    private final String QUERY_OF_ATIVAS_POR_CLIENTES = "from ControleOf where cod_controle_cliente = :codControleCliente and status = true";
    private final String QUERY_OF_POR_CLIENTES = "from ControleOf where cod_controle_cliente = :codControleCliente";
    private final String QUERY_OF_POR_CODIGO = "from ControleOf where cod_controle_of = :codControleOf";

    public List getOfs(Integer codControleCliente) {
        List list = null;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_OF_POR_CLIENTES;
            Query q = session.createQuery(hql);
            q.setParameter("codControleCliente", codControleCliente);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }
    
        public List getOfsAtivasPorCliente(Integer codControleCliente) {
        List list = null;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_OF_ATIVAS_POR_CLIENTES;
            Query q = session.createQuery(hql);
            q.setParameter("codControleCliente", codControleCliente);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public List getOfsPorCodigo(Integer codControleOf) {
        List list = null;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_OF_POR_CODIGO;
            Query q = session.createQuery(hql);
            q.setParameter("codControleOf", codControleOf);
            list = q.list();
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return list;
    }

    public ObservableList<ControleOf> _getOfsPorCodigoCliente(Integer codControleCliente) {
        List list = null;
        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String hql = QUERY_OF_POR_CLIENTES;
            Query q = session.createQuery(hql);
            q.setParameter("codControleCliente", codControleCliente);
            list = q.list();

            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }

        controleOf = FXCollections.observableArrayList(list);
        return controleOf;
    }

    public void addOf(ControleOf controleOf) {

        try {
            Session session = NewHibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(controleOf);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    public String alterStatus(ControleOf controleOf) {
        String status = "atualizacao_sucesso";
        Session session = NewHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {
            session.update(controleOf);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException he) {
            System.out.println("Exceção em Banco de dados - atualizarUsuario");
            status = he.getCause().getLocalizedMessage();
        }
        return status;
    }

    public void addPc() {

    }

    public void disableOf() {

    }

    public void disablePc() {
    }

}
