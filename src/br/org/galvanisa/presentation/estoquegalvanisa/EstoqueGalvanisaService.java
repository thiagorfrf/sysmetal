package br.org.galvanisa.presentation.estoquegalvanisa;

import br.org.galvanisa.business.estoquegalvanisa.EstoqueGalvanisaModel;
import br.org.galvanisa.business.entity.estoquegalvanisa.ProdutosGalvanisa;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class EstoqueGalvanisaService {

    EstoqueGalvanisaModel estoqueGalvanisa = new EstoqueGalvanisaModel();

    public ObservableList<ProdutosGalvanisa> getProdutosGalvanisa() {
        return estoqueGalvanisa.getProdutosGalvanisa();
    }
   
}