/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presentation.entradaitens;

import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.EntradaItens;
import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.entrada.EntradaBusinessEntity;
import br.org.galvanisa.business.entity.produtosdescricao.ProdutosDescricao;
import br.org.galvanisa.business.entradaitens.EntradaItensModel;
import br.org.galvanisa.business.estoque.EstoqueModel;
import br.org.galvanisa.presentation.entradas.ClienteEntradasService;
import br.org.galvanisa.presentation.materiais.ClienteMateriaisService;
import java.math.BigDecimal;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class EntradaItensService {

    private ObservableList<ProdutosDescricao> produtosEntradasList = FXCollections.observableArrayList();
    private ObservableList<EntradaItens> entradaItensList;
    private ObservableList<Estoque> estoqueList;
    private ObservableList<Entrada> entradaList;
    private EntradaItensModel entradaItensModel = new EntradaItensModel();
    private Entrada entrada = new Entrada();
    private Produtos produtos;
    private ClienteEntradasService clienteEntradasService = new ClienteEntradasService();
    private ClienteMateriaisService clienteMateriaisService = new ClienteMateriaisService();
    private EstoqueModel estoqueModel = new EstoqueModel();

    public ObservableList<ProdutosDescricao> loadEntradaItens(int codEntrada) {
        entradaItensList = entradaItensModel.loadProdutosEntrada(codEntrada);
        for (int i = 0; i < entradaItensList.size(); i++) {
            ProdutosDescricao objProdutosEntradas = new ProdutosDescricao();
            produtos = entradaItensModel.loadProdutosEntradaItens(entradaItensList.get(i).getCodProduto());
            objProdutosEntradas.setCodigo(entradaItensList.get(i).getCodProduto());
            objProdutosEntradas.setDescricao(produtos.getDescricao());
            objProdutosEntradas.setQuantKg(entradaItensList.get(i).getQuantKg());
            objProdutosEntradas.setQuantUn(entradaItensList.get(i).getQuantUn());
            objProdutosEntradas.setCodEstoque(entradaItensList.get(i).getCodEstoque());
            objProdutosEntradas.setCodigoMovimentacao(entradaItensList.get(i).getCodEntrada());
            produtosEntradasList.addAll(objProdutosEntradas);
        }
        return produtosEntradasList;
    }

    public void estornoEntradaApply(EntradaBusinessEntity entrada, ObservableList<ProdutosDescricao> produtosEntrada) {
        estoqueList = estoqueModel.loadEstoquePorCliente(entrada.getCodCliente());
        Boolean entradaDeletada = false;
        for (int i = 0; i < estoqueList.size(); i++) {
            for (int j = 0; j < produtosEntrada.size(); j++) {
                //verificação para pegar os produtos do estoque referente aos produtos da entrada
                if (estoqueList.get(i).getCodigo() == produtosEntrada.get(j).getCodEstoque()) {
                    Estoque objEstoque = new Estoque();
                    BigDecimal novaQuantidade = new BigDecimal("0");
                    BigDecimal novoPeso = new BigDecimal("0");
                    BigDecimal novaQuantidadeLiberada = new BigDecimal("0");
                    novaQuantidade = estoqueList.get(i).getQuantUn().subtract(produtosEntrada.get(j).getQuantUn());
                    novoPeso = estoqueList.get(i).getQuantKg().subtract(produtosEntrada.get(j).getQuantKg());
                    novaQuantidadeLiberada = estoqueList.get(i).getQuantLiberada().subtract(produtosEntrada.get(j).getQuantUn());

                    objEstoque = estoqueList.get(i);

                    objEstoque.setQuantKg(novoPeso);
                    objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                    objEstoque.setQuantUn(novaQuantidade);

                    this.entrada.setCodEntrada(entrada.getCodEntrada());

                    clienteMateriaisService.changeEstoque(objEstoque);
                    if (entradaDeletada == false) {
                        clienteEntradasService.deleteEntrada(this.entrada);
                        entradaDeletada = true;
                    }
                }
            }
        }
    }

    public void estornoEntradaApply(Entrada entrada) {
        estoqueList = estoqueModel.loadEstoquePorCliente(entrada.getCodCliente());
        Boolean entradaDeletada = false;
        ObservableList<ProdutosDescricao> produtosEntrada = loadEntradaItens(entrada.getCodEntrada());
        for (int i = 0; i < estoqueList.size(); i++) {
            for (int j = 0; j < produtosEntrada.size(); j++) {
                if (estoqueList.get(i).getCodigo() == produtosEntrada.get(j).getCodEstoque()) {
                    Estoque objEstoque = new Estoque();
                    BigDecimal novaQuantidade = new BigDecimal("0");
                    BigDecimal novoPeso = new BigDecimal("0");
                    BigDecimal novaQuantidadeLiberada = new BigDecimal("0");
                    novaQuantidade = estoqueList.get(i).getQuantUn().subtract(produtosEntrada.get(j).getQuantUn());
                    novoPeso = estoqueList.get(i).getQuantKg().subtract(produtosEntrada.get(j).getQuantKg());
                    novaQuantidadeLiberada = estoqueList.get(i).getQuantLiberada().subtract(produtosEntrada.get(j).getQuantUn());

                    objEstoque = estoqueList.get(i);

                    objEstoque.setQuantKg(novoPeso);
                    objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                    objEstoque.setQuantUn(novaQuantidade);

                    this.entrada.setCodEntrada(entrada.getCodEntrada());

                    clienteMateriaisService.changeEstoque(objEstoque);
                    if (entradaDeletada == false) {
                        clienteEntradasService.deleteEntrada(this.entrada);
                        entradaDeletada = true;
                    }
                }
            }
        }
    }
}
