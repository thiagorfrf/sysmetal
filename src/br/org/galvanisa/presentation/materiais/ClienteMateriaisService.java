package br.org.galvanisa.presentation.materiais;

import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.SaidaItens;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.materiais.ClienteMateriaisModel;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class ClienteMateriaisService {

    private final ClienteMateriaisModel clienteMateriaisModel = new ClienteMateriaisModel();
    private ObservableList<MateriaisData> materiaisData = FXCollections.observableArrayList();
    private List materiaisDataList;

    public ObservableList<MateriaisData> loadClienteMateriais(String codCliente) {
        return clienteMateriaisModel.loadClienteMateriais(codCliente);
    }

    public ObservableList<MateriaisData> loadClienteMateriaisSobrasOff(String codCliente) {
        ObservableList<MateriaisData> objMateriaisDataLocal2 = FXCollections.observableArrayList();
        //objMateriaisDataList = clienteMateriaisModel.loadClienteMateriais(codCliente);
        objMateriaisDataLocal2.addAll(getSaidasNormais(clienteMateriaisModel.loadClienteMateriais(codCliente)));

        return objMateriaisDataLocal2;
    }

    public ObservableList<MateriaisData> loadClienteMateriaisSobras(String codCliente) {
        ObservableList<MateriaisData> objMateriaisDataLocal = FXCollections.observableArrayList();
        objMateriaisDataLocal.addAll(getSaidasSobras(clienteMateriaisModel.loadClienteMateriais(codCliente)));
        return objMateriaisDataLocal;
    }

    public List loadEstoque(String codCliente) {
        return clienteMateriaisModel.loadEstoque(codCliente);
    }

    public List loadEstoque(String codCliente, String codProduto) {
        return clienteMateriaisModel.loadEstoque(codCliente, codProduto);
    }
    public List loadEstoquePorCodigo(int codigo) {
        return clienteMateriaisModel.loadEstoquePorCodigo(codigo);
    }

    public void changeEstoque(Estoque get) {
        clienteMateriaisModel.changeEstoque(get);
    }

    public void insertInToEstoque(Estoque estoque) {
        clienteMateriaisModel.insertInToEstoque(estoque);
    }

    public ObservableList<SaidaItens> treatEstoqueNovaSaida(ObservableList<MateriaisData> materiaisSelecionados) {
        return clienteMateriaisModel.treatEstoqueNovaSaida(materiaisSelecionados);
    }

    public ObservableList<MateriaisData> _treatEstoqueNovaSaida(ObservableList<MateriaisData> materiaisSelecionados) {
        return clienteMateriaisModel._treatEstoqueNovaSaida(materiaisSelecionados);
    }

    List<MateriaisData> getSaidasSobras(List<MateriaisData> materiais) {
        List novaLista = new ArrayList();
        for (int i = 0; i < materiais.size(); i++) {
            if (materiais.get(i).getSobra() == true) {
                novaLista.add(materiais.get(i));

            }
        }
        return novaLista;
    }

    List<MateriaisData> getSaidasNormais(List<MateriaisData> materiais) {
        List<MateriaisData> novaLista = new ArrayList();
        for (int i = 0; i < materiais.size(); i++) {
            if (materiais.get(i).getSobra() == Boolean.FALSE) {
                novaLista.add(materiais.get(i));
            }
        }
        return novaLista;
    }
}