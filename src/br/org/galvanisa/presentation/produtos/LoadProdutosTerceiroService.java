package br.org.galvanisa.presentation.produtos;

import br.org.galvanisa.business.controlecliente.ControleClienteModel;
import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.EntradaItens;
import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.produtos.ProdutosEstoqueTerceiros;
import br.org.galvanisa.business.entradaitens.EntradaItensModel;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.business.estoque.EstoqueModel;
import br.org.galvanisa.business.produtos.LoadProdutosModel;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class LoadProdutosTerceiroService {

    LoadProdutosModel loadProdutosModel = new LoadProdutosModel();
    LoadProdutosModel loadProdutosModel2 = new LoadProdutosModel();
    EntradaItensModel entradaItensModel = new EntradaItensModel();
    EstoqueModel estoqueModel = new EstoqueModel();
    EntradasModel entradasModel = new EntradasModel();
    ControleClienteModel controleClienteModel = new ControleClienteModel();
    ObservableList<Entrada> entradaList = FXCollections.observableArrayList();
    ObservableList<Produtos> produtosList = FXCollections.observableArrayList();
    ObservableList<ControleCliente> controleClientesList = FXCollections.observableArrayList();
    ObservableList<ProdutosEstoqueTerceiros> produtosEstoqueTerceirosList = FXCollections.observableArrayList();
    List<Entrada> entradaList2 = new ArrayList<Entrada>();
    List<Produtos> produtosList2 = new ArrayList<Produtos>();

    public ObservableList<Produtos> loadProdutos() {
        return loadProdutosModel.loadProdutos();
    }
    

    public ObservableList<ProdutosEstoqueTerceiros> loadProdutosProperty() {
        ObservableList<ControleCliente> objControleClientesLocalList = FXCollections.observableArrayList();
        ObservableList<EntradaItens> objEntradaItensList = entradaItensModel.loadAllProdutosEntrada();
        carregarListas();

        for (int i = 0; i < objEntradaItensList.size(); i++) {
            entradaList.addAll(getEntradaCliente(entradaList2, objEntradaItensList.get(i).getCodEntrada()));
            produtosList.addAll(getProdutosEntrada(produtosList2, objEntradaItensList.get(i).getCodProduto()));
            objControleClientesLocalList.addAll(getClienteEntrada(controleClientesList, entradaList.get(i).getCodCliente()));
        }
        for (int i = 0; i < objEntradaItensList.size(); i++) {
            ProdutosEstoqueTerceiros objProdutosEstoqueTerceiros = new ProdutosEstoqueTerceiros();
            objProdutosEstoqueTerceiros.setClienteDescricao(objControleClientesLocalList.get(i).getDescCliente());
            objProdutosEstoqueTerceiros.setCodigo(new SimpleStringProperty(produtosList.get(i).getCodigo()));
            objProdutosEstoqueTerceiros.setDescricao(new SimpleStringProperty(produtosList.get(i).getDescricao()));
            objProdutosEstoqueTerceiros.setFconversao(produtosList.get(i).getFconversao());
            objProdutosEstoqueTerceiros.setQuantKg(objEntradaItensList.get(i).getQuantKg());
            objProdutosEstoqueTerceiros.setQuantUn(objEntradaItensList.get(i).getQuantUn());
            produtosEstoqueTerceirosList.add(objProdutosEstoqueTerceiros);
        }
        return produtosEstoqueTerceirosList;
    }

    public ObservableList<ProdutosEstoqueTerceiros> loadEstoqueProperty() {
        carregarListas();
        ObservableList<ControleCliente> objControleClientesLocalList = FXCollections.observableArrayList();
        ObservableList<Estoque> objEstoqueList = estoqueModel.loadAllEstoque();
        produtosList2 = loadProdutosModel2.loadProdutos();
        entradaList2 = entradasModel.loadAllEntradas();
        for (int i = 0; i < objEstoqueList.size(); i++) {
            produtosList.addAll(getProdutosEntrada(produtosList2, objEstoqueList.get(i).getCodProduto()));
            objControleClientesLocalList.addAll(getClienteEntrada(controleClientesList, objEstoqueList.get(i).getCodCliente()));
        }
        for (int i = 0; i < objEstoqueList.size(); i++) {
            ProdutosEstoqueTerceiros objProdutosEstoqueTerceiros = new ProdutosEstoqueTerceiros();
            objProdutosEstoqueTerceiros.setClienteDescricao(objControleClientesLocalList.get(i).getDescCliente());
            objProdutosEstoqueTerceiros.setCodigo(new SimpleStringProperty(produtosList.get(i).getCodigo()));
            objProdutosEstoqueTerceiros.setDescricao(new SimpleStringProperty(produtosList.get(i).getDescricao()));
            objProdutosEstoqueTerceiros.setFconversao(produtosList.get(i).getFconversao());
            objProdutosEstoqueTerceiros.setQuantKg(objEstoqueList.get(i).getQuantKg());
            objProdutosEstoqueTerceiros.setQuantUn(objEstoqueList.get(i).getQuantUn());
            produtosEstoqueTerceirosList.add(objProdutosEstoqueTerceiros);
        }
        return produtosEstoqueTerceirosList;
    }

    List<Entrada> getEntradaCliente(List<Entrada> entradaList, int codEntrada) {
        return entradaList.stream().filter(p -> p.getCodEntrada() == codEntrada)
                .collect(Collectors.<Entrada>toList());
    }

    List<Produtos> getProdutosEntrada(List<Produtos> produtoList, String codProduto) {
        return produtoList.stream().filter(p -> p.getCodigo().trim().equals(codProduto.trim()))
                .collect(Collectors.<Produtos>toList());
    }

    List<ControleCliente> getClienteEntrada(List<ControleCliente> controleClienteList, String codCliente) {
        return controleClienteList.stream().filter(p -> p.getCodCliente().trim().equals(codCliente.trim()))
                .collect(Collectors.<ControleCliente>toList());
    }

    private void carregarListas() {
        controleClientesList = controleClienteModel.getAllControleCliente();
    }
}
