package br.org.galvanisa.presentation.controleof;

import br.org.galvanisa.business.controleof.ControleOfModel;
import br.org.galvanisa.business.entity.ControleOf;
import java.util.List;
import javafx.beans.Observable;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class ControleOfService {

    ControleOf controleof = new ControleOf();
    ControleOfModel controleOfs = new ControleOfModel();

    public List getOfs(Integer codControleCliente) {
        return controleOfs.getOfs(codControleCliente);
    }

    public List getOfsAtivasPorCliente(Integer codControleCliente) {
        return controleOfs.getOfsAtivasPorCliente(codControleCliente);
    }

    public ControleOf getOfsPorCodigo(Integer codControleOf) {
        for (int i = 0; i < controleOfs.getOfsPorCodigo(codControleOf).size(); i++) {
            controleof = (ControleOf) controleOfs.getOfsPorCodigo(codControleOf).get(i);
        }
        return controleof;
    }

    public ObservableList<ControleOf> _getOfsPorCodigoCliente(Integer codControleOf) {
        return controleOfs._getOfsPorCodigoCliente(codControleOf);
    }

    public void addOf(ControleOf controleOf) {
        controleOfs.addOf(controleOf);
    }

    public void addPc() {
        controleOfs.addPc();
    }

    public void disableOf() {
        controleOfs.disableOf();
    }

    public void disablePc() {
        controleOfs.disablePc();
    }

    public void alterStatus(ControleOf controleOf) {
        controleOfs.alterStatus(controleOf);
    }
}