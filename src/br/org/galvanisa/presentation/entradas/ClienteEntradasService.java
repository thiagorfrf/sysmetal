/*
 **Esta classe contem o tratamento
 *  da descrição do fornecedor 
 */
package br.org.galvanisa.presentation.entradas;

import br.org.galvanisa.business.cadastre.entrada.EntradaProdutos;
import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.entrada.EntradaBusinessEntity;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class ClienteEntradasService {

    private EntradasModel clienteEntradasModel = new EntradasModel();
    private ControleClienteService controleClienteService = new ControleClienteService();
    private List<ControleCliente> controleClienteList = new ArrayList<ControleCliente>();

    String getDescFornecedor(String codFornecedor) {
        String descFornecedor = null;

        if (codFornecedor.contains("0047")) {
            descFornecedor = ("GERDAU");
        } else if (codFornecedor.contains("1159")) {
            descFornecedor = ("GALFERRO");
        } else if (codFornecedor.contains("0720")) {
            descFornecedor = ("SCALA");
        } else if (codFornecedor.contains("0032")) {
            descFornecedor = ("CM");
        } else if (codFornecedor.contains("1971")) {
            descFornecedor = ("CEDISA");
        } else if (codFornecedor.contains("0032")) {
            descFornecedor = ("CM");
        } else {
            descFornecedor = codFornecedor;
        }
        return descFornecedor;
    }

    public ObservableList<Entrada> loadClienteEntradas(String CodCliente) {
        return clienteEntradasModel.loadClienteEntradas(CodCliente);
    }

    public ObservableList<EntradaBusinessEntity> loadClienteEntradasFilter(String CodCliente) {
        controleClienteList.addAll(controleClienteService.getControleCliente());
        ObservableList<EntradaBusinessEntity> entradaBusinnesEntityList = FXCollections.observableArrayList();
        ObservableList<Entrada> objClienteEntradasModelLocal = FXCollections.observableArrayList();
        objClienteEntradasModelLocal = clienteEntradasModel.loadClienteEntradas(CodCliente);      
        
        for (int i = 0; i < objClienteEntradasModelLocal.size(); i++) {
            EntradaBusinessEntity entradaBusinnesEntity = new EntradaBusinessEntity();
            entradaBusinnesEntity.setCodCliente(objClienteEntradasModelLocal.get(i).getCodCliente());
            entradaBusinnesEntity.setCodEntrada(objClienteEntradasModelLocal.get(i).getCodEntrada());
            entradaBusinnesEntity.setNumCertificado(objClienteEntradasModelLocal.get(i).getNumCertificado());
            entradaBusinnesEntity.setNumFornecedor(objClienteEntradasModelLocal.get(i).getNumFornecedor());
            entradaBusinnesEntity.setNumNf(objClienteEntradasModelLocal.get(i).getNumNf());
            entradaBusinnesEntity.setTipoOperacao(objClienteEntradasModelLocal.get(i).getTipoOperacao());
            entradaBusinnesEntity.setTotalKg(objClienteEntradasModelLocal.get(i).getTotalKg());
            entradaBusinnesEntity.setDtEntrada(objClienteEntradasModelLocal.get(i).getDtEntrada());
            String fornecedor;
            if(getFornecedor(controleClienteList , objClienteEntradasModelLocal.get(i).getNumFornecedor()).isEmpty()){
                 fornecedor = objClienteEntradasModelLocal.get(i).getNumFornecedor();
            }else {
            fornecedor = getFornecedor(controleClienteList , objClienteEntradasModelLocal.get(i).getNumFornecedor()).get(0).getDescCliente();
            }
            entradaBusinnesEntity.setDescFornecedor(fornecedor);
            
            if (objClienteEntradasModelLocal.get(i).getTipoOperacao() == 'T') {
                entradaBusinnesEntity.setTipoOperacaoString("Transferência");
            } else if (objClienteEntradasModelLocal.get(i).getTipoOperacao() == 'N') {
                entradaBusinnesEntity.setTipoOperacaoString("Normal");
            } else if (objClienteEntradasModelLocal.get(i).getTipoOperacao() == 'E') {
                entradaBusinnesEntity.setTipoOperacaoString("Empenho");
            } else if (objClienteEntradasModelLocal.get(i).getTipoOperacao() == 'S') {
                entradaBusinnesEntity.setTipoOperacaoString("Sobra");
            }

            //objEntradaBusinnesEntity.setDescFornecedor((objControleClienteService.getAllControleClientePorCodigo(objClienteEntradasModelLocal.get(i).getNumFornecedor()).getDescCliente()));
            entradaBusinnesEntity.setDataFormatada(
                    objClienteEntradasModelLocal.get(i).getDtEntrada().substring(6, 8) + '/'
                    + objClienteEntradasModelLocal.get(i).getDtEntrada().substring(4, 6) + '/'
                    + objClienteEntradasModelLocal.get(i).getDtEntrada().substring(0, 4));

            entradaBusinnesEntityList.add(entradaBusinnesEntity);
        }

        return entradaBusinnesEntityList;
    }

    public void saveNewEntrada(Entrada entrada, ObservableList<EntradaProdutos> dataTableViewInserir) {
        clienteEntradasModel.saveNewEntrada(entrada, dataTableViewInserir);

    }

    public void deleteEntrada(Entrada get) {
        clienteEntradasModel.deleteEstoque(get);
    }
    
        List<ControleCliente> getFornecedor(List<ControleCliente> controleClienteList, String codigo) {
        return controleClienteList.stream().filter(p -> p.getCodCliente().trim().equals(codigo.trim()))
                .collect(Collectors.<ControleCliente>toList());
    }
}