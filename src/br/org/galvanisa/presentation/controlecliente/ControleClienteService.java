/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presentation.controlecliente;

import java.util.List;
import br.org.galvanisa.business.controlecliente.ControleClienteModel;
import br.org.galvanisa.business.entity.ControleCliente;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class ControleClienteService {

    private final ControleClienteModel controleClienteModel = new ControleClienteModel();

    public void addControleCliente(ControleCliente cliente) {
        controleClienteModel.addControleCliente(cliente);
    }

    public ObservableList<ControleCliente> getAllControleCliente() {
        return controleClienteModel.getAllControleCliente();
    }

    public List<ControleCliente> getControleCliente() {
        return controleClienteModel.getControleCliente();
    }

    public List<ControleCliente> getControleClienteAtivosTerceiros() {
        return controleClienteModel.getControleClienteAtivosTerceiros();
    }

    public List<ControleCliente> getControleClienteAtivosGalvanisa() {
        return controleClienteModel.getControleClienteAtivosGalvanisa();
    }

    public ControleCliente getAllControleClientePorCodigo(String cliente) {
        return controleClienteModel.getAllControleClientePorCodigo(cliente);
    }
    
        public ControleCliente getAllControleClientePorCodigoControle(Integer CodControleCliente) {
        return controleClienteModel.getAllControleClientePorCodigoControle(CodControleCliente);
    }

    public ControleCliente getAllControleClientePorCodigoControle(int codigo) {
        return controleClienteModel.getAllControleClientePorCodigoControle(codigo);
    }

    public void disableClient(ControleCliente cliente) {
        //objControleClienteModel.disableCliente(cliente);
    }

    public void alterStatus(ControleCliente cliente) {
        controleClienteModel.alterStatus(cliente);
    }
    public void updateCliente(ControleCliente cliente){
        controleClienteModel.updateCliente(cliente);
    }

    public ObservableList<ControleCliente> getAllClientesMPInterna() {
        return controleClienteModel.getAllClientesMPInterna();
    }

    public ObservableList<ControleCliente> getAllClientesTerceirosAtivos() {
        return controleClienteModel.getAllClientesTerceiros();
    }
}