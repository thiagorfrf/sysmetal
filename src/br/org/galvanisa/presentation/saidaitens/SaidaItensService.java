/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.presentation.saidaitens;

import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.Estoque;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.SaidaItens;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.entity.produtosdescricao.ProdutosDescricao;
import br.org.galvanisa.business.entity.saida.SaidaBusinessEntity;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.business.estoque.EstoqueModel;
import br.org.galvanisa.business.saidaitens.SaidaItensModel;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.entradaitens.EntradaItensService;
import br.org.galvanisa.presentation.materiais.ClienteMateriaisService;
import br.org.galvanisa.presentation.saidas.SaidasService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class SaidaItensService {
    
    SaidaItensModel saidasItensModel = new SaidaItensModel();
    private ObservableList<ProdutosDescricao> produtosSaidasList = FXCollections.observableArrayList();
    private ObservableList<SaidaItens> saidaItensList;
    private Saida saida = new Saida();
    private Produtos produtos;
    private List<Estoque> estoqueList;
    private EstoqueModel estoqueModel = new EstoqueModel();
    private ClienteMateriaisService clienteMateriaisService = new ClienteMateriaisService();
    private SaidasService saidasService = new SaidasService();
    private List<Estoque> AllestoqueList = new ArrayList<Estoque>();
    private List<SaidaItens> saidaItensList2 = new ArrayList<SaidaItens>();
    private EntradasModel entradasModel = new EntradasModel();
    private EntradaItensService entradaItensService = new EntradaItensService();
    
    public ObservableList<ProdutosDescricao> loadSaidaItens(int codSaida) {
        saidaItensList = saidasItensModel.loadProdutosSaida(codSaida);
        for (int i = 0; i < saidaItensList.size(); i++) {
            ProdutosDescricao objProdutosEntradas = new ProdutosDescricao();
            produtos = saidasItensModel.loadProdutosSaidaItens(saidaItensList.get(i).getCodProduto());
            objProdutosEntradas.setCodigo(saidaItensList.get(i).getCodProduto());
            objProdutosEntradas.setDescricao(produtos.getDescricao());
            objProdutosEntradas.setQuantKg(saidaItensList.get(i).getQuantKg());
            objProdutosEntradas.setQuantUn(saidaItensList.get(i).getQuantUn());
            objProdutosEntradas.setCodEstoque(saidaItensList.get(i).getCodEstoque());
            produtosSaidasList.addAll(objProdutosEntradas);
        }
        return produtosSaidasList;
    }
    
    public void estornoSaidaApply(SaidaBusinessEntity saida, ObservableList<ProdutosDescricao> produtosSaida, String tipoOperacao) {
        saidaItensList2 = saidasService.getSaidaItens(saida.getCodSaida());
        AllestoqueList = estoqueModel.AllEstoque();
        Boolean saidaDeletada = false;
        for (int i = 0; i < saidaItensList2.size(); i++) {
            estoqueList = getEstoqueSaida(AllestoqueList, saidaItensList2.get(i).getCodEstoque());
            if (!estoqueList.isEmpty()) {
                for (int j = 0; j < produtosSaida.size(); j++) {
                    if (produtosSaida.get(j).getCodEstoque() == saidaItensList2.get(i).getCodEstoque()) {
                        Estoque objEstoque = new Estoque();
                        objEstoque = estoqueList.get(0);
                        BigDecimal novaQuantidade = new BigDecimal("0");
                        BigDecimal novoPeso = new BigDecimal("0");
                        BigDecimal novaQuantidadeLiberada = new BigDecimal("0");
                        BigDecimal novaQuantidadeEmpenho = new BigDecimal("0");
                        
                        novaQuantidade = estoqueList.get(0).getQuantUn().add(produtosSaida.get(j).getQuantUn());
                        novoPeso = estoqueList.get(0).getQuantKg().add(produtosSaida.get(j).getQuantKg());
                        novaQuantidadeLiberada = estoqueList.get(0).getQuantLiberada().add(produtosSaida.get(j).getQuantUn());
                        novaQuantidadeEmpenho = estoqueList.get(0).getQuantEmpenho().subtract(produtosSaida.get(j).getQuantUn());
                        
                        this.saida.setCodSaida(saida.getCodSaida());
                        
                        if (tipoOperacao.equals("N")) {
                            objEstoque.setCodigo(produtosSaida.get(j).getCodEstoque());
                            objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                            objEstoque.setQuantKg(novoPeso);
                            objEstoque.setQuantUn(novaQuantidade);
                        } else if (tipoOperacao.equals("E")) {
                            objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                            objEstoque.setQuantEmpenho(novaQuantidadeEmpenho);
                        } else if (tipoOperacao.equals("D")) {
                            objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                            objEstoque.setQuantUn(novaQuantidade);
                            objEstoque.setQuantKg(novoPeso);
                        } else if (tipoOperacao.equals("T")) {
                            List<Entrada> entradas = new ArrayList<Entrada>();
                            objEstoque.setQuantLiberada(novaQuantidadeLiberada);
                            objEstoque.setQuantUn(novaQuantidade);
                            objEstoque.setQuantKg(novoPeso);
                            entradas = entradasModel.loadEntradasPorNF(Integer.toString(saida.getCodSaida()));
                            for (int k = 0; k < entradas.size(); k++) {
                                entradaItensService.estornoEntradaApply(entradas.get(k));
                            }
                        }
                        
                        clienteMateriaisService.changeEstoque(objEstoque);
                        if (saidaDeletada == false) {
                            saidasService.deleteSaida(this.saida);
                            saidaDeletada = true;
                        }
                    }
                }
            }
        }
    }
    
    List<Estoque> getEstoqueSaida(List<Estoque> estoque, int codEstoque) {
        return estoque.stream().filter(p -> p.getCodigo() == codEstoque)
                .collect(Collectors.<Estoque>toList());
    }
}
