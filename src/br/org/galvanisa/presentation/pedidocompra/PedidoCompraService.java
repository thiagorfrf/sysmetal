/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.org.galvanisa.presentation.pedidocompra;

import br.org.galvanisa.business.entity.pedidocompra.ProdutosPedidoCompra;
import br.org.galvanisa.business.pedidocompra.PedidoCompraModel;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class PedidoCompraService {
    PedidoCompraModel pedidoCompraModel = new PedidoCompraModel();
    ProdutosPedidoCompra produtosPedidoCompra = new ProdutosPedidoCompra();
        public ObservableList<ProdutosPedidoCompra> getPedidosCompras() {
        return pedidoCompraModel.getPedidosCompra();

    }
}