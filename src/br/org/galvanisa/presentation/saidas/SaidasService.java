package br.org.galvanisa.presentation.saidas;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.SaidaItens;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.entity.saida.SaidaBusinessEntity;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.business.saidas.SaidasModel;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presentation.controleof.ControleOfService;
import java.util.List;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author thiago
 */
public class SaidasService {

    SaidasModel saidasModel = new SaidasModel();
    EntradasModel entradasModel = new EntradasModel();
    private final SaidasModel clienteSaidasModel = new SaidasModel();
    private ControleClienteService controleClienteService = new ControleClienteService();
    private ControleOfService controleOfsService = new ControleOfService();
    String codClienteTransfrencia;
    String codCliente;
    int codControleOf;

    public ObservableList<SaidaBusinessEntity> loadSaidasFilter(int codCliente) {

        ObservableList<SaidaBusinessEntity> objSaidaBusinnesEntityList = FXCollections.observableArrayList();
        ObservableList<Saida> objClienteSaidasModelLocal = null;
        objClienteSaidasModelLocal = clienteSaidasModel.loadSaidas(codCliente);
        List<ControleOf> controleOfList = controleOfsService.getOfsAtivasPorCliente(codCliente);
        List<ControleCliente> controleClienteList = controleClienteService.getControleCliente();
        ControleOf controleOf;
        ControleCliente controleCliente;
        ControleCliente controleCliente2;

        for (int i = 0; i < objClienteSaidasModelLocal.size(); i++) {
            SaidaBusinessEntity objSaidaBusinnesEntity = new SaidaBusinessEntity();
            objSaidaBusinnesEntity.setCodControleCliente(objClienteSaidasModelLocal.get(i).getCodControleCliente());
            objSaidaBusinnesEntity.setCodSaida(objClienteSaidasModelLocal.get(i).getCodSaida());
            objSaidaBusinnesEntity.setCodControleOf(objClienteSaidasModelLocal.get(i).getCodControleOf());
            objSaidaBusinnesEntity.setCodClienteTransf(objClienteSaidasModelLocal.get(i).getCodClienteTransf());
            objSaidaBusinnesEntity.setTipoOperacao(objClienteSaidasModelLocal.get(i).getTipoOperacao());
            objSaidaBusinnesEntity.setTotalKg(objClienteSaidasModelLocal.get(i).getTotalKg());
            objSaidaBusinnesEntity.setDtSaida(objClienteSaidasModelLocal.get(i).getDtSaida());

            if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == 'T') {
                objSaidaBusinnesEntity.setTipoOperacaoString("Transferência");
            } else if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == 'N') {
                objSaidaBusinnesEntity.setTipoOperacaoString("Normal");
            } else if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == 'E') {
                objSaidaBusinnesEntity.setTipoOperacaoString("Empenho");
            } else if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == 'S') {
                objSaidaBusinnesEntity.setTipoOperacaoString("Sobra");
            } else if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == 'D') {
                objSaidaBusinnesEntity.setTipoOperacaoString("Devolução");
            } else if (objClienteSaidasModelLocal.get(i).getTipoOperacao() == null) {
            }
            if (objSaidaBusinnesEntity.getCodClienteTransf() == null || objSaidaBusinnesEntity.getCodClienteTransf() == "") {
                objSaidaBusinnesEntity.setClienteTransfString("--");
            } else {
                codClienteTransfrencia = objClienteSaidasModelLocal.get(i).getCodClienteTransf();
                if (!getActiveClienteTransferencia(controleClienteList, objClienteSaidasModelLocal.
                        get(i).getCodClienteTransf()).isEmpty()) {
                    controleCliente = getActiveClienteTransferencia(controleClienteList, codClienteTransfrencia).get(0);
                    objSaidaBusinnesEntity.setClienteTransfString(controleCliente.getDescCliente());
                }
            }
            if (getActiveCliente(controleClienteList, codCliente).size() > 0) {
                controleCliente2 = getActiveCliente(controleClienteList, codCliente).get(0);
                objSaidaBusinnesEntity.setControleClienteString(controleCliente2.getDescCliente());

            }
            objSaidaBusinnesEntity.setDtSaidaFormatada(
                    objClienteSaidasModelLocal.get(i).getDtSaida().substring(6, 8) + '/'
                    + objClienteSaidasModelLocal.get(i).getDtSaida().substring(4, 6) + '/'
                    + objClienteSaidasModelLocal.get(i).getDtSaida().substring(0, 4));

            objSaidaBusinnesEntityList.add(objSaidaBusinnesEntity);

            codControleOf = objSaidaBusinnesEntity.getCodControleOf();
            if (getActiveOf(controleOfList, codControleOf).size() > 0) {
                controleOf = getActiveOf(controleOfList, codControleOf).get(0);
                objSaidaBusinnesEntity.setOf(controleOf.getNumOf());
                objSaidaBusinnesEntity.setPc(controleOf.getNumPc());
            }

        }

        return objSaidaBusinnesEntityList;
    }

    List<ControleOf> getActiveOf(List<ControleOf> controleOf, int codControleOf) {
        return controleOf.stream().filter(p -> p.getCodControleOf() == codControleOf)
                .collect(Collectors.<ControleOf>toList());
    }

    List<ControleCliente> getActiveClienteTransferencia(List<ControleCliente> controleCliente, String clienteTransfrencia) {
        return controleCliente.stream().filter(p -> p.getCodCliente().trim().equals(clienteTransfrencia.trim()))
                .collect(Collectors.<ControleCliente>toList());
    }

    List<ControleCliente> getActiveCliente(List<ControleCliente> controleCliente, int codCliente) {
        return controleCliente.stream().filter(p -> p.getCodControle() == codCliente)
                .collect(Collectors.<ControleCliente>toList());
    }

    public ObservableList<Saida> loadSaidas(int CodControle) {
        return saidasModel.loadSaidas(CodControle);
    }

    public ObservableList<Saida> loadSaidasFilter2(int CodControle) {
        return saidasModel.loadSaidas(CodControle);
    }

    public void saveNewSaida(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {
        saidasModel.saveNewSaida(codCliente, saida, materiaisItens);
    }

    public void saveEntradaTransferencia(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {
        entradasModel.saveNewEntrada(codCliente, saida, materiaisItens);
    }

    public void saveNewEmpenho(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens) {
        saidasModel.saveNewEmpenho(codCliente, saida, materiaisItens);
    }

    public ObservableList<MateriaisData> getItensFromSaida(Saida saida) {
        return saidasModel.getItensFromSaida(saida);
    }

    public void getObjectsFromSaida(List<Object> objetosSaida) {
        saidasModel.getObjetcsFromSaida(objetosSaida);
    }

    public List<String> transformSaidaInToCadastroView(Saida saida) {
        return saidasModel.transformSaidaInToCadastroView(saida);
    }

    public void updateEmpenhoToSaida(String codCliente, Saida saida, ObservableList<MateriaisData> materiaisItens, List<Object> objetosSaida) {
        saidasModel.updateEmpenhoToSaida(codCliente, saida, materiaisItens, objetosSaida);
    }
    /*
     public void getObjectsFromSaida(List<Object> objetosSaida) {
     saidasModel.getObjetcsFromSaida(objetosSaida);
     }
     */

    public void deleteSaida(Saida objSaida) {
        clienteSaidasModel.deleteSaida(objSaida);
    }

    public List<SaidaItens> getSaidaItens(int codSaida) {
       return clienteSaidasModel.getSaidaItens(codSaida);
        
    }

    public Saida getSaida(int codSaida) {
        return clienteSaidasModel.getSaida(codSaida);
    }
}
