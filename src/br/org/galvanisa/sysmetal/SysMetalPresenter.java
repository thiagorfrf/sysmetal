/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.galvanisa.sysmetal;

import br.org.galvanisa.business.entity.ControleCliente;
import br.org.galvanisa.presentation.controlecliente.ControleClienteService;
import br.org.galvanisa.presenter.ClientesPresenter;
import br.org.galvanisa.presenter.EntradasPresenter;
import br.org.galvanisa.presenter.MateriaisPresenter;
import br.org.galvanisa.presenter.RelatoriosPresenter;
import br.org.galvanisa.presenter.SaidasPresenter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.controlsfx.dialog.Dialogs;

/**
 * FXML Controller class
 *
 * @author thiago
 */
public class SysMetalPresenter implements Initializable {

    private ObservableList<ControleCliente> listClientes = FXCollections.observableArrayList();
    private final ControleClienteService objControleClienteService = new ControleClienteService();
    public static SaidasPresenter saidasPresenter = new SaidasPresenter();
    private MateriaisPresenter materiaisPresenter = new MateriaisPresenter();
    public ControleCliente controleCliente;
    public Boolean clienteEstoqueTerceiro = true;
    public Boolean needReload = false;

    @FXML
    private BorderPane borderPane;
    @FXML
    private Button btnMPTerceiros;
    @FXML
    private Button btnMPGalvanisa;
    @FXML
    private Button btnMateriais;
    @FXML
    private Button btnEntradas;
    @FXML
    private Button btnSaidas;
    @FXML
    private Button btnRelatorios;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private Button button;
    @FXML
    private HBox footerBarHBox;
    @FXML
    private Button btnClientes;
    @FXML
    private VBox leftVBox;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        styleActiveButton(btnMateriais);

        setIconsButton();
        leftVBox.getStyleClass().add("leftVBox");
        footerBarHBox.setStyle("-fx-background-color: linear-gradient(from 100% 0% to 100% 100%, white 0%, #807f7f 100%);");

        try {
            loadMateriais();
        } catch (IOException ex) {
            Logger.getLogger(SysMetalPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        borderPane.getStyleClass().add("bordePane");
        mainTabPane.getStyleClass().add("table");
    }

    @FXML
    private void handleBtnMateriaisAction(ActionEvent event) throws IOException {
        styleActiveButton(btnMateriais);
        actionBtnMateriais();
    }

    @FXML
    private void handleBtnClienteAction(ActionEvent event) throws IOException {
        if (!clienteEstoqueTerceiro) {
            borderPane.setCenter(mainTabPane);
        }
        needReload = true;
        Tab tabCliente1 = new Tab();
        String tipo = "/view/Clientes.fxml";
        tabCliente1.setText("Painel ADM Clientes");
        FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
        tabCliente1.setContent(loader.load());
        ClientesPresenter controller = loader.<ClientesPresenter>getController();
        mainTabPane.getTabs().clear();
        mainTabPane.getTabs().add(tabCliente1);
        styleActiveButton(btnClientes);

    }

    @FXML
    private void handleBtnEntradasAction(ActionEvent event) throws IOException {

        actionLeftButtons("/view/Entradas.fxml", btnEntradas);

    }

    @FXML
    private void handleBtnSaidasAction(ActionEvent event) throws IOException {

        actionLeftButtons("/view/Saidas.fxml", btnSaidas);

    }

    @FXML
    private void handleBtnRelatoriosAction(ActionEvent event) throws IOException {

        actionLeftButtons("/view/Relatorios.fxml", btnRelatorios);

    }

    @FXML
    private void handleBtnMPGalvanisa(ActionEvent event) throws IOException {

        styleActiveButton(btnMateriais);
        terceirosToGalvanisa();
        // clienteEstoqueTerceiro = false;
        loadMateriais();
        mainTabPane.getTabs().clear();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EstoqueGalvanisa.fxml"));
        borderPane.setCenter(loader.load());

    }

    @FXML
    private void handleBtnMPTerceiros(ActionEvent event) throws IOException {

        galvanisaToTerceiros();
        borderPane.setCenter(mainTabPane);
        styleActiveButton(btnMateriais);
        loadMateriais();

    }

    private void loadTabs(String tipo, Integer tab) throws IOException {
        // primeiro loadTabs que ler "Materiais, seta na primeira Aba, nao vai ler o segundo paramentro
        // Criar uma interface e classes utilizando generics <T> para ler dinamicamente essas tabs
        //loadMateriais();
        
        if(needReload){
            needReload = false;
            loadMateriais();
        }
        mainTabPane.getTabs().clear();

        SingleSelectionModel<Tab> selectionModel = mainTabPane.getSelectionModel();

        List<Tab> todasTabs = new ArrayList<Tab>();

        for (ControleCliente cliente : listClientes) {
            Tab tabCliente = new Tab();
            todasTabs.add(tabCliente);
            tabCliente.setText(cliente.getDescCliente().trim() + " - " + cliente.getNumOp());
            if (tipo.equals("/view/Materiais.fxml")) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
                tabCliente.setContent(loader.load());
                MateriaisPresenter controller = loader.<MateriaisPresenter>getController();
                controller.setCliente(cliente, SysMetalPresenter.this);

            }

            if (tipo.equals("/view/Entradas.fxml")) {

                FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
                tabCliente.setContent(loader.load());
                EntradasPresenter controller = loader.<EntradasPresenter>getController();
                controller.setCliente(cliente);

            }

            if (tipo.equals("/view/Saidas.fxml")) {

                FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
                tabCliente.setContent(loader.load());
                SaidasPresenter controller = loader.<SaidasPresenter>getController();
                controller.setCliente(cliente);

            }

            if (tipo.equals("/view/Relatorios.fxml")) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
                tabCliente.setContent(loader.load());
                RelatoriosPresenter controller = loader.<RelatoriosPresenter>getController();
                controller.setCliente(cliente);
            }

            if (tipo.equals("/view/Estornos.fxml")) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(tipo));
                tabCliente.setContent(loader.load());
                RelatoriosPresenter controller = loader.<RelatoriosPresenter>getController();
                controller.setCliente(cliente);
            }
            mainTabPane.getTabs().add(tabCliente);

            //setar na tab correta.
            //mainTabPane.getTabs().;
        }
        if (todasTabs.isEmpty()) {

        } else {
            selectionModel.select(todasTabs.get(tab));
        }
        //mainTabPane.getTabs().set(2, tabCliente); //tabcliente vai dar error ?
    }

    public void setController(ControleCliente controleCliente, SaidasPresenter saidasPresenter) {
        this.saidasPresenter = saidasPresenter;
        this.controleCliente = controleCliente;

    }

    public void loadMateriais() throws IOException {
        if (!listClientes.isEmpty()) {
            listClientes.clear();
            if (clienteEstoqueTerceiro) {
                listClientes = FXCollections.observableList((List<ControleCliente>) objControleClienteService.getControleClienteAtivosTerceiros());
            } else if (!clienteEstoqueTerceiro) {
                listClientes = FXCollections.observableList((List<ControleCliente>) objControleClienteService.getControleClienteAtivosGalvanisa());
                if (listClientes.isEmpty()) {
                    //disableLeftButtons();
                    mainTabPane.getTabs().clear();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EstoqueGalvanisa.fxml"));
                    borderPane.setCenter(loader.load());
                }
            }
        } else {
            listClientes = FXCollections.observableList(
                    (List<ControleCliente>) objControleClienteService.getControleClienteAtivosTerceiros());
        }

        loadTabs("/view/Materiais.fxml", 0);

    }

    public void setTab(String descCliente) {
        mainTabPane.getSelectionModel().selectLast();
    }

    public void init() throws IOException {

    }

    public void loadAllMateriais() throws IOException {
        loadTabs("/view/Materiais.fxml", 0);
        styleActiveButton(btnMateriais);
    }

    public void styleActiveButton(Button button) {
        btnClientes.getStyleClass().removeAll("selectedButtom");
        btnEntradas.getStyleClass().removeAll("selectedButtom");
        btnMPGalvanisa.getStyleClass().removeAll("selectedButtom");
        btnMPTerceiros.getStyleClass().removeAll("selectedButtom");
        btnMateriais.getStyleClass().removeAll("selectedButtom");
        btnRelatorios.getStyleClass().removeAll("selectedButtom");
        btnSaidas.getStyleClass().removeAll("selectedButtom");
        button.getStyleClass().add("selectedButtom");
    }

    private void setIconsButton() {
        btnMPTerceiros.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Estoque1.png"))));
        btnMPGalvanisa.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Estoque3.png"))));
        btnMateriais.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Materiais.png"))));
        btnEntradas.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Entradas.png"))));
        btnSaidas.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Saidas.png"))));
        btnRelatorios.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/Reports.png"))));
        btnClientes.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/view/icons/cliente2.png"))));
    }

    public void actionLeftButtons(String tipo, Button button) {
        if (!clienteEstoqueTerceiro) {
            borderPane.setCenter(mainTabPane);
        }
        try {

            if (mainTabPane.getSelectionModel().isEmpty()) {
                loadTabs(tipo, 0);
            } else {
                loadTabs(tipo, mainTabPane.getSelectionModel().getSelectedIndex());
            }
            //loadTabs(tipo, mainTabPane.getSelectionModel().getSelectedIndex());
        } catch (IOException ex) {
            Logger.getLogger(SysMetalPresenter.class.getName()).log(Level.SEVERE, null, ex);
        }
        styleActiveButton(button);
    }

    private void disableLeftButtons() {

        btnEntradas.setVisible(false);
        btnSaidas.setVisible(false);
        btnRelatorios.setVisible(false);
    }

    private void enableLeftButtons() {
        btnMateriais.setVisible(true);
        btnEntradas.setVisible(true);
        btnSaidas.setVisible(true);
        btnRelatorios.setVisible(true);
        btnClientes.setVisible(true);
    }

    public void actionBtnMateriais() {

        if (clienteEstoqueTerceiro) {
            styleActiveButton(btnMateriais);
            borderPane.setCenter(mainTabPane);
            styleActiveButton(btnMateriais);
            try {
                // loadMateriais();
                if (mainTabPane.getSelectionModel().isEmpty()) {
                    loadTabs("/view/Materiais.fxml", 0);
                } else {
                    loadTabs("/view/Materiais.fxml", mainTabPane.getSelectionModel().getSelectedIndex());
                }
            } catch (IOException ex) {
                Logger.getLogger(SysMetalPresenter.class.getName()).log(Level.SEVERE, null, ex);

            }
        } else if (!clienteEstoqueTerceiro) {
            terceirosToGalvanisa();
            mainTabPane.getTabs().clear();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EstoqueGalvanisa.fxml"));
            styleActiveButton(btnMateriais);
            try {
                borderPane.setCenter(loader.load());
            } catch (IOException ex) {
                Logger.getLogger(SysMetalPresenter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void terceirosToGalvanisa() {
        btnEntradas.setText("Transferências");
        btnMateriais.setText("Estoque");
        clienteEstoqueTerceiro = false;
    }

    public void galvanisaToTerceiros() {
        btnEntradas.setText("Entradas");
        btnMateriais.setText("Materiais");
        clienteEstoqueTerceiro = true;
    }

    public void actionBtnTerceiros() throws IOException {

        //clienteEstoqueTerceiro = true;
        if (!listClientes.isEmpty()) {
            listClientes.clear();
            if (clienteEstoqueTerceiro) {
                listClientes = FXCollections.observableList((List<ControleCliente>) objControleClienteService.getControleClienteAtivosTerceiros());
            } else if (!clienteEstoqueTerceiro) {
                listClientes = FXCollections.observableList((List<ControleCliente>) objControleClienteService.getControleClienteAtivosGalvanisa());
                if (listClientes.isEmpty()) {
                    //disableLeftButtons();
                    mainTabPane.getTabs().clear();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EstoqueGalvanisa.fxml"));
                    borderPane.setCenter(loader.load());
                }
            }
        } else {
            listClientes = FXCollections.observableList(
                    (List<ControleCliente>) objControleClienteService.getControleClienteAtivosTerceiros());
        }

    }
}
