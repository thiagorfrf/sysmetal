/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import br.org.galvanisa.business.entity.Saida;
import java.math.BigDecimal;

/**
 *
 * @author thiago
 */
public class ImportSaidas extends Saida {

    private int codigo;
    private int codEstoque;
    private Integer codSaida;
    private Integer numItem;
    private String codProduto;
    private BigDecimal quantKg;
    private BigDecimal quantUn;
    private BigDecimal comprimento;

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodEstoque() {
        return codEstoque;
    }

    public void setCodEstoque(int codigoEstoque) {
        this.codEstoque = codigoEstoque;
    }

    public Integer getCodSaidaInSaidaItens() {
        return this.codSaida;
    }

    public void setCodSaida(Integer codSaida) {
        this.codSaida = codSaida;
    }

    public Integer getNumItem() {
        return this.numItem;
    }

    public void setNumItem(Integer numItem) {
        this.numItem = numItem;
    }

    public String getCodProduto() {
        return this.codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public BigDecimal getQuantKg() {
        return this.quantKg;
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg = quantKg;
    }

    public BigDecimal getQuantUn() {
        return this.quantUn;
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn = quantUn;
    }

    public BigDecimal getComprimento() {
        return this.comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }
}
