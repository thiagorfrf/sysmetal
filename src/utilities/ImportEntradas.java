/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import br.org.galvanisa.business.entity.Entrada;
import java.math.BigDecimal;

/**
 *
 * @author thiago
 */
public class ImportEntradas extends Entrada {

    private int codigo;
    private int codEstoque;
    private Integer codEntrada;
    private Integer numItem;
    private String codProduto;
    private BigDecimal quantKg;
    private BigDecimal quantUn;
    private BigDecimal comprimento;
    private BigDecimal fconversao;
    private Boolean sobra;

    public ImportEntradas() {
    }

    public ImportEntradas(int codigo) {
        this.codigo = codigo;
    }

    public ImportEntradas(int codigo, int codEstoque, Integer codEntrada, Integer numItem, String codProduto, BigDecimal quantKg, BigDecimal fconversao, BigDecimal quantUn, BigDecimal comprimento, Boolean sobra) {
        this.codigo = codigo;
        this.codEstoque = codEstoque;
        this.codEntrada = codEntrada;
        this.numItem = numItem;
        this.codProduto = codProduto;
        this.quantKg = quantKg;
        this.quantUn = quantUn;
        this.comprimento = comprimento;
        this.sobra = sobra;
        this.fconversao = fconversao;
    }

    public ImportEntradas(Integer numItem, String codProduto, BigDecimal quantKg, BigDecimal quantUn, BigDecimal comprimento, BigDecimal fconversao, Boolean sobra) {
        //this.codigo = codigo;
        //this.codEntrada = codEntrada;
        this.numItem = numItem;
        this.codProduto = codProduto;
        this.quantKg = quantKg;
        this.quantUn = quantUn;
        this.comprimento = comprimento;
        this.sobra = sobra;
        this.fconversao = fconversao;
    }

    public BigDecimal getFconversao() {
        return fconversao;
    }

    public void setFconversao(BigDecimal fconversao) {
        this.fconversao = fconversao;
    }
    
    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Integer getCodEntradalocal() {
        return this.codEntrada;
    }

    public void setCodEntrada(Integer codEntrada) {
        this.codEntrada = codEntrada;
    }

    public int getCodEstoque() {
        return codEstoque;
    }

    public void setCodEstoque(int codEstoque) {
        this.codEstoque = codEstoque;
    }

    public Integer getNumItem() {
        return this.numItem;
    }

    public void setNumItem(Integer numItem) {
        this.numItem = numItem;
    }

    public String getCodProduto() {
        return this.codProduto;
    }

    public void setCodProduto(String codProduto) {
        this.codProduto = codProduto;
    }

    public BigDecimal getQuantKg() {
        return this.quantKg;
    }

    public void setQuantKg(BigDecimal quantKg) {
        this.quantKg = quantKg;
    }

    public BigDecimal getQuantUn() {
        return this.quantUn;
    }

    public void setQuantUn(BigDecimal quantUn) {
        this.quantUn = quantUn;
    }

    public BigDecimal getComprimento() {
        return this.comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    public Boolean getSobra() {
        return this.sobra;
    }

    public void setSobra(Boolean sobra) {
        this.sobra = sobra;
    }
}
