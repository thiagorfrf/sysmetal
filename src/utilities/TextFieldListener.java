/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 *
 * @author eric
 */
public class TextFieldListener implements ChangeListener<String> {

    private final TextField textField;

    public TextFieldListener(TextField textField) {
        this.textField = textField;
    }
    private String numericLastKey;

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        if (!isNumeric(newValue)) {
            textField.clear();
        } else if (!isValid()) {
            textField.clear();
        }
    }

    private boolean isNumeric(String text) {
        return text.matches("-?\\d+(.\\d+)?");
        // return text.matches("/^\\d+(\\.\\d+)?$/");
    }

    private boolean isValid() {
        if (textField.getText().length() == 0) {
            return true;
        }
        try {
            String testText = textField.getText();

            testText = (numericLastKey != null && numericLastKey != "") ? testText + numericLastKey : testText;
            numericLastKey = "";
            Integer.parseInt(testText);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
}
