/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utilities;

/**
 *
 * @author thiago
 */
public class ImportEntradaFornecedor {
    String fornecedor;
    String numNf;

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getNumNf() {
        return numNf;
    }

    public void setNumNf(String numNf) {
        this.numNf = numNf;
    }
    
}