/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import br.org.galvanisa.business.cadastre.entrada.EntradaProdutos;
import br.org.galvanisa.business.controleof.ControleOfModel;
import br.org.galvanisa.business.entity.ControleOf;
import br.org.galvanisa.business.entity.Entrada;
import br.org.galvanisa.business.entity.EntradaItens;
import br.org.galvanisa.business.entity.Produtos;
import br.org.galvanisa.business.entity.Saida;
import br.org.galvanisa.business.entity.materiais.MateriaisData;
import br.org.galvanisa.business.entradas.EntradasModel;
import br.org.galvanisa.business.produtos.LoadProdutosModel;
import br.org.galvanisa.business.saidas.SaidasModel;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author thiago
 */
public class ImportXLS extends Application {

    /*
     *
     *configurações da planilha   
     *
     */
    
     //TECNOMONT
     //Planilha 
     final int linhasQueSobram = 2;
     final String planilha = "t.xls";
     //Quais planilhas envolvidas?
     final int planilhaResumo = 1;
     final int planilhaMovimentacao = 2;
     final int planilhaControleNF = 3;

     final int linhasFinal = 22;
     //Linhas envolvidas PLANILHA CONTROLE NF
     final int linhaData = 0;
     final int linhaFornecedor = 1;
     final int linhaNF = 3;

     //Informações do cliente
     final int codigoControleCliente = 1;
     final String codigoCliente = "1539";
     
    /*
     //CONSTRUMETAL
     //Planilha 
     final int linhasQueSobram = 1;
     final String planilha = "c.xls";
     //Quais planilhas envolvidas?
     final int planilhaResumo = 1;
     final int planilhaMovimentacao = 2;
     final int planilhaControleNF = 4;

     //Linhas sem conteudo na planilha RESUMO
     final int linhasFinal = 2;
     //Linhas envolvidas PLANILHA CONTROLE NF
     final int linhaData = 0;
     final int linhaFornecedor = 2;
     final int linhaNF = 1;

     //Informações do cliente
     final int codigoControleCliente = 3;
     final String codigoCliente = "0010";
     */
    /*
     //CONSTRUMETAL 685
     //Planilha 
     final int linhasQueSobram = 1;
     final String planilha = "c2.xls";
     //Quais planilhas envolvidas?
     final int planilhaResumo = 1;
     final int planilhaMovimentacao = 2;
     final int planilhaControleNF = 4;

     //Linhas sem conteudo na planilha RESUMO
     final int linhasFinal = 2;
     //Linhas envolvidas PLANILHA CONTROLE NF
     final int linhaData = 0;
     final int linhaFornecedor = 2;
     final int linhaNF = 1;

     //Informações do cliente
     final int codigoControleCliente = 5;
     final String codigoCliente = "0010";
     */
    /*
     //ENGEVIX
     //Planilha 
     final int linhasQueSobram = 32;
     final String planilha = "e.xls";
     //Quais planilhas envolvidas?
     final int planilhaResumo = 1;
     final int planilhaMovimentacao = 2;
     final int planilhaControleNF = 4;

     //Linhas sem conteudo na planilha RESUMO
     final int linhasFinal = 35;

     //Linhas envolvidas PLANILHA CONTROLE NF
     final int linhaData = 0;
     final int linhaFornecedor = 2;
     final int linhaNF = 1;

     //Informações do cliente
     final int codigoControleCliente = 2;
     final String codigoCliente = "0828";
     */
    BigDecimal totalKg = BigDecimal.ZERO;
    Entrada entrada = new Entrada();
    Saida saida = new Saida();
    static List<Produtos> produtosList = new ArrayList<Produtos>();
    List<Entrada> entradaslist = new ArrayList<Entrada>();
    List<EntradaItens> entradaItensList = new ArrayList<EntradaItens>();

    List<ImportEntradas> importEntradasList = new ArrayList<ImportEntradas>();
    List<ImportEntradaFornecedor> importEntradaFornecedorList = new ArrayList<ImportEntradaFornecedor>();
    LoadProdutosModel loadProdutosModel = new LoadProdutosModel();
    List<Produtos> allProdutosList = new ArrayList<Produtos>();
    List<EntradaProdutos> entradaProdutosList = new ArrayList<EntradaProdutos>();
    EntradasModel entradasModel = new EntradasModel();

    List<MateriaisData> materiaisDataList = new ArrayList<MateriaisData>();
    List<ImportSaidas> importSaidasList = new ArrayList<ImportSaidas>();
    ControleOfModel controleOfModel = new ControleOfModel();
    List<ControleOf> controleOfList = new ArrayList<ControleOf>();
    List<ControleOf> controleOfListOF = new ArrayList<ControleOf>();
    List<ControleOf> controleOfListFinal = new ArrayList<ControleOf>();
    List<ControleOf> todasAsOfs = new ArrayList<ControleOf>();
    SaidasModel saidasModel = new SaidasModel();

    @Override

    public void start(Stage primaryStage) {
        carregarOfs();

        Button btn = new Button();
        btn.setText("Iniciar Importação");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                carregarProdutos();
                todasAsOfs();
                inserirEntradas();
                inserirSaidas();

            }
        });

        StackPane root = new StackPane();
        root.getChildren().add(btn);

        Scene scene = new Scene(root, 300, 250);
        try {
            todasAsOfs();
            executeEntradaImport();
            executeSaidaImport();

        } catch (IOException ex) {
            Logger.getLogger(ImportXLS.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BiffException ex) {
            Logger.getLogger(ImportXLS.class.getName()).log(Level.SEVERE, null, ex);
        }
        primaryStage.setTitle("Mostrar objetos");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);

    }

    public void executeEntradaImport() throws IOException, BiffException {
        Workbook workbook = Workbook.getWorkbook(new File(planilha));

        Sheet sheet = workbook.getSheet(planilhaResumo);

        int linhas = sheet.getRows();
        linhas = (linhas - linhasFinal);
        for (int i = 6; i < linhas; i++) {
            jxl.Cell celula1 = sheet.getCell(1, i);
            jxl.Cell celula2 = sheet.getCell(2, i);
            Produtos produtos = new Produtos();
            //Código Planilha
            produtos.setCodigo(celula1.getContents());
            //Código Microsiga
            produtos.setDescricao(celula2.getContents());
            produtosList.add(produtos);
        }
        for (int i = 0; i < produtosList.size(); i++) {
            System.out.println("Código Planilha :" + produtosList.get(i).getCodigo());
            System.out.println("Código Midrosiga :" + produtosList.get(i).getDescricao());
        }

        Sheet sheet3 = workbook.getSheet(planilhaControleNF);

        int linhas3 = sheet3.getRows();
        System.out.println("Numero de linhas :" + linhas3);
        linhas3 = linhas3 - linhasQueSobram;
        for (int i = 4; i < linhas3; i++) {
            jxl.Cell fornecedor = sheet3.getCell(linhaFornecedor, i);
            jxl.Cell numNf = sheet3.getCell(linhaNF, i);
            ImportEntradaFornecedor ief = new ImportEntradaFornecedor();
            //Código Planilha
            ief.setFornecedor(fornecedor.getContents());
            //Código Microsiga
            ief.setNumNf(numNf.getContents());
            if (!(ief.getFornecedor().isEmpty() || ief.getNumNf().isEmpty())) {
                importEntradaFornecedorList.add(ief);
            }

        }
        System.out.println("importEntradaFornecedorList :" + importEntradaFornecedorList.size());
        for (int i = 0; i < importEntradaFornecedorList.size(); i++) {
            System.out.println("ImportEntradaFornecedor fornecedor : " + importEntradaFornecedorList.get(i).getFornecedor());
            System.out.println("ImportEntradaFornecedor numNF : " + importEntradaFornecedorList.get(i).getNumNf());
        }

        Sheet sheet2 = workbook.getSheet(planilhaMovimentacao);
        int linhas2 = sheet2.getRows();
        System.out.println("Linhas 2: " + linhas2);
        for (int i = 0; i < linhas2; i++) {
            jxl.Cell dataEntrada = sheet2.getCell(0, i);
            jxl.Cell tipoOperacao = sheet2.getCell(1, i);
            jxl.Cell codigoMp = sheet2.getCell(2, i);
            jxl.Cell descricao = sheet2.getCell(3, i);
            jxl.Cell pesoKg = sheet2.getCell(4, i);
            jxl.Cell qtdUn = sheet2.getCell(5, i);
            jxl.Cell pesoTotal = sheet2.getCell(6, i);
            jxl.Cell nf = sheet2.getCell(7, i);
            jxl.Cell op = sheet2.getCell(8, i);

            if (tipoOperacao.getContents().equals("ENTRADA")) {
                ImportEntradas importEntrada = new ImportEntradas();

                System.out.println("Conteudo da celula 01 :" + tipoOperacao.getContents());
                System.out.println("Conteudo da celula 02 :" + codigoMp.getContents());
                System.out.println("Conteudo da celula 03 :" + descricao.getContents());
                System.out.println("Conteudo da celula 04 :" + pesoKg.getContents());
                System.out.println("Conteudo da celula 05 :" + qtdUn.getContents());
                System.out.println("Conteudo da celula 06 :" + pesoTotal.getContents());
                System.out.println("Conteudo da celula 07 :" + nf.getContents());
                System.out.println("Conteudo da celula 08 :" + op.getContents());
                System.out.println("-----------------------------------------");

                ImportEntradaFornecedor ief = new ImportEntradaFornecedor();
                ief = getFornecedor(importEntradaFornecedorList, nf.getContents()).get(0);
                String data;
                data = (20 + dataEntrada.getContents().substring(6, 8)
                        + dataEntrada.getContents().substring(3, 5)
                        + dataEntrada.getContents().substring(0, 2));
                importEntrada.setCodCliente(codigoCliente);
                Produtos unicoProduto = getCodigoProduto(produtosList, codigoMp.getContents()).get(0);
                importEntrada.setCodProduto("0" + unicoProduto.getCodigo() + "       ");
                //importEntrada.setCodigo(i);
                importEntrada.setComprimento(BigDecimal.ZERO);
                importEntrada.setDtEntrada(data);
                importEntrada.setNumCertificado("");
                importEntrada.setNumFornecedor(getDescFornecedor(ief.getFornecedor()));
                //importEntrada.getNumItem(); preencher no proximo passo
                importEntrada.setNumNf(nf.getContents());
                importEntrada.setQuantKg(new BigDecimal(virgulaToPonto(pesoTotal.getContents())));
                importEntrada.setQuantUn(new BigDecimal(virgulaToPonto(qtdUn.getContents())));
                importEntrada.setSobra(false);
                importEntrada.setTipoOperacao('N');

                //importEntrada.setTotalKg(); é preenchigo lá no método
                System.out.println("importEntrada getCodCliente :" + importEntrada.getCodCliente());
                System.out.println("importEntrada getCodProduto :" + importEntrada.getCodProduto());
                System.out.println("importEntrada getDtEntrada :" + importEntrada.getDtEntrada());
                System.out.println("importEntrada getNumCertificado :" + importEntrada.getNumCertificado());
                System.out.println("importEntrada getNumFornecedor :" + importEntrada.getNumFornecedor());
                System.out.println("importEntrada getNumNf :" + importEntrada.getNumNf());
                System.out.println("importEntrada getCodEntrada :" + importEntrada.getCodEntrada());
                System.out.println("importEntrada getCodEntradalocal :" + importEntrada.getCodEntradalocal());
                System.out.println("importEntrada getCodEstoque :" + importEntrada.getCodEstoque());
                System.out.println("importEntrada getCodigo :" + importEntrada.getCodigo());
                System.out.println("importEntrada getComprimento :" + importEntrada.getComprimento());
                System.out.println("importEntrada getNumItem :" + importEntrada.getNumItem());
                System.out.println("importEntrada getQuantKg :" + importEntrada.getQuantKg());
                System.out.println("importEntrada getQuantUn :" + importEntrada.getQuantUn());
                System.out.println("importEntrada getSobra :" + importEntrada.getSobra());
                System.out.println("importEntrada getTipoOperacao :" + importEntrada.getTipoOperacao());
                System.out.println("importEntrada getTotalKg :" + importEntrada.getTotalKg());
                if (!importEntrada.getCodProduto().contains("-")) {
                    importEntradasList.add(importEntrada);
                }
                //System.out.println("Codigo da lista ");
            }
        }
    }

    private void inserirEntradas() {

        for (int l = 0; l < importEntradasList.size(); l++) {
            System.out.println("codigo produto :" + importEntradasList.get(l).getCodProduto());
            System.out.println("ProdutoList size :" + produtosList.size());
            System.out.println("Tamanho do retorno do método :" + getProdutos(produtosList, importEntradasList.get(l).getCodProduto()).size());
        }

        for (int i = 0; i < importEntradaFornecedorList.size(); i++) {
            totalKg = BigDecimal.ZERO;
            for (int j = 0; j < importEntradasList.size(); j++) {
                if (importEntradaFornecedorList.get(i).getNumNf().equals(importEntradasList.get(j).getNumNf())) {

                    EntradaProdutos entradaProdutos = new EntradaProdutos();
                    totalKg = totalKg.add(importEntradasList.get(j).getQuantKg());
                    entrada.setCodCliente(codigoCliente);
                    entrada.setCodEntrada(0);
                    entrada.setDtEntrada(importEntradasList.get(j).getDtEntrada());
                    entrada.setNumCertificado(importEntradasList.get(j).getNumCertificado());
                    entrada.setNumFornecedor(importEntradasList.get(j).getNumFornecedor());
                    entrada.setNumNf(importEntradasList.get(j).getNumNf());
                    entrada.setTotalKg(totalKg);
                    entrada.setTipoOperacao('N');

                    entradaProdutos.setCodProduto(importEntradasList.get(j).getCodProduto());
                    //entradaProdutos.setDescProduto(getCodigoProduto(produtosList, importEntradasList.get(j).getCodProduto()).get(0).getDescricao());
                    entradaProdutos.setCodEntrada(0);
                    entradaProdutos.setCodigo(0);
                    entradaProdutos.setComprimento(BigDecimal.ZERO);
                    entradaProdutos.setFconversao(getProdutos(produtosList, importEntradasList.get(j).getCodProduto()).get(0).getFconversao());
                    entradaProdutos.setNumItem(0);
                    entradaProdutos.setQuantUn(importEntradasList.get(j).getQuantUn());
                    entradaProdutos.setQuantKg(importEntradasList.get(j).getQuantKg());
                    entradaProdutos.setSobra(false);
                    // entradaItens
                    entradaProdutosList.add(entradaProdutos);
                }
            }

            if (entradaProdutosList.size() >= 1) {
                entradasModel.saveNewEntrada(entrada, FXCollections.observableArrayList(entradaProdutosList));
            }
            entradaProdutosList.clear();
        }
    }

    /*
     *
     *
     *
     *
     *Saídas
     *
     *
     *
     *
     *
     */
    private void inserirSaidas() {
        for (int i = 0; i < todasAsOfs.size(); i++) {
            totalKg = BigDecimal.ZERO;
            for (int j = 0; j < importSaidasList.size(); j++) {
                if (importSaidasList.get(j).getCodControleOf().equals(todasAsOfs.get(i).getCodControleOf())) {
                    MateriaisData materiaisData = new MateriaisData();
                    totalKg = totalKg.add(importSaidasList.get(j).getQuantKg());
                    saida.setCodClienteTransf("");
                    saida.setCodControleCliente(codigoControleCliente);
                    saida.setCodControleOf(importSaidasList.get(j).getCodControleOf());
                    saida.setDtSaida(importSaidasList.get(j).getDtSaida());
                    saida.setTotalKg(totalKg);
                    saida.setTipoOperacao('N');

                    materiaisData.setCodProduto(importSaidasList.get(j).getCodProduto());
                    materiaisData.setCodCliente(codigoCliente);
                    materiaisData.setComprimento(BigDecimal.ZERO);
                    materiaisData.setFconversao(getProdutos(produtosList, importSaidasList.get(j).getCodProduto()).get(0).getFconversao());
                    materiaisData.setDescProduto(getProdutos(produtosList, importSaidasList.get(j).getCodProduto()).get(0).getDescricao());
                    materiaisData.setQuantUn(importSaidasList.get(j).getQuantUn());
                    materiaisData.setQuantKg(importSaidasList.get(j).getQuantKg());
                    materiaisData.setSobra(false);
                    // entradaItens
                    materiaisDataList.add(materiaisData);
                    for (int k = 0; k < materiaisDataList.size(); k++) {
                        System.out.println("materiaisDataList getCodCliente :" + materiaisDataList.get(k).getCodCliente());
                        System.out.println("materiaisDataList getCodProduto :" + materiaisDataList.get(k).getCodProduto());
                        System.out.println("materiaisDataList getDescProduto :" + materiaisDataList.get(k).getDescProduto());
                        System.out.println("materiaisDataList getCodEstoque :" + materiaisDataList.get(k).getCodEstoque());
                        System.out.println("materiaisDataList getCodigo :" + materiaisDataList.get(k).getCodigo());
                        System.out.println("materiaisDataList getComprimento :" + materiaisDataList.get(k).getComprimento());
                        //System.out.println("materiaisDataList getEscolha :" + materiaisDataList.get(k).getEscolha());
                        System.out.println("materiaisDataList getFconversao :" + materiaisDataList.get(k).getFconversao());
                        System.out.println("materiaisDataList getQuantEmpenho :" + materiaisDataList.get(k).getQuantEmpenho());
                        System.out.println("materiaisDataList getQuantKg :" + materiaisDataList.get(k).getQuantKg());
                        System.out.println("materiaisDataList getQuantLiberada :" + materiaisDataList.get(k).getQuantLiberada());
                        System.out.println("materiaisDataList getQuantUn :" + materiaisDataList.get(k).getQuantUn());
                        System.out.println("materiaisDataList getSobra :" + materiaisDataList.get(k).getSobra());
                    }
                }
            }
            if (!materiaisDataList.isEmpty()) {
                saidasModel.saveNewSaida(codigoCliente, saida, FXCollections.observableArrayList(materiaisDataList));
            }
            for (int k = 0; k < materiaisDataList.size(); k++) {
                System.out.println("materiaisDataList getCodCliente :" + materiaisDataList.get(k).getCodCliente());
                System.out.println("materiaisDataList getCodProduto :" + materiaisDataList.get(k).getCodProduto());
                System.out.println("materiaisDataList getDescProduto :" + materiaisDataList.get(k).getDescProduto());
                System.out.println("materiaisDataList getCodEstoque :" + materiaisDataList.get(k).getCodEstoque());
                System.out.println("materiaisDataList getCodigo :" + materiaisDataList.get(k).getCodigo());
                System.out.println("materiaisDataList getComprimento :" + materiaisDataList.get(k).getComprimento());
                //System.out.println("materiaisDataList getEscolha :" + materiaisDataList.get(k).getEscolha());
                System.out.println("materiaisDataList getFconversao :" + materiaisDataList.get(k).getFconversao());
                System.out.println("materiaisDataList getQuantEmpenho :" + materiaisDataList.get(k).getQuantEmpenho());
                System.out.println("materiaisDataList getQuantKg :" + materiaisDataList.get(k).getQuantKg());
                System.out.println("materiaisDataList getQuantLiberada :" + materiaisDataList.get(k).getQuantLiberada());
                System.out.println("materiaisDataList getQuantUn :" + materiaisDataList.get(k).getQuantUn());
                System.out.println("materiaisDataList getSobra :" + materiaisDataList.get(k).getSobra());
            }
            materiaisDataList.clear();

        }
    }

    public void executeSaidaImport() throws IOException, BiffException {
        Workbook workbook = Workbook.getWorkbook(new File(planilha));

        Sheet sheet = workbook.getSheet(1);

        int linhas = sheet.getRows();
        linhas = (linhas - 22);
        for (int i = 6; i < linhas; i++) {
            jxl.Cell celula1 = sheet.getCell(1, i);
            jxl.Cell celula2 = sheet.getCell(2, i);
            Produtos produtos = new Produtos();
            //Código Planilha
            produtos.setCodigo(celula1.getContents());
            //Código Microsiga
            produtos.setDescricao(celula2.getContents());
            produtosList.add(produtos);
        }

        Sheet sheet2 = workbook.getSheet(2);
        int linhas2 = sheet2.getRows();
        System.out.println("Linhas 2: " + linhas2);
        for (int i = 0; i < linhas2; i++) {
            jxl.Cell dataSaida = sheet2.getCell(0, i);
            jxl.Cell tipoOperacao = sheet2.getCell(1, i);
            jxl.Cell codigoMp = sheet2.getCell(2, i);
            jxl.Cell descricao = sheet2.getCell(3, i);
            jxl.Cell pesoKg = sheet2.getCell(4, i);
            jxl.Cell qtdUn = sheet2.getCell(5, i);
            jxl.Cell pesoTotal = sheet2.getCell(6, i);
            jxl.Cell nf = sheet2.getCell(7, i);
            jxl.Cell op = sheet2.getCell(8, i);
            jxl.Cell of = sheet2.getCell(9, i);
            jxl.Cell prog = sheet2.getCell(10, i);
            jxl.Cell pc = sheet2.getCell(11, i);
            if (tipoOperacao.getContents().equals("SAIDA") && !dataSaida.getContents().isEmpty()) {
                System.out.println("S   A   I   D   A   S");
                System.out.println("Conteudo da celula 01 :" + tipoOperacao.getContents());
                System.out.println("Conteudo da celula 02 :" + codigoMp.getContents());
                System.out.println("Conteudo da celula 03 :" + descricao.getContents());
                System.out.println("Conteudo da celula 04 :" + pesoKg.getContents());
                System.out.println("Conteudo da celula 05 :" + qtdUn.getContents());
                System.out.println("Conteudo da celula 06 :" + pesoTotal.getContents());
                System.out.println("Conteudo da celula 07 :" + nf.getContents());
                System.out.println("Conteudo da celula 08 :" + op.getContents());
                System.out.println("-----------------------------------------");

                ImportSaidas importSaidas = new ImportSaidas();

                String data;
                data = (20 + dataSaida.getContents().substring(6, 8)
                        + dataSaida.getContents().substring(3, 5)
                        + dataSaida.getContents().substring(0, 2));

                importSaidas.setCodControleCliente(1);
                //importSaidas.setCodEntrada();
                //importSaidas.setCodEstoque();
                Produtos unicoProduto = getCodigoProduto(produtosList, codigoMp.getContents()).get(0);
                importSaidas.setCodProduto("0" + unicoProduto.getCodigo() + "       ");
                BigDecimal valor = BigDecimal.ZERO;
                importSaidas.setQuantUn(new BigDecimal(virgulaToPonto(qtdUn.getContents())));
                importSaidas.setTipoOperacao('N');
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                System.out.println("Tamanho da lista de produtos :" + produtosList.size());
                if (unicoProduto.getFconversao() != null) {
                    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                    System.out.println("Unico produto :" + unicoProduto.getFconversao());
                    valor = (unicoProduto.getFconversao().multiply(new BigDecimal(qtdUn.getContents())));
                    importSaidas.setQuantKg(valor);
                } else if (unicoProduto.getFconversao() == null) {
                    valor = (new BigDecimal(virgulaToPonto(pesoTotal.getContents())));
                    importSaidas.setQuantKg(valor);
                    System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    System.out.println("Fator de conversao variavel unico produto :" + unicoProduto.getFconversao());
                    System.out.println("Codigo do produto na variavel " + unicoProduto.getCodigo());
                    System.out.println("Codigo do produto na variavel " + unicoProduto.getDescricao());
                    System.out.println("Codigo do produto na planilha " + codigoMp.getContents());
                }

                //importEntrada.setCodigo(i);
                importSaidas.setComprimento(BigDecimal.ZERO);
                importSaidas.setDtSaida(data);
                System.out.println("Op :" + op.getContents());
                if (of.getContents().length() < 7) {
                    System.out.println("OF :" + of.getContents());
                } else {
                    System.out.println("OF :" + of.getContents().substring(5, 7));
                }

                if (controleOfListFinal.size() > 0) {
                    System.out.println("  codControleOF :" + controleOfListFinal.get(0).getCodControleOf());
                }
                if (of.getContents().length() < 7) {
                    controleOfListOF = getCodigoOPPorOF(controleOfList, of.getContents());
                } else {
                    controleOfListOF = getCodigoOPPorOF(controleOfList, of.getContents().substring(5, 7));
                }
                controleOfListFinal = getCodigoOPPorPC(controleOfListOF, pc.getContents());
                importSaidas.getCodControleOf();
                //importEntrada.getNumItem(); preencher no proximo passo
                //importSaidas.getCodEstoque(nf.getContents());

                if (controleOfListFinal.isEmpty()) {
                    ControleOf controleOf = new ControleOf();

                    if (of.getContents().length() < 7) {
                        controleOf.setNumOf(of.getContents());
                    } else {
                        controleOf.setNumOf(of.getContents().substring(5, 7));
                    }
                    controleOf.setCodControleCliente(codigoControleCliente);
                    controleOf.setStatus(true);
                    controleOf.setNumPc(pc.getContents());
                    System.out.println("num of" + controleOf.getNumOf());
                    System.out.println("num pc" + controleOf.getNumPc());
                    System.out.println("cod controle cliente" + controleOf.getCodControleCliente());
                    System.out.println("num CodControleOf" + controleOf.getCodControleOf());
                    System.out.println("num pc" + controleOf.getStatus());

                    controleOfModel.addOf(controleOf);
                    todasAsOfs();
                    importSaidas.setCodControleOf(todasAsOfs.get(todasAsOfs.size() - 1).getCodControleOf());
                } else {
                    importSaidas.setCodControleOf(controleOfListFinal.get(0).getCodControleOf());
                }

                if (!importSaidas.getCodProduto().contains("-")) {
                    importSaidasList.add(importSaidas);
                }
                //System.out.println("Codigo da lista ");
            }
        }
    }

    public void todasAsOfs() {
        todasAsOfs = controleOfModel.getOfs(codigoControleCliente);
    }

    private void carregarProdutos() {
        produtosList = loadProdutosModel.loadProdutosList();
        for (Produtos i : produtosList) {
            System.out.println("********************************************");
            System.out.println("Codigo do produto :" + i.getCodigo());
            System.out.println("Fator de Conversão do prosduto :" + i.getFconversao());
        }
    }

    private void carregarOfs() {
        controleOfList = controleOfModel.getOfs(codigoControleCliente);
    }

    List<Produtos> getCodigoProduto(List<Produtos> produtos, String descricao) {
        return produtos.stream().filter(p -> p.getDescricao().trim().equals(descricao.trim()))
                .collect(Collectors.<Produtos>toList());
    }

    List<ImportEntradaFornecedor> getFornecedor(List<ImportEntradaFornecedor> produtos, String nf) {
        return produtos.stream().filter(p -> p.getNumNf().trim().equals(nf.trim()))
                .collect(Collectors.<ImportEntradaFornecedor>toList());
    }

    List<Produtos> getProdutos(List<Produtos> produtos, String codigo) {
        return produtos.stream().filter(p -> p.getCodigo().trim().equals(codigo.trim()))
                .collect(Collectors.<Produtos>toList());
    }

    List<ControleOf> getCodigoOPPorOF(List<ControleOf> controleOf, String codigo) {
        return controleOf.stream().filter(p -> p.getNumOf().trim().equals(codigo.trim()))
                .collect(Collectors.<ControleOf>toList());
    }

    List<ControleOf> getCodigoOPPorPC(List<ControleOf> controleOf, String codigo) {
        return controleOf.stream().filter(p -> p.getNumPc().trim().equals(codigo.trim()))
                .collect(Collectors.<ControleOf>toList());
    }

    private String virgulaToPonto(String string) {
        String ponto = string.replace(',', '.');
        return ponto;
    }

    void imprimirNf() {
        for (int j = 0; j < importEntradasList.size(); j++) {
            System.out.println("Notas fiscais :" + importEntradasList.get(j).getCodProduto());
            System.out.println("Notas fiscais :" + importEntradasList.get(j).getNumNf());
        }
    }

    private String getDescFornecedor(String descFornecedor) {
        String codFornecedor = null;

        if (descFornecedor.contains("GERDAU")) {
            codFornecedor = ("0047");
        } else if (descFornecedor.contains("GALFERRO")) {
            codFornecedor = ("1159");
        } else if (descFornecedor.contains("SCALA")) {
            codFornecedor = ("0720");
        } else if (descFornecedor.contains("ENGEVIX")) {
            codFornecedor = ("0828");
        } else if (descFornecedor.contains("CM")) {
            codFornecedor = ("0032");
        } else if (descFornecedor.contains("CEDISA")) {
            codFornecedor = ("1971");
        } else if (descFornecedor.contains("CONSTRUMETAL")) {
            codFornecedor = ("0010");
        } else if (descFornecedor.contains("TECNOMONT")) {
            codFornecedor = ("1539");
        } else if (descFornecedor.contains("NI")) {
            codFornecedor = ("NI");
        } else {
            codFornecedor = "0";
        }
        return codFornecedor;
    }
}
